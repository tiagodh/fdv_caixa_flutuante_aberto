# TODO

<https://bitbucket.org/tiagofrancafernandes/fdv_caixa_flutuante_aberto/addon/trello/trello-board>


## Principais alterações

[*] Mudar de "Principal", para "Direita" ou "Esquerda".
[*] Criar verificação se já existe um direita ou esquerda e selecionar o marcado
[*] Encontrar uma forma eficiente de mostrar os banners no caixa_flutuante.php
[*] Corrigir a impressão da url da imagem
[*] Corrigir problema que ocorre ao editar item. (Ao editar este fica desabilitado)
[*] Fazer texto ficar dentro da caixa
[*] Colocar editor visual na edição de conteúdo
[*] Impedir de excluir um banner principal
[*] colocar pra mostrar imagem apenas se houver uma no banco
[*] Adicionar um item
[*] Criar coluna 'caixa_estado' na tabela 'adboxes_boxes' valores aceitáveis 'on' ou 'off'
[*] Ocultar 'Editar Ad'
[*] Listar caixa apenas se uma das caixas estiverem ativadas