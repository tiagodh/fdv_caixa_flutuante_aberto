<style type="text/css">
.tgo-right8 {
    right: 8px!important}
.tgo-top8 {
    top: 8px!important}
.tgo-absolute {
    position: absolute!important}
.tgo-paddingBottom16 {
    padding-bottom: 16px!important}
.tgo-justifyContentSpaceBetween {
    -webkit-box-pack: justify!important;
    -webkit-justify-content: space-between!important;
    -ms-flex-pack: justify!important;
    justify-content: space-between!important}
.tgo-alignItemsCenter {
    -webkit-box-align: center!important;
    -webkit-align-items: center!important;
    -ms-flex-align: center!important;
    align-items: center!important}
.tgo-flex {
    display: -webkit-box!important;
    display: -webkit-flex!important;
    display: -ms-flexbox!important;
    display: flex!important}
.tgo-xs-left0 {left:0;}
.tgo-marginTop4 {
    margin-top: 4px!important}
.uiScale .ui-caption {
    color:rgba(0, 0, 0, 0.77)!important;
    fill: rgba(0,0,0,.54)!important;
    clear: both;
    margin: 0 4px;
    padding: 3px;
    font-size: 13px;
    text-align: justify;
}
.tgo-backgroundColorWhite {/* Força fundo branco */
    background-color: #fff!important}
.tgo-right20 {
    right: 20px!important}
.tgo-right40 {
    right: 40px!important}
.tgo-top20 {
    top: 20px!important}
.tgo-topPercent {
    /* top: 53.5%!important; */
    bottom: 11%!important;
}
.tgo-fixed {
    position: fixed!important}
.tgo-borderBox {
    -webkit-box-sizing: border-box!important;
    box-sizing: border-box!important}
.tgo-borderRadius4 {
    border-radius: 4px!important}
.tgo-borderLighter {
    border: 2px dotted rgb(60, 67, 107)!important}
.floating {
    overflow: hidden;
    position: absolute;
    z-index: 700;
    visibility: visible}

.parag  {
    display: block;
    margin-block-start: 1em;
    margin-block-end: 1em;
    margin-inline-start: 0px;
    margin-inline-end: 0px;
}
.img_banner_float, .ui-caption img {
    max-width: 300px;
    margin: 0;
    padding: 0;
    width: 95%;
}
.box_cabecalho {
    width: 100%;
    top: 0;
}
.tipo_box_interna {
    font-size: 0.5em;
    float: left;
    padding: 6px 2px;
}
.box_interna {
    width: 100%;
}
.classe_fechar {
    padding: 0px 10px 5px 10px;
    cursor: pointer;
    float: right;
    background: transparent;
    text-align: center;
}

span.float_titulo {
    font-size: 0.8em;
    font-weight: bolder;
    color: #736d6d;
    text-shadow: #2d6698 1px 1px 1px;
}
span.float_subTitulo {
    font-size: 0.6em;
    font-weight: bolder;
    color: #1f6ca7;
}
div.oculta_div {
    display: none !important;
}

/* INICIO ANIMAÇÃO */
.animacao-in {
    margin-top: 25px;
    font-size: 21px;
    text-align: center;

    -webkit-animation: fadein 1s; /* Safari, Chrome and Opera > 12.1 */
       -moz-animation: fadein 1s; /* Firefox < 16 */
        -ms-animation: fadein 1s; /* Internet Explorer */
         -o-animation: fadein 1s; /* Opera < 12.1 */
            animation: fadein 1s;
}
.maxw{
    /* max-width: 378px !important; */
    max-width: 300px !important;
    min-width: 296px;
    min-height: 110px;
}
@keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}

/* Firefox < 16 */
@-moz-keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}

/* Safari, Chrome and Opera > 12.1 */
@-webkit-keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}

/* Internet Explorer */
@-ms-keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}

/* Opera < 12.1 */
@-o-keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}
/* FIM ANIMAÇÃO */

/* MEDIA QUERY */
@media only screen and (max-width: 500px) {
  .tgo-fixed{display:none;}
}
@media only screen and (max-width: 700px) {
  /*max-width: 278px !important;*/
}


/* Links */
.box_interna a {
    font-weight: bold;
}

.box_interna a{
    text-decoration: none !important;
}
.box_interna a:hover {
    text-decoration: underline;
}
.box_interna a:active {
    color: #668CB3;
}
.box_interna a:hover, .box_interna a:focus {
    color: #004080;
    text-decoration: underline;
}


/*Nova Media Query*/
@media screen and (min-width: 10px) and (max-width: 864px) {
  .tgo-topPercent {
    /* top: 53.5%!important; */
    bottom: 8.2%!important;
    /*background: red !important;*/
    display:none;
   }
}
/*
@media screen and (min-width: 600px) and (max-width: 965px) {
  .tgo-topPercent {
    /* top: 53.5%!important; *//*
    bottom: 32%!important;
    background: green !important;
   }
}*/
@media screen and (min-width: 865px) and (max-width: 1207px) {
  .tgo-topPercent {
    /* top: 53.5%!important; */
    bottom: 25%!important;
    /*background: blue !important;*/
   }
}
@media screen and (min-width: 1208px) and (max-width: 2100px){
  .tgo-topPercent {
    /* top: 53.5%!important; */
    bottom: 16%!important;
    /*background: yellow !important;*/
   }
}
/*FIM Nova Media Query*/
</style>
