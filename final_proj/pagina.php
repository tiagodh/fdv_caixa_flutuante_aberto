<!DOCTYPE html>
<html>
<head>

<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<base href="https://www.falandodeviagem.com.br/">
<meta http-equiv="content-language" content="pt-br"/>
<meta http-equiv="content-style-type" content="text/css"/>
<meta http-equiv="imagetoolbar" content="no"/>
<meta name="resource-type" content="document"/>
<meta name="distribution" content="global"/>
<meta name="keywords" content="dicas, roteiro, miami, disney, florida, viagem, EUA, estados unidos, turismo, compras, shopping, sawgrass, outlet, hoteis, baratos, orlando, desconto, promoções, roupas, Buenos Aires, Nova York, Orlando, Aruba, Cancun, Curacao, Punta Cana, Rio de Janeiro, onde ficar, Montevideu, Barbados, Caribe, Europa, Paris, Londres, trem na Europa, primeira viagem Europa, Berlim, St Maarten, St Marteen, Lisboa, Roma, Amsterdam, Amsterda, Bruges, Costa Amalfitana, Nordeste, resorts, all-inclusive, passagens aéreas, promocao, milhas, milhagem, smiles, fidelidade tam, cartao de credito, malas, bagagem, gol, tam, azul, barata, passagem, volta ao mundo, regras passagem, dicas, aeroporto, singapore airlines, american airlines, tam, gol, trip, webjet, varig, airfrance, klm, singapore, companhia aerea, empresa aerea, programa de fidelidade, status elite, tap, assentos, sala vip, ac power, dc power, aeroporto, classe economica, classe executiva, primeira classe, fidelidade, aadvantage, smiles, grecia, atenas, poseidon, sao lourenco, minas gerais, parque das aguas, fidelidade tam, admirals club, aerolineas argentinas, mexicana, aeromexico, delta, united, continental, taca, avianca, oceanair, lan, gogo inflight, low cost, alitalia, azul, british airlines, copa, destino, america do sul, argentina, emirates, iberia, klm, lufthansa, milhas, aadvantage, tam fidelidade, smiles pontos, pluna, oneworld, star alliance, skyteam"/> 
<meta name="description" content="Todas as informações para você viajar melhor. Dicas sobre passagens aéreas, promoções, programas de fidelidade, hotéis, destinos, resenhas e muito mais."/> 

        <!-- App Indexing for Google Search -->
        <link href="android-app://com.quoord.tapatalkpro.activity/tapatalk/tapatalk://www.falandodeviagem.com.br/?location=topic&amp;tid=2049&amp;page=1&amp;perpage=10&amp;channel=google-indexing" rel="alternate"/>
        <link href="ios-app://307880732/tapatalk/tapatalk://www.falandodeviagem.com.br/?location=topic&amp;tid=2049&amp;page=1&amp;perpage=10&amp;channel=google-indexing" rel="alternate"/>
        
        <link href="https://groups.tapatalk-cdn.com/static/manifest/manifest.json" rel="manifest">
        
        <meta name="apple-itunes-app" content="app-id=307880732, affiliate-data=at=10lR7C, app-argument=tapatalk://tapatalk://www.falandodeviagem.com.br/?location=topic&tid=2049&page=1&perpage=10"/>
        


<!-- Dynamic Facebook Mod - http://www.cocking.com/dynamic-facebook-twitter-meta-tags-phpbb/ -->
<meta property="og:locale" content="pt_BR"/>
<meta property="og:locale:alternate" content="pt_BR"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Benefícios MasterCard Black"/>
<meta property="og:url" content="https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049"/>
<meta property="og:site_name" content="Falando de Viagem"/>

	<meta property="og:description" content="Benefícios MasterCard Black O MasterCard Black é o melhor cartão disponível pela bandeira no mercado brasileiro. Site:   As bandeiras ofer..."/>


<meta property="og:image" itemprop="image" content="https://www.falandodeviagem.com.br/imagens20/MasterCardBlack.jpg">


<meta name="twitter:card" content="summary"/>
<meta name="twitter:url" content="https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049"/>
<meta name="twitter:title" content="Benefícios MasterCard Black"/>
<meta name="twitter:description" content="Benefícios MasterCard Black O MasterCard Black é o melhor cartão disponível pela bandeira no mercado brasileiro. Site:   As bandeiras ofer..."/>

<meta name="twitter:image" itemprop="image" content="https://www.falandodeviagem.com.br/imagens20/MasterCardBlack.jpg">

<!-- Dynamic Facebook Mod END -->

<title>Falando de Viagem &bull; Benefícios MasterCard Black</title>
<link rel="alternate" type="application/atom+xml" title="Feed - Falando de Viagem" href="https://www.falandodeviagem.com.br/feed.php"/><link rel="alternate" type="application/atom+xml" title="Feed - Todos os fóruns" href="https://www.falandodeviagem.com.br/feed.php?mode=forums"/><link rel="alternate" type="application/atom+xml" title="Feed - Novos tópicos" href="https://www.falandodeviagem.com.br/feed.php?mode=topics"/>
<link rel="stylesheet" href="https://www.falandodeviagem.com.br//new_style.css" type="text/css"/>
<style>html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:before,blockquote:after,q:before,q:after{content:'';content:none}table{border-collapse:collapse;border-spacing:0}</style>
<link rel="stylesheet" href="styles/new_beach/template/css/custom.css" type="text/css" media="screen">
<link rel='image_src' href='https://www.falandodeviagem.com.br/images/fdv-logo.jpg'/>
<style type="text/css">.rodape{font-size:10px;width:977px}.rodapeSocial{display:block}.rodapeSocialBox{display:block;float:left;padding:30px 10px;width:142px;border-right:1px solid #ccc}.rodapeSocialBoxR{display:block;float:left;padding:30px 10px;width:142px}.rodapeSocialIcone{float:left;margin-right:10px;text-align:left}.rodapeSocialTxt{float:left;text-align:left}.rodapeSocialTxt a{color:#1a64c9}.rodapeSocialTxt h4{float:left;font-size:14px;text-align:left}.rodapeBox{margin:20px 0 10px 0;height:40px;background:url(pag/img/bgRodape.jpg) repeat-x;text-align:left;font-size:10px;color:#fff}.rodapeLogo{margin:10px 100px 0 10px;alignment-adjust:middle}.paginstyle{background-color:#44adf1;color:#fff;padding:4px;border:1px solid #c4ff96}a.paginstyle:link,a.paginstyle:visited,a.paginstyle:active{background-color:#44adf1;color:#fff;padding:4px;border:1px solid #c4ff96}.onpagestyle{background-color:#91cb21;color:#fff;padding:4px;border:1px solid #c4ff96}</style>

<script type='text/javascript'>var gptadslots=[];var googletag=googletag||{};googletag.cmd=googletag.cmd||[];(function(){var gads=document.createElement('script');gads.async=true;gads.type='text/javascript';var useSSL='https:'==document.location.protocol;gads.src=(useSSL?'https:':'http:')+'//www.googletagservices.com/tag/js/gpt.js';var node=document.getElementsByTagName('script')[0];node.parentNode.insertBefore(gads,node);})();</script>

 
<script type='text/javascript'>googletag.cmd.push(function(){googletag.defineSlot('/30511280/banner_post',[468,60],'div-gpt-ad-1380752548597-0').addService(googletag.pubads());googletag.defineSlot('/30511280/banner_rodape',[728,90],'div-gpt-ad-1380752548597-1').addService(googletag.pubads());googletag.defineSlot('/30511280/ItaboraiPlaza-300x250',[300,250],'div-gpt-ad-1380752548597-2').addService(googletag.pubads());googletag.defineSlot('/30511280/ItaboraiPlaza728x90',[728,90],'div-gpt-ad-1380752548597-3').addService(googletag.pubads());gptadslots[7]=googletag.defineSlot('/30511280/ItaboraiPlaza-300x250-1',[[300,250]],'div-gpt-ad-206655188689252952-1').addService(googletag.pubads());gptadslots[8]=googletag.defineSlot('/30511280/ItaboraiPlaza-300x250-2',[[300,250]],'div-gpt-ad-206655188689252952-2').addService(googletag.pubads());gptadslots[9]=googletag.defineSlot('/30511280/ItaboraiPlaza-300x250-3',[[300,250],[300,600],[336,280],[320,100]],'div-gpt-ad-206655188689252952-3').addService(googletag.pubads());gptadslots[12]=googletag.defineSlot('/30511280/Right_Sidebar_NEW',[[300,250],[336,280],[300,600]],'div-gpt-ad-631567264683897441-1').addService(googletag.pubads());googletag.pubads().enableAsyncRendering();googletag.enableServices();});</script>

<script type="text/javascript">//<![CDATA[
function hide(id){document.getElementById(id).style.display="none";}function show(id){document.getElementById(id).style.display="block";}function layerTest(id){if(document.getElementById(id).style.display=='none'){show(id);}else{hide(id);}}function layerQR(id){if(document.getElementById(id).style.display=='block'){hide(id);}else{show(id);}}function popup(url,width,height,name){if(!name){name='_popup';}window.open(url.replace(/&amp;/g,'&'),name,'height='+height+',resizable=yes,scrollbars=yes,width='+width);return false;}function jumpto(){var page=prompt('Escreva o número da página a qual você deseja ir:','1');var per_page='10';var base_url='https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049&amp;sid=5aa91c30d4923ae7dc55879a7590b259';if(page!==null&&!isNaN(page)&&page==Math.floor(page)&&page>0){if(base_url.indexOf('?')==-1){document.location.href=base_url+'?start='+((page-1)*per_page);}else{document.location.href=base_url.replace(/&amp;/g,'&')+'&start='+((page-1)*per_page);}}}function find_username(url){popup(url,870,570,'_usersearch');return false;}function marklist(id,name,state){var parent=document.getElementById(id);if(!parent){eval('parent = document.'+id);}if(!parent){return;}var rb=parent.getElementsByTagName('input');for(var r=0;r<rb.length;r++){if(rb[r].name.substr(0,name.length)==name){rb[r].checked=state;}}}function selectCode(a){var e=a.parentNode.parentNode.getElementsByTagName('CODE')[0];if(window.getSelection){var s=window.getSelection();if(s.setBaseAndExtent){s.setBaseAndExtent(e,0,e,e.innerText.length-1);}else{var r=document.createRange();r.selectNodeContents(e);s.removeAllRanges();s.addRange(r);}}else if(document.getSelection){var s=document.getSelection();var r=document.createRange();r.selectNodeContents(e);s.removeAllRanges();s.addRange(r);}else if(document.selection){var r=document.body.createTextRange();r.moveToElementText(e);r.select();}}
//]]></script>

<script type="text/javascript" src="//code.jquery.com/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="https://www.falandodeviagem.com.br/fbimg/jquery.cookie.js"></script>
<!-- <link rel="stylesheet" type="text/css" href='" media="screen"> -->
<link rel="stylesheet" href="https://www.falandodeviagem.com.br/pag/css/superfish.css.pagespeed.ce.23BmQRsWQ5.css" type="text/css" media="screen">

<script src="pag/js/jquery/superfish/hoverIntent.js+superfish.js.pagespeed.jc.9nJ_qbsC5q.js"></script><script>eval(mod_pagespeed__Tfq9XOyVP);</script>
<script>eval(mod_pagespeed_KkWgPfQSWp);</script>
<script type="text/javascript">jQuery(function(){jQuery('ul.sf-menu').superfish();});$(document).ready(function(){var seps=$(document).find("div#customsep");if(seps.length>0){seps.each(function(index,element){$(this).css("display","none");return false;});}var images=['1.jpg','2.jpg','3.jpg','4.jpg','5.jpg','6.jpg','7.jpg','8.jpg','9.jpg','10.jpg','11.jpg'];$("img#rnd").attr("src","fbimg/rnd/"+images[Math.floor(Math.random()*images.length)]);if($.cookie("liked")=="true"){$("#popcon").css("display","none");}else{if($.cookie("fbhour")=="true"){}else{$("#popcon").css("display","block");}}if((screen.width<=1024))$("#adbrite-tab").css("display","none");if((screen.width<=1280)&&(screen.width>1024))$("#adbrite-tab").css("margin-left","-139px");$('ul.sf-menu').superfish({animation:{opacity:'show',height:'show'}});});function closepop(){var date=new Date();date.setTime(date.getTime()+(30*60*1000));$.cookie("fbhour","true",{expires:date,path:'/'});$("#popcon").css("display","none");}</script>



<!--script type="text/javascript">

  var _gaq = _gaq || [];
  var pluginUrl = 
  '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
  _gaq.push(['_require', 'inpage_linkid', pluginUrl]);
  _gaq.push(['_setAccount', 'UA-17636981-1']);
  _gaq.push(['_setDomainName', 'falandodeviagem.com.br']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script-->
<style type="text/css">.fixed2{width:100%;height:auto;float:left;position:fixed;top:0;z-index:999}</style>
<script type="text/javascript">$(document).ready(function(){$(window).scroll(function(){if($(this).scrollTop()>120){$(".fixedMenu").addClass("fixed2");}else{$(".fixedMenu").removeClass("fixed2");}});});</script>

<!-- Begin comScore Tag -->
<script>var _comscore=_comscore||[];_comscore.push({c1:"2",c2:"16950873"});(function(){var s=document.createElement("script"),el=document.getElementsByTagName("script")[0];s.async=true;s.src=(document.location.protocol=="https:"?"https://sb":"http://b")+".scorecardresearch.com/beacon.js";el.parentNode.insertBefore(s,el);})();</script>
<noscript>
  <img src="//b.scorecardresearch.com/p?c1=2&c2=16950873&cv=2.0&cj=1"/>
</noscript>
<!-- End comScore Tag --><link rel="canonical" href="https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049"/>

<script id="navegg" type="text/javascript" src="//tag.navdmp.com/tm36974.js"></script>

<!-- Begin comScore Tag -->
<script>var _comscore=_comscore||[];_comscore.push({c1:"2",c2:"14194541"});(function(){var s=document.createElement("script"),el=document.getElementsByTagName("script")[0];s.async=true;s.src=(document.location.protocol=="https:"?"https://sb":"http://b")+".scorecardresearch.com/beacon.js";el.parentNode.insertBefore(s,el);})();</script>

<noscript>
	<img src="http://b.scorecardresearch.com/p?c1=2&c2=14194541&cv=2.0&cj=1"/>
</noscript>
<!-- End comScore Tag -->
<script type="text/javascript" id="r7barrautil" src="//barra.r7.com/barra.js">{ banner: false, submenu: true }</script>
<!-- TailTarget Tag Manager TT-9964-3/CT-23 -->
<script>(function(i){var ts=document.createElement('script');ts.type='text/javascript';ts.async=true;ts.src=('https:'==document.location.protocol?'https://':'http://')+'tags.t.tailtarget.com/t3m.js?i='+i;var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(ts,s);})('TT-9964-3/CT-23');</script>
<!-- End TailTarget Tag Manager -->

</head>
<body class="ltr">

<link itemprop="thumbnailUrl" href="https://www.falandodeviagem.com.br/imagens19/rodape1.png"> 

<span itemprop="thumbnail" itemscope itemtype="http://schema.org/ImageObject"> 
	<link itemprop="url" href="https://www.falandodeviagem.com.br/imagens19/rodape1.png"> 
</span>

<!-- Tapatalk Detect body start --> 
<script type="text/javascript">if(typeof(app_ios_id)!="undefined"){tapatalkDetect();}</script>
<!-- Tapatalk Detect banner body end -->
<!--
<div style="display:none; position:fixed; margin-left:35%; top:25%; z-index:10000; border:1px solid #000" id="popcon">
  <div id="pop"  style="width:412px; " class="reveal-modal">
    <img src="https://www.falandodeviagem.com.br/fbimg/1.png" width="412" height="16"  onclick="closepop()" style="cursor:pointer" />
    <img src="https://www.falandodeviagem.com.br/fbimg/2.png" width="412" height="36" />
    <img src="https://www.falandodeviagem.com.br/fbimg/test.png" width="412" height="309" id="rnd" class="rnd"  />
    <div id="fb" style="position:absolute; right:10px; top:92%" >
      <fb:like href="https://www.facebook.com/FalandoDeViagem" send="false" layout="button_count" width="450" show_faces="true"></fb:like>
    </div>
    <img src="https://www.falandodeviagem.com.br/fbimg/3.png" width="412" height="44"  />
  </div>
</div>
-->
<a name="top"></a>
<style type="text/css">#adbrite-tab{position:absolute;width:160px;height:600px;top:30%;left:100%;margin-left:-180px;text-align:center;padding:10px}</style>

<section>
    <header id="header">
      <div class="line-blue-hd">
        <div class="limit-1190">
          <nav class="menu-secundary left">
            <ul>
                <li class="active"><a data-remote="true" href="http://app.falandodeviagem.com.br/account/login">Login</a></li>
                
              <li><a href="http://app.falandodeviagem.com.br/account/auth/facebook">Facebook Conectar</a></li>
              <li><a href="http://app.falandodeviagem.com.br/account/auth/google_oauth2">Google Conectar</a></li>
              
            </ul>
          </nav>
          <form class="form_search left" name="form_search" action="pesquisar.php" enctype="multipart/form-data">
            <input type="text" onclick="if(this.value=='Buscar')this.value='';" onblur="if(this.value=='')this.value='Buscar';" title="Google Pesquisa Personalizada" value="Buscar" id="q" name="q" size="17"/>            
            <input type="submit" name="bt_buscar" value="Buscar" class="bt_buscar right">
          </form>
          <div class="follow-us right">
            <ul>
              <li><a href="https://www.facebook.com/FalandoDeViagem" target="blank" class="spr fb external">facebook</a></li>
              <li><a href="https://plus.google.com/u/0/106394975895440310613" target="blank" class="spr goo external">google</a></li>
              <li><a href="https://www.youtube.com/falandodeviagem" target="blank" class="spr ytb external">youtube</a></li>
              <li><a href="https://twitter.com/falandodeviagem" target="blank" class="spr tt external">twitter</a></li>
              <li><a href="https://instagram.com/falandodeviagem" target="blank" class="spr stg external">instagram</a></li>
            </ul>
          </div>
        </div>
      </div>
      <!-- end line-blue-hd -->
      <div class="limit-1190">
        <h1 class="left logo">
        <a href="http://app.falandodeviagem.com.br">
          <img alt="Falando de Viagem" src="styles/new_beach/template/images/logo.png" title="Falando de Viagem">
        </a>
        </h1>
        <div class="promo-hd right">
          <h6>Publicidade</h6>
            <center>
              <!-- ItaboraiPlaza728x90 -->
              <div id='div-gpt-ad-1380752548597-3' style='padding-top:6px;width:728px; height:90px;'>
                <script type='text/javascript'>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1380752548597-3');});</script>
              </center>
            </div>
        <div class="clear"></div>
        <nav class="menuBar">
        <ul>
            <li class="active">
              <a class="active" href="http://app.falandodeviagem.com.br"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAAlCAYAAADFniADAAAFcUlEQVRYw8WYe1DUVRTHyUr/VcxQS8txcCwSFnCX1dQSRQMR18ekkDrm1Gg45vjKNGcac5jK1HIEFVAQtHR0MwmU18IuKCQaClSKvBNDU3Fxl+WxD76dc3kMEIjyY/HOnGH3tz/u/fy+53F/5zoAcGhnA8n866zWn0qMxtI/9DU1BfoaQ18bzfuo2GAsr7VY1LTeArJB7TnaA71mMFvO779ZZPFJ18EjMdnu5p2mxXfXC60PGxt1tP64zlCv3qmv/0uVeUHc7JWc2m/mSev5ajPwd62pmDicW6GeJxnjF1y4CIWEyScmpUiC8yMwfaM5k0OIoXyOlJaZpSj0dooGm6/lSQJjxfbeKLQSz0IHs812ZI4us9eTudNkx8orBNST3K9oUbW9tXpoBsUYee0Xh9umuoLePiFPFpj1G2xNTQiiv93d53o+CZ60RkDGBWy8eg27rt9AeHEp1LcqcbikFGt/zxVBzw9YZDBWOBQZDLd76zqPxBTkPKhGvdWKWZSxHX9LxrTUNGy5lg/dv/egN5thsTWhotaEH0nZj3OuYAr9zsCtovD/5On11ZKgFl/MFipV1dW1ucCTQDmbYsrKcb+hATxMBK25cxcfXsppW1zR5UMKqIe9hmKp+Yl5UDEU3ydRwMcSDMWFuN5Eprl7F1xq3EiRnpWXCCUnyStqa8XicZW3seZKbpsyPB7Q53W5V58Ips+g2EWmFkUYhrK4DehydbWIMflTJpBkqJWXLqOrkX3/AZS9LC+SoTi1O49/KOCVKb2v6pKhtucXdABqJPetaMmuZwa1NS+/A1RiVRVkEjdzyVBchVsHb1jvP6ai9xvU0uzm7YVHzp+FGLP5S3iePgMlbRdKTdqzgZqu0aKGtg4ee8JjMXjCdAz1nIWRAcvgvOUryKKOQZGQKACVmnRhXqkaeKXYEYr3q5sGg4DaEPIDhrj7YOjE2cIc6bOjbAZe8vLFCL8gjF4WjLHrt+ONnbsxYX8E3CKOQhZ9HIpOhbVPtpno0jIBtW1XGEHNbIPqyoa4egvYkQHLMe6LEHicUMOrU3GVDKWkHX4RVXV23ZgpAf8HIVc6ymZi2GR/jPrgE7jsDYM8/lyzG2mP7Fv38YRk43fswrCp8zCY3NQBxG2GuP76R+sx4cBh4SIO/u5ApEHRpPyUb367Dy9PUwkVRPx4+AjXOHkvxNhPt1KAH28O7nStHbNPwKTBZU+oWNiRYoeDmFUZ4b8Uzp/tgMdJNSaRGt5xiVj7dST8TpyFPFVjB6gWZThbhvsGYginPCnzyvwVGE9ZJGqSSPfmmjQlIQkZk1eh6TkV7jgFYuW+GHimpfUd1CStDm6RMRg+e7GIkVFBq+GyOxTyuARyi67b+FBQDVLP/Ry2ASo0DFyAvau/gZtOJw2KlXENjxYQo5evgSsFqrie/uQTy7RarAuJFGqxalmKYPj8HI+JqU8d6CmQ/3oOb31/UCjkRd8fl7o9vpUSwDtx53EqYCvqB83HvaGLsWHnIXikpz+lUkktIH3YkrsTxJKIk8iSB8Pywlyc9d2EqfGJ3SYBc+RzN0MvZnlyiS13T+aWocXysOPIVK5BldNCbNpxADKdtkuoEqOx3MFis4Vzk2jvgwxOAlZOFaPG0SXbcGbORqhi1R1U49OeOqv1FJ8lvEttd6N7P562cGxxCVm1OwqLok4TsEaoFHqziF/NAhhqANGpA7OyobCzG7t822ipc/PIWwazJZl4Xmw9n3KiPi13CYH19/kUr6fKuMhd9nU+uOt8kudE7bU6uqys0V+XKd6X7HmKx/Nz73iouMRstFgSWoE6Q4kDNDJvaizDK02mq4WPDOX2slsmUx51QFG03nvssvYc/wFE39SgfDxd4QAAAABJRU5ErkJggg==" alt="" data-pagespeed-url-hash="1450682042" onload="pagespeed.CriticalImages.checkImageForCriticality(this);"></a>
            </li>
            <li><a href="http://app.falandodeviagem.com.br/destinos">Destinos</a></li>
            <li><a href="http://app.falandodeviagem.com.br/hospedagens">Hoteis</a></li>
            <li><a href="http://app.falandodeviagem.com.br/voos">Voos</a></li>
            <li><a href="http://carros.falandodeviagem.com.br/?campaign=brasil&content=aluguel&requestorid=47&source=phpbb#/">Carros</a></li>
            <li><a href="https://falandodeviagem.minhaviagem.com.br/">Ingressos</a></li>
            <li><a href="http://app.falandodeviagem.com.br/seguro">Seguros</a></li>
            <!-- <li><a href="">Trens</a></li>
            <li><a href="">Cruzeiros</a></li> -->
            <li><a href="http://app.falandodeviagem.com.br/partners">Parceiros</a></li>
            <li><a href="http://app.falandodeviagem.com.br/revista">Revista</a></li>
            <li class="dropdown">
                <a href="https://falandodeviagem.com.br/index.php" class="bookings">Fórum</a>
                <div class="dropdown-content">
                  <a href="https://www.falandodeviagem.com.br/viewforum.php?f=3">Companhias aéreas</a>
                  <a href="https://www.falandodeviagem.com.br/viewforum.php?f=4">Benefícios financeiros</a>
                  <a href="https://www.falandodeviagem.com.br/viewforum.php?f=23">Milhas aéreas</a>
                  <a href="https://www.falandodeviagem.com.br/viewforum.php?f=343">Cambio</a>
                  <a href="https://www.falandodeviagem.com.br/viewforum.php?f=224">Alfândega</a>
                  <a href="https://www.falandodeviagem.com.br/viewforum.php?f=160">Passaporte</a>
                  <a href="https://www.falandodeviagem.com.br/viewforum.php?f=132">Vistos</a>
                  <a href="https://www.falandodeviagem.com.br/viewforum.php?f=267">Vacinas</a>
                </div>
            </li>
            <li class="dropdown">
                <a href="#" class="bookings">Mais</a>
                <div class="dropdown-content">
                  <a href="http://app.falandodeviagem.com.br/fotos">Fotos</a>
                  <a href="http://app.falandodeviagem.com.br/comunidades">Comunidade</a>
                  <a href="http://app.falandodeviagem.com.br/avaliacoes">Avalie destinos</a>
                  <a href="http://app.falandodeviagem.com.br/interests">Interesses</a>
                  <a href="http://app.falandodeviagem.com.br/destinos">Newsletter</a>
                  <a href="https://www.falandodeviagem.com.br/viewtopic.php?t=14810">Grupos WhatsApp</a>
                </div>
            </li>
        </ul>
        </nav>
      </div>
      <!-- end limit-1190 -->
    </header>
    <div class="clear"></div>
</section>

<div id="wrapheader">
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
  <td style="background-color: #FFFFFF;padding: 0px 5px 0px 5px;">
<!-- 
  <a class="squarebuttonl" href="https://www.falandodeviagem.com.br/faq.php?sid=5aa91c30d4923ae7dc55879a7590b259"><span>FAQ</span></a><span class="navspacel"></span>
  <a class="squarebuttonl" href="https://www.falandodeviagem.com.br/gallery/index.php?sid=5aa91c30d4923ae7dc55879a7590b259"><span>Gallery</span></a><span class="navspacel"></span>
  
  <a class="squarebuttonl" href="https://www.falandodeviagem.com.br/memberlist.php?sid=5aa91c30d4923ae7dc55879a7590b259"><span>Membros</span></a><span class="navspacel"></span>
  
<span class="navspacer"></span>
<!--
  
-->

</span>
  </td>
  </tr>
  </table>

</div>


<table class="wrap limit-1190" width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td class="inner main">

  <div id="datebar">
    <table width="100%" cellspacing="0">
      
            
    </table>
  </div>
  
  <!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) - Start //--><!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) - End //--><div class="bc" style="clear: both;">
	<table class="bcbg" width="100%" cellspacing="1" cellpadding="0" style="margin-top: 5px;">
	<tr>
		<td class="row1">
			<p class="breadcrumbs"><a style="background-color:#3374d0; color:#FFF; padding:2px; float:left " href="https://www.falandodeviagem.com.br/index.php?sid=5aa91c30d4923ae7dc55879a7590b259">Página Principal
            </a><div style="float:left;width:12px;height:18px;background-image:url(images/bread1.png.pagespeed.ce.rOcnHsFU-Z.png)"></div>
              <div id="customsep" style="float:left;width:12px;height:18px;background-image:url(images/bread2.png.pagespeed.ce.rOcnHsFU-Z.png)"></div><a href="https://www.falandodeviagem.com.br/viewforum.php?f=4&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="padding:2px; color:#FFF; background-color:#6c6c6c; float:left">Cartões de crédito e serviços financeiros</a>  <div id="customsep" style="float:left;width:12px;height:18px;background-image:url(images/bread2.png.pagespeed.ce.rOcnHsFU-Z.png)"></div><a href="https://www.falandodeviagem.com.br/viewforum.php?f=212&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="padding:2px; color:#FFF; background-color:#6c6c6c; float:left">MasterCard</a><div id="customsep" style="float:left;width:8px;height:18px;background-image:url(images/bread3.png.pagespeed.ce.Aao52Rmq5h.png)"></div></p>
			<p class="datetime"><span style="display: none;">Todos os horários são GMT - 3 horas [ <abbr title="Horário de verão">DST</abbr> ]</p>
		</td>
	</tr>
	</table>
</div><!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) ( full wide ) - Start //--><!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) ( full wide ) - End //--><!-- aqui ficava o botão curtir flutuante -->

<!--<div id="lateral" style="float: right; margin-left: 10px; width: 300px;">
 INCLUDE sidebar_body.html 
</div>-->
<aside class="right">
﻿﻿


<script>function abrefecha(obj){switch(obj.id){case'primeiro':document.getElementById('divum').style.display="none";document.getElementById('divdois').style.display="none";break
case'segundo':document.getElementById('divum').style.display="block";document.getElementById('divdois').style.display="none";break
case'terceiro':document.getElementById('divum').style.display="none";document.getElementById('divdois').style.display="block";break}}</script>
<div>
    <div class="faca-parte">
    <h3 class="faca_parte_header">Faça parte</h3>
    <nav>
        <ul id="faca_parte">
            <li><a href="http://app.falandodeviagem.com.br/fdv_profile?target=cadastre">Cadastre-se</a></li>
            <li><a href="http://app.falandodeviagem.com.br/fdv_profile?target=fotos">Envie fotos</a></li>
            <li><a href="http://app.falandodeviagem.com.br/fdv_profile?target=videos">Envie vídeos</a></li>
            <li><a href="http://app.falandodeviagem.com.br/fdv_profile?target=procurar">Procurar amigo</a></li>
            <li><a href="http://app.falandodeviagem.com.br/fdv_profile?target=avalie">Avalie um lugar</a></li>
            <li><a href="http://app.falandodeviagem.com.br/fdv_profile?target=roteiro">Faça seu roteiro</a></li>
            <li><a href="http://app.falandodeviagem.com.br/fdv_profile?target=postagem">Faça uma postagem</a></li>
            <div class="clear"></div>    
        </ul>
    </nav>
    </div>
</div>
<br/>

<div style="background: none repeat scroll 0 0 #CCFF99; border-bottom: 1px solid #3F8EC9; border-top: 1px solid #3F8EC9; color: #000000; font-size: 14px; font-weight: bold; padding: 5px 5px 5px 10px; font-family: arial,verdana,Helvetica,sans-serif;" id="destinos-populares-tit"><a class="genmedw" href="https://www.falandodeviagem.com.br/viewforum.php?f=0&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="text-transform: uppercase;"></a></div>


<div id="forum3" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread_subforum.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Troca de informações sobre companhias aéreas, milhas, promoções e tudo mais relacionado ao assunto." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=3&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Companhias aéreas</a></div>
	
</div>

<div id="forum5" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread_subforum.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Troca de informações sobre empresas de aluguel de carro." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=5&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Aluguel de carros</a></div>
	
</div>

<div id="forum7" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread_subforum.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Troca de informações sobre hotéis ao redor do mundo, promoções e programas de fidelidade." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=7&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Hotéis</a></div>
	
</div>

<div id="forum6" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread_subforum.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Troca de informações sobre destinos e empresas de cruzeiro." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=6&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Cruzeiros</a></div>
	
</div>

<div id="forum351" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread_subforum.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Troca de informações sobre destinos, produtos, serviços de viagem e os padrões de atendimento que o viajante encontrará: Exclusivas, Luxuosas, Moderadas e Econômicas." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=351&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Falando +D</a></div>
	
</div>

<div id="forum4" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread_subforum.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Troca de informações sobre cartões de crédito, serviços financeiros e suas vantagens em viagens." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=4&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Cartões de crédito e serviços financeiros</a></div>
	
</div>

<div id="forum13" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread_subforum.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Troca de informações sobre destinos na América do Sul." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=13&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Destinos - América do Sul</a></div>
	
</div>

<div id="forum9" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread_subforum.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Troca de informações sobre destinos na América do Norte." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=9&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Destinos - América do Norte</a></div>
	
</div>

<div id="forum22" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread_subforum.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Troca de informações sobre destinos na América Central, Caribe &amp; México." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=22&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Destinos - América Central, Caribe &amp; México</a></div>
	
</div>

<div id="forum8" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread_subforum.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Troca de informações sobre destinos no Brasil." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=8&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Destinos - Brasil</a></div>
	
</div>

<div id="forum10" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread_subforum.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Troca de informações sobre destinos na Europa." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=10&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Destinos - Europa</a></div>
	
</div>

<div id="forum11" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread_subforum.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Troca de informações sobre destinos na Ásia." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=11&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Destinos - Ásia</a></div>
	
</div>

<div id="forum61" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread_subforum.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Troca de informações sobre destinos na África." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=61&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Destinos - África</a></div>
	
</div>

<div id="forum12" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread_subforum.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Troca de informações sobre destinos na Oceania." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=12&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Destinos - Oceania</a></div>
	
</div>

<div id="forum25" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread_subforum.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Troca de informações sobre destinos no Oriente Médio." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=25&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Destinos - Oriente Médio</a></div>
	
</div>

<div id="forum84" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Troca de informações sobre intercâmbio no exterior." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=84&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Intercâmbio</a></div>
	
</div>

<div id="forum95" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread_subforum.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Troca de informações sobre documentos, vistos, vacinas e outras informações úteis para viagens." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=95&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Informações para viagens</a></div>
	
</div>

<div id="forum341" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Troca de dicas em geral sobre viagem." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=341&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Dicas de viagem</a></div>
	
</div>

<div id="forum165" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread_subforum.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Dicas sobre como usar a tecnologia para viajar melhor." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=165&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Tecnologia</a></div>
	
</div>

<div id="forum397" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Troca de informações sobre empresas rodoviárias." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=397&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Empresas Rodoviárias</a></div>
	
</div>

<div id="forum509" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Entrevista do Falando de Viagem." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=509&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Entrevistas</a></div>
	
</div>

<div id="forum414" style="overflow: hidden;">
	
	
</div>

<div id="forum415" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="O idealizador do Falando de Viagem escreve sobre suas viagens, experiências e dá valiosas dicas." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=415&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Coluna do Gabriel Dias</a></div>
	
</div>

<div id="forum416" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Um apaixonado por viagens, vinhos e carros. Se deixar, viaja até em pé, mas nunca deixa de viajar." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=416&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Coluna do Fabio Macedo</a></div>
	
</div>

<div id="forum539" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Seu lema é conhecer o mundo, então se o tema é viagem ela topa tudo, dos lugares mais exóticos aos mais refinados. Em sua coluna ela relatará um pouco de suas aventuras pelo mundo afora com muitas dicas." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=539&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Coluna da Juliana Magalhães</a></div>
	
</div>

<div id="forum659" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="" class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=659&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Coluna da Manoela Caldas</a></div>
	
</div>

<div id="forum88" style="overflow: hidden;">
	
	
</div>

<div id="forum167" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Informações gerais sobre o Falando de Viagem." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=167&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Informações sobre o Falando de Viagem</a></div>
	
</div>

<div id="forum578" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Informações sobre os parceiros do Falando de Viagem." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=578&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Parceiro Plus</a></div>
	
</div>

<div id="forum353" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Está sem companhia para viajar? Procure aqui um viajante para lhe acompanhar." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=353&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Vamos viajar juntos?</a></div>
	
</div>

<div id="forum299" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Espaço reservado para bate-papo em geral, sem assuntos específicos." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=299&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Bate-papo</a></div>
	
</div>

<div id="forum27" style="overflow: hidden;">
	
	
		<div class="forum" style="margin: 0 2px 0; border-bottom: 1px solid #CCCCCC; padding: 5px 0;"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/forum_unread.png" width="27" height="28" alt="Mensagens não lidas" title="Mensagens não lidas"/> <a title="Troca de informações sobre empresas que não merecem o nosso dinheiro." class="forumlinks" href="https://www.falandodeviagem.com.br/viewforum.php?f=27&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #3F8EC9;">Denuncie</a></div>
	
</div>


<br/>

<br/>
<div style="border-bottom:1px dotted #153448;padding-bottom:20px">
  <div id='div-gpt-ad-1380752548597-2' style='width:336px; height:280px;'>
    <script type='text/javascript'>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1380752548597-2');});</script>
  </div>
</div>

<br/>

<!-- Added on 28-May 2015  -->
<div style="border-bottom:1px dotted #153448;padding-bottom:20px">  
  <!-- Beginning Async AdSlot 1 for Ad unit ItaboraiPlaza-300x250-1  ### size: [[300,250]] --><!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[7]]) -->
  <div id='div-gpt-ad-206655188689252952-1'>
      <script type='text/javascript'>googletag.cmd.push(function(){googletag.display('div-gpt-ad-206655188689252952-1');});</script>
  </div>
  <!-- End AdSlot 7 -->
</div>
<br/>




<br/>
    


<br/>

<div class="forum" id="destinos-populares" style="overflow: hidden; width: 300px;">
<h3>Tópicos Recentes</h3><!--destinos-populares-tit-->
    <div class="box-forum">    
        <div id="forum_listing" style="overflow: hidden;">
            <ul class="list-forum">
                
                <li><a href="viewtopic.php?f=711&t=16885">The James Rotterdam: um hotel boutique na melhor localização de Roterdã</a></li>
                
                <li><a href="viewtopic.php?f=290&t=16884">Vale a pena pedir o cartão de crédito Multiplus em 2019?</a></li>
                
                <li><a href="viewtopic.php?f=701&t=16883">Como funciona o pacote de bebidas do Symphony of the Seas | Royal Caribbean</a></li>
                
                <li><a href="viewtopic.php?f=708&t=16882">Aluguel de carro em Barbados com a locadora Sixt rent a car</a></li>
                
                <li><a href="viewtopic.php?f=291&t=16881">Cartão de crédito Santander Select Unique MasterCard Black</a></li>
                
                <li><a href="viewtopic.php?f=651&t=16880">Sala VIP Star Alliance Lounge | Aeroporto de Ezeiza (EZE) | Buenos Aires</a></li>
                
                <li><a href="viewtopic.php?f=159&t=16879">Coco Key Hotel and Water Resort: hotel econômico em Orlando com parque aquático</a></li>
                
                <li><a href="viewtopic.php?f=91&t=16878">5 passeios não óbvios para fazer no Rio de Janeiro</a></li>
                
                <li><a href="viewtopic.php?f=125&t=16877">Nova York tem ofertas de musicais, restaurantes e passeios em janeiro e fevereiro de 2019</a></li>
                
                <li><a href="viewtopic.php?f=701&t=16875">Vale a pena fazer compras no Symphony of the Seas</a></li>
                
            </ul>
        </div>
    </div>
</div>
<!-- ENDPHP -->

<br/>

<div class="forum" id="destinos-populares" style="overflow: hidden; width: 300px;">
    <h3>Tópicos Populares</h3><!--destinos-populares-tit-->
    <div class="box-forum">    
        <div id="forum_listing" style="overflow: hidden;">
            <ul class="list-forum">

            

                        <li><a href="viewtopic.php?f=228&t=14510">Novas regras de bagagem na LATAM Airlines Brasil</a></li>

        



                        <li><a href="viewtopic.php?f=474&t=9309">Cupom de desconto exclusivo para o outlet Sawgrass Mills</a></li>

        



                        <li><a href="viewtopic.php?f=423&t=9124">Qual é a melhor mala de viagem?</a></li>

        



                        <li><a href="viewtopic.php?f=41&t=6480">Estações do ano nos Estados Unidos</a></li>

        



                        <li><a href="viewtopic.php?f=177&t=8054">Ingresso para os parques de Orlando em 10 vezes sem juros</a></li>

        



                        <li><a href="viewtopic.php?f=41&t=687">Imigração e Alfândega nos Estados Unidos: Como funciona?</a></li>

        



                        <li><a href="viewtopic.php?f=395&t=5175">Posso transferir minha passagem aérea para outra pessoa?</a></li>

        



                        <li><a href="viewtopic.php?f=182&t=1761">Jersey Gardens Outlet Mall</a></li>

        



                        <li><a href="viewtopic.php?f=186&t=2004">Imposto de compra nos Estados Unidos</a></li>

        



                        <li><a href="viewtopic.php?f=10&t=6245">Quantos euros devo levar na minha viagem?</a></li>

        


        </ul>
        </div>
    </div>
</div>

<!-- Banner r7.com -->
<script data-sizes="[300, 250]" data-vendor="r7.com" src="https://sc.r7.com/r7/js/adPartner.min.js"></script>
<!-- Banner r7.com -->

<br/>

<!-- Added on 28-May 2015  -->
<div style="border-top:1px dotted #153448;padding-top:20px; ">
  <!-- Beginning Async AdSlot 2 for Ad unit ItaboraiPlaza-300x250-2  ### size: [[300,250]] --><!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[8]]) -->
  <div id='div-gpt-ad-206655188689252952-2'>
    <script type='text/javascript'>googletag.cmd.push(function(){googletag.display('div-gpt-ad-206655188689252952-2');});</script>
  </div>
  <!-- End AdSlot 8 -->
</div>

<br/>
<!-- New banner added on 11- July, 2015 -->
<div style="border-top:1px dotted #153448;border-bottom:1px dotted #153448;padding-bottom:20px">
  <!-- Beginning Async AdSlot 3 for Ad unit ItaboraiPlaza-300x250-3  ### size: [[300,250]] --><!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[9]]) -->
  <div id='div-gpt-ad-206655188689252952-3'>
    <script type='text/javascript'>googletag.cmd.push(function(){googletag.display('div-gpt-ad-206655188689252952-3');});</script>
  </div>
  <!-- End AdSlot 9 -->
</div>

<!-- Beginning Async AdSlot 12 for Ad unit Right_Sidebar_NEW  ### size: [[300,250],[336,280],[300,600]] --><!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[12]]) -->
<div id='div-gpt-ad-631567264683897441-1'>
	<script type='text/javascript'>googletag.cmd.push(function(){googletag.display('div-gpt-ad-631567264683897441-1');});</script>
</div>
<!-- End AdSlot 12 -->
</div>
</aside>
<div class="container">
    <div class="page-hospedagens">
        <section class="main limit-1190">
            <article class="left">
<div id="pagecontent">
  <script type="text/javascript">google_ad_client="ca-pub-4025109770547615";google_ad_slot="3380848693";google_ad_width=468;google_ad_height=15;</script>
  <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>
  <br/>

<div id="pageheader" class="forum-header" style="background-color: #CCFF99;border-bottom: 2px solid #66CCFF;border-top: 2px solid #66CCFF;">
                                <h2 style="padding-top: 5px;padding-left: 5px;color: black;"><a class="titles" href="https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049&amp;sid=5aa91c30d4923ae7dc55879a7590b259">Benefícios MasterCard Black</a></h2>
</div>

<div class="forumbar">
<table width="100%" cellspacing="0">
  <tr>
    <td nowrap="nowrap">
      
    </td>
  </tr>
</table>
</div>

<div id="social_icons" style="text-align: left;padding-top: 5px;">

<div id="fb-root"></div>
<script type="text/javascript">//<![CDATA[
(function(d,s,id){document.write('<fb:like href="https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049" send="true" layout="button_count" width="100" show_faces="true" action="like" colorscheme="light" font="verdana"><\/fb:like>');var js,fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="//connect.facebook.net/pt_BR/all.js#xfbml=1";fjs.parentNode.insertBefore(js,fjs);}(document,'script','facebook-jssdk'));
//]]></script>

<script type="text/javascript">//<![CDATA[
(function(){document.write('<a href="https://twitter.com/share" class="twitter-share-button" data-url="https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049" data-text="Benefícios MasterCard Black" data-count="horizontal" data-via="" data-lang="pt">Tweet<\/a>');var s=document.createElement('SCRIPT'),s1=document.getElementsByTagName('SCRIPT')[0];s.type='text/javascript';s.async=true;s.src='//platform.twitter.com/widgets.js';s1.parentNode.insertBefore(s,s1);})();
//]]></script>
<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js">
  {lang: 'pt-BR'}
</script>

<!-- Place this tag where you want the +1 button to render. -->
<div class="g-plusone" data-size="medium" data-annotation="bubble" data-href="https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049"></div>

</div>

<div class="tbspace">
  <table width="100%" cellspacing="0">
  <tr>
    <td align="left" height="30" width="100px" valign="middle" nowrap="nowrap">
    <div class="buttons">
    
    <div class="reply-icon"><a href="http://app.falandodeviagem.com.br/fdv_profile?ref_url=http://www.falandodeviagem.com.br/posting.php?mode=reply|f=212|t=2049&amp;sid=5aa91c30d4923ae7dc55879a7590b259&c_url=1" title="Responder">Responder</a></div>
    
        </div>
    </td>
    
      <td class="gensmall" width="100%" align="right" nowrap="nowrap"><b><a style="background-color: #44ADF1;color: white;padding: 4px;border: 1px solid #C4FF96" href="#" onclick="jumpto(); return false;" title="Clique para ir à página...">Ir para página</a> <span class="onpagestyle"><strong>1</strong></span><span class="page-sep">, </span><a class="paginstyle" href="https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;start=10">2</a><span class="page-sep">, </span><a class="paginstyle" href="https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;start=20">3</a><span class="page-sep">, </span><a class="paginstyle" href="https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;start=30">4</a><span class="page-sep">, </span><a class="paginstyle" href="https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;start=40">5</a><span class="page-dots"> ... </span><a class="paginstyle" href="https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;start=270">28</a> &nbsp;<a style="background-color: #44ADF1;color: white;padding: 4px;border: 1px solid #C4FF96" href="https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;start=10">&raquo;</a></b> <br/><br/>
      &nbsp;<b>Página <b>1</b> de <b>28</b></b>&nbsp;[ 277 mensagens ]&nbsp;
      </td>
    
  </tr>
  </table>
</div>


<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="1" height="1"></td>
    <td height="1">&nbsp;</td>
    <td width="1" height="1"></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="1"></td>
    <td>
<div id="topic_global">

  <table class="tablebg" width="100%" cellspacing="0">
    <tr>
      <td>

  
  <div class="data-titulo" style="float: left;">&nbsp;Título: Benefícios MasterCard Black  <a name="unread"></a><a name="p14327"></a></div>
  <div class="data-titulo" style="float: right;"><a href="https://www.falandodeviagem.com.br/viewtopic.php?p=14327&amp;sid=5aa91c30d4923ae7dc55879a7590b259#p14327"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/pt_br/icon_post_target_unread.gif" width="12" height="9" alt="Mensagem não lida" title="Mensagem não lida"/></a>Enviado: Dom Ago 21, 2011 5:06 pm&nbsp;</div>

  <div class="detalhes">
    <div class="avatar"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABaCAIAAAC3ytZVAAALsElEQVR42u2cCVRTZxbHowMu44JarFitoEWttgKigiCLYEFlUUEdZtRq3Tq0KriNo8K01erR1upQRnAGaXUcHTzqOSN7wpZElrCKyJoEAmENIRsQEghZ5iYPaQgQQPNIMN7zDgcfyZf3fu9+//u/33sRIxv1yGWx8tlsmU4GZpQ/r1Eg2EZ4Rm5te4dDJpZKD5JyPNMIIonkHQ7ZL5VV5k9jj+cXyHQ1Rg9HJrNlVQJ2RWz85eISfcfBFYlAMgCHRVxCBLVS33GElFdAXthjk1bGJyY1Nuk1Dqgj67BJaxNxdoqfpTzeKHxot0TK7+6u7xC85HLTmUy4BtF19Y/ptfdp1f+uooWTKXCFrpeV/1Ra/k8K9T+06jQGg87no45DKpUF5uVbxSXYK1isT0qp7+hAo2ZVtrUnNzX9t7rm+5clX2bn/Ck90xtPdElKsU3EWccnWsUnfhobvzQ69qOnMSDnsPOLLNI3RS8fVtfksdi09vY2UTcMgjqOlCaGVVwisIDNJgHnhSe0d3drZORWkaiMx4OrHfyiaHdG1jpcMnwQaJNlXAKc/+oErE0Cdk0CFn63UOzZmJoGmCAXUpsYYH+0MFnAX+zJyLKOxyI44Jj+nJ37hmPS+R3/q607X1gEZCHdkPMHkYbfkU+BDS7+SgUFR1wyHMDP5WRiczOrs1PL2gEzFpkmyAbHDfn5ekPBFHtUQz+Sm+eES7ZUIICLb/dqZGRbq5gXQMEtJe1Ybj68HmaBBKarjkgpJCdcpd7DhQMFARvpIIUczqXiEuekFEAAoylnQe+2WlHCgc7+rOw7lVXNQqHOVZaXXJ6NopooZ8cTeu0w3w514WldPZweDAJvtB2IAgwOegEZ4Ut8FlpBrmht1d1Ce+FlsaXSTIFDB2F7zuYM+UamsPMWhbqNQERE0a4fBWRewF9hwBN5BXhGc0e3WKd9B0ModFYUOeUTAGGDyazmXezOLjAFXngieDY4VfvBQcAv5wpf5LBYY8OGgYwppwai9u4paeyurgFfD9X3bhXNIxVvobjmA4KwV0wNwAEgUPJyqOAAKYcSoCyisEEt/DwzS9xP52EPmEVfYrp6EPAnsFIwbBGHO8ZMOsyU9X1nCnJh//q8UOWVZbzWr3JyV8QlrB4chJ2iJG1MxT+soY/JngXX2KSSGkhZ+aG0TNlTQsuANHWDgUAqKLwAfGeTQChDP1DBcbm41KKvcCCmA9QBeUEWs2U7zI7YhAFNhDJBzzQCtrFxDHe0oAV7M0mr+iU/lMxcFhs8YhiZgrRVakDYKtz31zl5aPR7o4qjQSCAEqty2dcqykoBmx2QVwBFVH1SrFJ4jX9RqOKR+GsdxZHd0tI/NeD0XJNTN6fi1SsForhb8ERout+S5R9oGfoLB0LERm1SIPqyL5M0WPc9JnFAu2U5EI4hN2ABFaRDLJZpLzSPIzCvYMgZ0X8DQfmuqFgqk8q0GhrGIZJI/NIz1XiqgVnEJZwqeC6Sav9elIZxgB/dkJJqO5RGqJiLgLx8gVbnCFo4Sni8tX3XONRv4D780jPAocp0IzSMg9TSYj1s4YAkWotNesnlyXQmNIyD0Nw8fBzQod6nVct0KTSMI2mg5m0wyThd8FymY6EdHLY9K2P8dzh6HFc4mSLTvdAwDtwwcKxJwHrhibpTTVDEkcpgDInj09j4XyurZDoZGsaRwWSqryyQGlsJRG6XSC9wFHI4a/rdK1RRDZ1NDc3jqG7nQ8kYbHUH9jvhUupGd4FLmzjaRCLPNMJg6xogK0HlZJkOh4ZxQH9+iJQzsHzgkq2iY8/dudP2xndSx9J6x7nCF1YDLf844Ikrw25hMJjF5ubXrl2rrq7WCxw/lZUPuDjolJG16PCXmFdhZGS0Y8eO+/fvMxiMtxnHE3rtAIuDuGSHpNQZn3yC6RezZ8/29fWNjIyk0WhvIY4iLrf/cznrUvFrHkQZTJqEGTymT5++devWq1ev5ufnt7W1vSU4moRCl+RUlVoLwmF1Mxwz7FiwYAGkzMWLF4lEIp1Ol47WDRfN45BIpfuzslf1LS6OzzKWf3sB81oxZcqUlStX7tmzB+hgsdiysjKWph/rQBGHTHFvQUVNHdMzzY8GYDQRhoaGxsbG9vb2R48evXHjRlRUVEpKCoVC4fF4ncN4NlALOGLrG6z6ZodTepbZ/gMY1GLSpEkmJiZLliyxtrZ2dHT08fHx9/cPDg6+cuXK3xVx/fr1iIiItLS0rkGet0ERRw2f74BLUpYPwLHw4CGMlmLevHn79u178uRJTU2NehlCBYdIItmdkaV8pxYmy9KTp0eZwrJly06cOBETEyMY9l1OtJ4Nu97XjIGUWoaEjgKCyZMn29nZQbUmkUiika8woYWjgM1e/cp9OBCeORHTPz5zdty4cShRAC/n7e19+/ZtMpmsc1IK0SEW+xKf2abiIS+sI26/7+SMBoVZs2aBaoKjZTKZultoe9w6k/lx2C2TDZ/9ztBQsxTMzMwOHDgQHR3dqOnnpNDCAVPXx89vnIGBBilAET1z5kxmZma72md1dQtHVVUVGCRDDWXEhAkTrKysgoKCCASCBP2vm2I0C+LYsWMzZsx4cwozZ850dXUNDQ0tKSmRjuITYprBUVlZefLkSSMjozekAJ2bn5/fvXv36uvrx2RHy+Vyoci/YUbMnz9/9+7dDx8+1FZfrwEcHA4HQIDIvx4C8CAWFhZHjhzB4XDAdAyvhvH5fGiKFi5c+HogXFxcLly4UFBQINK9+5IjwyEWix88eABX9TXWLBwcHKC/zMnJeRtuLCAgoB0YEYU5c+aAawTvDFqrZvDuJhY/JYd5/ibd/WtO+COdxgEgoDUeEYjFixf7+/vDu1paWgYbVirsEuaVcm49btj3TdWKP1DmupGNXSgmn8FPxonrUkGnLuIgEolQ/4dJwdzc/PDhw/Hx8cJhfFFRzGmr8zlVZriGPHsD5cPNVDNP6kIv+WbmSZ61vn7nGRG9SYdwgAvcvn37kD2ogYGBjY3N+fPnwTt3jPDmq6SV33zyBuCgLtjcw+LVRp7tSrPe1ZGWq30c2dnZvr6+6ilMnDjRw8MjJCQEXOObOpdfnlLmb6bMdVchQvnAnfLhJk5olNZwVFRUQLaraTfAbrm7u4eFhZWWlmrwINoTM2mrdpGNXVWIUBd4wM5G/8viFu6o4iCTyQEBAVOnTh2QgqmpKbhGqCx1dXUoHUd3fXPD58Hk91zkOqJMxMwLdtY4HhCQikYDB/RdgYGB/UGAaixfvvzUqVPgGtFrqPtqiYT9cxT1oy1QX1Qnzlw3qqkn64e70m50n9XGTJs2TZnC+PHjwWUBIBKJ1K2h/1liRCHILalZfxiKC9XUQ3XizFrfsPdvYFJQxNF7n8LZ2fnSpUtFRUVaoaBacc7fhHJDNnFTrTjGrtW2e9vj09HCsXPnzsjISPWuUSshyCqiu38lT5MFHqoVx8St+UyIhC9AS0p1MyBNmMHh8qR4fwNo6m9QTOVWrdYrAHytHuFAQviCXP/Hs3Lz2tebkOfIFZcd8kCD+joGcCDR+ii5xvkQFN0+Fhb05T2Xuh1/6Syi6hcO+dxpF7RcjKg03wJtnnLdkU+lRVu4d6L1CwcSnWW0poAfkb6mt/ED8wb/hDLcWU7TLxw9glJY0XjwIuWDjQqV9ext/CqXbOPceqx3OJDoIOTX+52F0itvi2H6LII02QRTqfHAd13UWr3D0QMFn9fwxbfyhRJjV4pCaOVpstSHE/5IKurWOxw906egnBF4rWr5dqQPlKuJsUud9/GO9EJ9xIGEiNbADAqrsvSTZ4qJm9ytzN/EunpH0iHURxw9ywVNLN7dmFrv48CCPNO5wsgJ/D4fR9JTHD0hlQqyi5lBN2nWu8p/b1cx3bHx8PeiWoa+4uhNFgaL+2t03fbT4Osrzbdywh9LB/m6lV7g6I0uCp3zj4f0TUehCeIn58j6PSGhXzh+s7Yllbx7ce0xRDGnz5dr/g/OV6M7eDtYsQAAAABJRU5ErkJggg==" alt="Avatar do usuário"/></div>
    <div class="autor">
      <b class="postauthor" style="color: #AA0000">falandodeviagem</b>
      <span style="margin-left: 100px;"><b>Mensagens:</b> 18048</span><div style="float: right;">Administrador</div>
    </div>
  </div>

  
    <div class="profile-icons">
      <a href="https://www.falandodeviagem.com.br/memberlist.php?mode=viewprofile&amp;u=2&amp;sid=5aa91c30d4923ae7dc55879a7590b259"></a>
    </div>
  

  <div class="postbody"><span style="font-size: 150%; line-height: normal"><strong>Benefícios MasterCard Black</strong></span><br/><br/><img src="https://www.falandodeviagem.com.br/imagens20/MasterCardBlack.jpg" alt="O MasterCard Black é o melhor cartão disponível pela bandeira no mercado brasileiro." title="O MasterCard Black é o melhor cartão disponível pela bandeira no mercado brasileiro." class="img_postbody"/><br/><br/><strong>Site</strong>: <a href="http://www.mastercard.com/affluent/br/black" onclick="window.open(this.href);return false;" class="postlink">http://www.mastercard.com/affluent/br/black</a><br/><br/>As bandeiras oferecem diversos benefícios exclusivos para os seus associados. Na maioria das vezes os melhores benefícios são oferecidos por ela e não pelo banco emissor. É importante conhecer todos os benefícios e serviços agregados, para que você aproveite melhor seu cartão de crédito, principalmente em viagens.<br/><br/>O <strong>MasterCard Black</strong> é o melhor cartão de crédito da bandeira oferecido no <strong>Brasil</strong>.<br/><br/>Atualmente esses bancos são os únicos emissores do cartão: <strong>Bradesco</strong>, <strong>Banco do Brasil</strong>, <strong>Caixa</strong>, <strong>Itaú</strong>, <strong>Citibank</strong>, <strong>Safra</strong> e <strong>Santander</strong>. Apenas na <strong>Caixa</strong> não é necessário ser correntista do banco.<br/><br/>Tanto faz você ter um <strong>Bradesco MasterCard Black</strong> ou um <strong>Santander MasterCard Unlimited Black</strong>, pois todos os benefícios abaixo serão válidos.<br/><br/>Existe apenas um benefício que só o <strong>Santander</strong> disponibiliza: O <strong>Priority Pass</strong>.<br/><br/>- <strong>MasterCard Global Service</strong>: Central de atendimento global para auxílio a portadores em viagem, disponível a qualquer hora, em qualquer lugar, em qualquer idioma. É para lá que você deve ligar no caso de roubo/perda do seu cartão.<br/><br/><strong>Telefone no Brasil</strong>: 0800-891-3294.<br/><strong>Telefone nos Estados Unidos e Canadá</strong>: 1-800-307-7309.<br/><br/>Para os demais países, consulte o site oficial.<br/><br/>Nós já precisamos solicitar um cartão de crédito emergencial em <strong>Miami</strong> e foi tudo rápido e muito bom, com atendimento em português e sem custos. Clique <strong><a href="http://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=981" onclick="window.open(this.href);return false;" class="postlink">aqui</a></strong> para ler o relato completo.<br/><br/>- <strong>MasterSeguro de Viagens</strong>: Comprando as passagens com o seu cartão, você conta com a exclusiva cobertura terrestre, marítima ou aérea do MasterSeguro de Viagens.<br/><br/>Válido para você, seu cônjuge, filhos solteiros, menores de 23 anos, enteados ou filhos adotados legalmente. Não é necessário que todos estejam viajando juntos para contar com a cobertura. O plano pagará benefícios relacionados a qualquer sinistro ocorrido até 365 dias após a data do acidente coberto.<br/><br/>- <strong>MasterAssist Black</strong>: Os portadores de cartões <strong>MasterCard Black</strong> e membros da família podem se beneficiar da proteção e segurança oferecidos através do nosso programa de medicina em viagens. Alguns dos benefícios fornecidos são: Despesas Médicas, Evacuação Médica de Emergência, Repatriação de Restos Mortais, Retorno de Dependente e Idoso, Serviço de Transporte VIP e Viagem da Família em situação de Emergência.<br/><br/>Disponível 24 horas por dia / 7 dias por semana / 365 dias por ano.<br/>A cobertura é válida para viagens em todo o mundo.<br/><br/>- <strong>MasterSeguro de Automóveis</strong>: Quando você aluga e paga um veículo usando o seu cartão, será fornecida cobertura para danos ao carro alugado causados por colisão, roubo e/ou incêndio acidental.<br/><br/>Valor máximo: US$75.000.<br/>Tempo máximo: 31 dias.<br/><br/>Para ter direito a esta cobertura, você deve recusar a opção de Isenção de Responsabilidade em Caso de Danos por Colisão (CDW/LDW) oferecido pela locadora. O seguro contra terceiros não é coberto e você pode contratá-lo adicionalmente, caso deseje.<br/><br/>Clique <strong><a href="http://www.mastercard.com/affluent/br/black/#!/offer/869554" onclick="window.open(this.href);return false;" class="postlink">aqui</a></strong> para ler as informações completas no site oficial.<br/><br/>- <strong>Seguro caixa eletrônico</strong>: Proteção contra roubo ou assalto ao utilizar caixas eletrônicos em qualquer lugar do mundo. Valor máximo de US$500.<br/><br/>- <strong>Resgate médico</strong>: Em caso de acidente durante uma viagem em que haja necessidade de Evacuação Médica de Emergência, ou até mesmo repatriação, você tem acesso a uma cobertura de até US$100.000.<br/><br/>- <strong>Certificado de Schengen</strong>: Para entrar na maioria dos países da <strong><a href="http://www.falandodeviagem.com.br/viewforum.php?f=10" onclick="window.open(this.href);return false;" class="postlink">Europa</a></strong>, é obrigatório um seguro de, pelo menos, €30.000, que garanta assistência médica por doença ou acidente. Este seguro é automaticamente oferecido a você, seu cônjuge e filhos dependentes de até 23 anos, desde que as passagens tenham sido compradas com o cartão.<br/><br/>Clique <strong><a href="http://www.falandodeviagem.com.br/viewtopic.php?f=10&amp;t=919" onclick="window.open(this.href);return false;" class="postlink">aqui</a></strong> para entender como funciona o <strong>Tratado de Schengen</strong>.<br/><br/>Clique <strong><a href="http://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=951" onclick="window.open(this.href);return false;" class="postlink">aqui</a></strong> para ler os procedimentos de emissão do certificado.<br/><br/>- <strong>Sala Vip MasterCard Black</strong>: Todos os cartões Black oferecem acesso à <a href="http://www.falandodeviagem.com.br/viewforum.php?f=131" onclick="window.open(this.href);return false;" class="postlink"><strong>sala vip</strong></a> no <strong>Aeroporto de Guarulhos</strong>, em <a href="http://www.falandodeviagem.com.br/viewforum.php?f=92" onclick="window.open(this.href);return false;" class="postlink"><strong>São Paulo</strong></a>. A <a href="http://www.falandodeviagem.com.br/viewforum.php?f=131" onclick="window.open(this.href);return false;" class="postlink"><strong>sala vip</a></strong> está localizada no 2º andar da Asa A do Terminal 1. Se você estiver no Terminal 2 terá que andar bastante para chegar a <a href="http://www.falandodeviagem.com.br/viewforum.php?f=131" onclick="window.open(this.href);return false;" class="postlink"><strong>sala vip</strong></a>.<br/><br/>Sugerimos a leitura: <strong><a href="http://www.falandodeviagem.com.br/viewtopic.php?f=131&amp;t=2450" onclick="window.open(this.href);return false;" class="postlink">Sala vip MasterCard Black - Aeroporto de Guarulhos</a></strong><br/><br/>- <strong>Salas vip Priority Pass</strong>: É o maior programa independente de salas vips no mundo. Você pode usá-las independentemente da companhia aérea e classe pela qual esteja voando e com direito a um acompanhante gratuitamente.<br/><br/>Este benefício é oferecido apenas pelo <strong><a href="http://www.falandodeviagem.com.br/viewtopic.php?f=4&amp;t=260" onclick="window.open(this.href);return false;" class="postlink">Santander Unlimited MasterCard Black</a></strong> e <a href="http://www.falandodeviagem.com.br/viewtopic.php?f=458&amp;t=6837" onclick="window.open(this.href);return false;" class="postlink"><strong>Porto Seguro Visa Infinite</strong></a>.<br/><br/>- <strong>Black Events</strong>: Eventos exclusivos e diferenciados, no qual os associados receberão convites para participar. Não são eventos gratuitos geralmente, mas sim exclusivos e selecionados a um público de alta renda.<br/><br/>- <strong>Privilégios Aventura Mall</strong>: Descontos e serviços especiais no <strong><a href="http://www.falandodeviagem.com.br/viewtopic.php?f=126&amp;t=963" onclick="window.open(this.href);return false;" class="postlink">Shopping Aventura Mall</a></strong>, em <strong><a href="http://www.falandodeviagem.com.br/viewforum.php?f=126" onclick="window.open(this.href);return false;" class="postlink">Miami</a></strong>.<br/><br/>- <strong>MasterCard Concierge</strong>: Atendimento personalizado para lhe ajudar em tudo. Através deste serviço, com uma ligação, você pode solicitar reservas em espetáculos e restaurantes, comprar produtos ou solicitar serviços.<br/><br/>Nós testamos os serviço do Concierge em 2010, 2011 e 2012 Sugerimos a leitura:<br/>- <strong><a href="http://www.falandodeviagem.com.br/viewtopic.php?f=4&amp;t=398" onclick="window.open(this.href);return false;" class="postlink">Concierge 2010: Testamos os principais cartões</a></strong><br/>- <strong><a href="http://www.falandodeviagem.com.br/viewtopic.php?f=4&amp;t=2418" onclick="window.open(this.href);return false;" class="postlink">Concierge 2011: Testamos os principais cartões</a></strong> <br/>- <strong><a href="http://www.falandodeviagem.com.br/viewtopic.php?f=250&amp;t=4971" onclick="window.open(this.href);return false;" class="postlink">Concierge 2012: Testamos os principais cartões</a></strong> <br/><br/><strong>Anote o telefone</strong>: 0800-891-3294.<br/><br/>- <strong>MasterCard Experiences and Offers</strong>: É um programa lhe abre as portas a uma enorme gama de ofertas valiosas, upgrades, experiências e acesso preferencial destinadas a atender o seu estilo de vida.<br/><br/><strong>Site dos benefícios para compras</strong>: <a href="http://www.mastercard.com/affluent/br/black/#!/category/buy" onclick="window.open(this.href);return false;" class="postlink">http://www.mastercard.com/affluent/br/black/#!/category/buy</a><br/><strong>Site dos benefícios em viagens</strong>: <a href="http://www.mastercard.com/affluent/br/black/#!/category/travel" onclick="window.open(this.href);return false;" class="postlink">http://www.mastercard.com/affluent/br/black/#!/category/travel</a><br/><strong>Site dos benefícios para diversão</strong>: <a href="http://www.mastercard.com/affluent/br/black/#!/category/going_out" onclick="window.open(this.href);return false;" class="postlink">http://www.mastercard.com/affluent/br/black/#!/category/going_out</a><br/><br/>- <strong><a href="http://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=1322" onclick="window.open(this.href);return false;" class="postlink">Priceless Miami</a></strong>: É um programa de benefícios do <strong>MasterCard</strong> para os cartões de crédito premium da marca. Através desse serviço você terá benefícios e descontos em diversos estabelecimentos de <strong><a href="http://www.falandodeviagem.com.br/viewforum.php?f=126" onclick="window.open(this.href);return false;" class="postlink">Miami</a></strong>.<br/><br/>Nós escrevemos sobre ele <strong><a href="http://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=1322" onclick="window.open(this.href);return false;" class="postlink">aqui</a></strong> e sugerimos a leitura.<br/><br/>- <strong>MasterCard Surpreenda</strong>: Programa de benefícios, onde você junta pontos e troca por prêmios. Você paga 1 e ganha outro inteiramente grátis. Associados <strong>MasterCard Black</strong> tem acesso a benefícios exclusivo para trocas.<br/><br/><strong>Site</strong>: <a href="http://www.naotempreco.com.br" onclick="window.open(this.href);return false;" class="postlink">http://www.naotempreco.com.br</a><br/><br/><strong>Guia de benefícios em PDF</strong><br/><br/>O <strong>MasterCard</strong> fornece um amplo e completo <strong>PDF</strong> com as informações de todos os benefícios detalhadas. São 24 páginas, com tudo que você precisa saber sobre os benefícios e condições do seu <strong>MasterCard Black</strong>.<br/><br/>Mesmo que você não leia agora, guarde-o para consultas futuras.<br/><br/>Clique <strong><a href="http://www.falandodeviagem.com.br/MasterCardBlack.pdf" onclick="window.open(this.href);return false;" class="postlink">aqui</a></strong> para realizar o download.<br/><br/><strong>Matérias</strong><br/><br/>Nós já escrevemos matérias sobre alguns cartões <strong>MasterCard Black</strong> e sugerimos a leitura.<br/>- <strong><a href="http://www.falandodeviagem.com.br/viewtopic.php?f=4&amp;t=260" onclick="window.open(this.href);return false;" class="postlink">Santander Unlimited MasterCard Black</a></strong><br/>- <strong><a href="http://www.falandodeviagem.com.br/viewtopic.php?f=4&amp;t=75" onclick="window.open(this.href);return false;" class="postlink">Bradesco MasterCard Black</a></strong><br/>- <strong><a href="http://www.falandodeviagem.com.br/viewtopic.php?f=4&amp;t=918" onclick="window.open(this.href);return false;" class="postlink">Citibank AAdvantage Black MasterCard</a></strong><br/>- <strong><a href="http://www.falandodeviagem.com.br/viewtopic.php?f=4&amp;t=2054" onclick="window.open(this.href);return false;" class="postlink">Itaú Personnalité MasterCard Black</a></strong><br/>- <strong><a href="http://www.falandodeviagem.com.br/viewtopic.php?f=327&amp;t=3644" onclick="window.open(this.href);return false;" class="postlink">Safra MasterCard Black</a></strong><br/><br/>E você, tem algum cartão <strong>MasterCard Black</strong>? Já usou algum benefício? Está satisfeito? Conte para nós a sua experiência!<br/><br/><strong><a href="https://fdv.im/MaiorCobertura" rel="nofollow" onclick="window.open(this.href);return false;" class="postlink"><span style="color: #FF0054">Contrate aqui o seguro viagem bom, barato e com a maior e melhor cobertura do mundo &#40;</span><span style="color: #255D82">2 milhões de dólares</span><span style="color: #FF0054">&#41;.</span><br/><span style="color: #255D82">Busque aqui: </span><span style="color: #39B9BC">https://fdv.im/MaiorCobertura</span></a></strong><br/><br/><strong><a href="https://fdv.im/VoosMaisBaratos" rel="nofollow" onclick="window.open(this.href);return false;" class="postlink"><span style="color: #FF0054">Buscando Voos mais baratos? Aqui você os encontrará!<br/></span><span style="color: #255D82">Busque aqui: </span><span style="color: #39B9BC">https://fdv.im/VoosMaisBaratos</span></a></strong><br/><br/><strong><a href="https://fdv.im/PasseiosWA" rel="nofollow" onclick="window.open(this.href);return false;" class="postlink"><span style="color: #FF0054">Economize e reserve seus passeios com antecedência<br/></span><span style="color: #255D82">Acesse: </span><span style="color: #39B9BC">https://fdv.im/PasseiosWA</span></a></strong><br/><br/><div class="remover-rodape"><img src="https://www.falandodeviagem.com.br/imagens19/rodape1.png" alt="Imagem" class="img_postbody"/><br/><a href="https://fdv.im/VoosFDV" rel="nofollow" onclick="window.open(this.href);return false;" class="postlink"><img src="https://www.falandodeviagem.com.br/imagens19/rodape2.png" alt="Imagem" class="img_postbody"/></a><a href="https://fdv.im/BookingRodape" rel="nofollow" onclick="window.open(this.href);return false;" class="postlink"><img src="https://www.falandodeviagem.com.br/imagens19/rodape3.png" alt="Imagem" class="img_postbody"/></a><a href="https://fdv.im/RodapeCarro" rel="nofollow" onclick="window.open(this.href);return false;" class="postlink"><img src="https://www.falandodeviagem.com.br/imagens19/rodape4.png" alt="Imagem" class="img_postbody"/></a><a href="https://fdv.im/CompareSeguroViagem" rel="nofollow" onclick="window.open(this.href);return false;" class="postlink"><img src="https://www.falandodeviagem.com.br/imagens19/rodape5.png" alt="Imagem" class="img_postbody"/></a><a href="https://fdv.im/IngressosInt" rel="nofollow" onclick="window.open(this.href);return false;" class="postlink"><img src="https://www.falandodeviagem.com.br/imagens19/rodape6.png" alt="Imagem" class="img_postbody"/></a></div></div>

  <!-- EXIBIR ASSINATURA APENAS DE ADMINISTRADORES -->
      <div class="postbody"><br/>_________________<br/><span style="font-size: 150%; line-height: normal">Confira também as últimas matérias publicadas no FDV: <strong><a href="https://fdv.im/novidades" onclick="window.open(this.href);return false;" class="postlink">https://fdv.im/novidades</a></strong></span></div>
    <br clear="all"/><hr/><!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) - Start //--><tr id="sponsors_topics" class="row1">
	
	<td align="center" valign="middle" colspan="2">
	
			<span id="sponsor"><a href="https://www.falandodeviagem.com.br/redirect.php?id=7&amp;sid=5aa91c30d4923ae7dc55879a7590b259" title="Tokio Marine - Seguro viagem" onclick="window.open(this.href);return false;"><img src="http://www.falandodeviagem.com.br/imagens%20-%20B/banners/tokiomarine%20banner%20entrepost%20750x100.jpg" alt="Tokio Marine - Seguro viagem" title="Tokio Marine - Seguro viagem"/></a></span>
	
	</td>
</tr>
<tr>
	<td colspan="2" height="1"><hr/></td>
</tr><!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) - End //-->
      
      </td>
    </tr>
  </table>


  <table class="tablebg" width="100%" cellspacing="0">
    <tr>
      <td>

  
  <div class="data-titulo" style="float: left;">&nbsp;Título: Re: Benefícios MasterCard Black  <a name="p23210"></a></div>
  <div class="data-titulo" style="float: right;"><a href="https://www.falandodeviagem.com.br/viewtopic.php?p=23210&amp;sid=5aa91c30d4923ae7dc55879a7590b259#p23210"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/pt_br/icon_post_target_unread.gif" width="12" height="9" alt="Mensagem não lida" title="Mensagem não lida"/></a>Enviado: Sáb Jan 21, 2012 8:22 pm&nbsp;</div>

  <div class="detalhes">
    <div class="avatar"><img src="styles/new_beach/theme/images/user_avatar.png.pagespeed.ce.tlEGucAxYo.png" alt=""/></div>
    <div class="autor">
      <b class="postauthor">leecat</b>
      <span style="margin-left: 100px;"><b>Mensagens:</b> 116</span><span style="margin-left: 100px;"><b>Rio de Janeiro</b></span>
    </div>
  </div>

  
    <div class="profile-icons">
      <a href="https://www.falandodeviagem.com.br/memberlist.php?mode=viewprofile&amp;u=10783&amp;sid=5aa91c30d4923ae7dc55879a7590b259"></a>
    </div>
  

  <div class="postbody">Pessoal, vale a pena tentar pedir um Mastercard Black no lugar do Platinum no meu caso que já tenho o Visa Infinite? pelo que eu saiba são cartões bem semelhantes...</div>

  <!-- EXIBIR ASSINATURA APENAS DE ADMINISTRADORES --><br clear="all"/><hr/><!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) - Start //--><!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) - End //-->
      
      </td>
    </tr>
  </table>


  <table class="tablebg" width="100%" cellspacing="0">
    <tr>
      <td>

  
  <div class="data-titulo" style="float: left;">&nbsp;Título: Re: Benefícios MasterCard Black  <a name="p23212"></a></div>
  <div class="data-titulo" style="float: right;"><a href="https://www.falandodeviagem.com.br/viewtopic.php?p=23212&amp;sid=5aa91c30d4923ae7dc55879a7590b259#p23212"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/pt_br/icon_post_target_unread.gif" width="12" height="9" alt="Mensagem não lida" title="Mensagem não lida"/></a>Enviado: Sáb Jan 21, 2012 8:25 pm&nbsp;</div>

  <div class="detalhes">
    <div class="avatar"><img src="download/xfile.php,qavatar=55_1517658012.jpg.pagespeed.ic.soOm_u-H8w.jpg" width="90" height="90" alt="Avatar do usuário"/></div>
    <div class="autor">
      <b class="postauthor" style="color: #AA0000">GabrielDias</b>
      <span style="margin-left: 100px;"><b>Mensagens:</b> 35264</span>
    </div>
  </div>

  
    <div class="profile-icons">
      <a href="https://www.falandodeviagem.com.br/memberlist.php?mode=viewprofile&amp;u=55&amp;sid=5aa91c30d4923ae7dc55879a7590b259"></a>
    </div>
  

  <div class="postbody">Claro que vale. Principalmente porque não deverão te cobrar as duas anuidades. Sendo assim, por que não ter ambos?</div>

  <!-- EXIBIR ASSINATURA APENAS DE ADMINISTRADORES -->
      <div class="postbody"><br/>_________________<br/><!-- m --><a class="postlink" href="https://fdv.im/GabrielDias" onclick="window.open(this.href);return false;">https://fdv.im/GabrielDias</a><!-- m --></div>
    <br clear="all"/><hr/><!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) - Start //--><!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) - End //-->
      
      </td>
    </tr>
  </table>


  <table class="tablebg" width="100%" cellspacing="0">
    <tr>
      <td>

  
  <div class="data-titulo" style="float: left;">&nbsp;Título: Re: Benefícios MasterCard Black  <a name="p23226"></a></div>
  <div class="data-titulo" style="float: right;"><a href="https://www.falandodeviagem.com.br/viewtopic.php?p=23226&amp;sid=5aa91c30d4923ae7dc55879a7590b259#p23226"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/pt_br/icon_post_target_unread.gif" width="12" height="9" alt="Mensagem não lida" title="Mensagem não lida"/></a>Enviado: Sáb Jan 21, 2012 10:01 pm&nbsp;</div>

  <div class="detalhes">
    <div class="avatar"><img src="download/xfile.php,qavatar=3039_1305552862.jpg.pagespeed.ic.1Oni3JPnEy.jpg" width="90" height="62" alt="Avatar do usuário"/></div>
    <div class="autor">
      <b class="postauthor" style="color: #FF9900">gobbato</b>
      <span style="margin-left: 100px;"><b>Mensagens:</b> 1560</span>
    </div>
  </div>

  
    <div class="profile-icons">
      <a href="https://www.falandodeviagem.com.br/memberlist.php?mode=viewprofile&amp;u=3039&amp;sid=5aa91c30d4923ae7dc55879a7590b259"></a>
    </div>
  

  <div class="postbody">Realmente o Santander inovou! Este cartãozinho PriorityPass é demais mesmo!!<br/><br/>Com certeza quem puder ter o Black terá o melhor cartão da atualidade.</div>

  <!-- EXIBIR ASSINATURA APENAS DE ADMINISTRADORES --><br clear="all"/><hr/><!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) - Start //--><!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) - End //-->
      
      </td>
    </tr>
  </table>


  <table class="tablebg" width="100%" cellspacing="0">
    <tr>
      <td>

  
  <div class="data-titulo" style="float: left;">&nbsp;Título: Re: Benefícios MasterCard Black  <a name="p25140"></a></div>
  <div class="data-titulo" style="float: right;"><a href="https://www.falandodeviagem.com.br/viewtopic.php?p=25140&amp;sid=5aa91c30d4923ae7dc55879a7590b259#p25140"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/pt_br/icon_post_target_unread.gif" width="12" height="9" alt="Mensagem não lida" title="Mensagem não lida"/></a>Enviado: Ter Fev 07, 2012 1:21 am&nbsp;</div>

  <div class="detalhes">
    <div class="avatar"><img src="styles/new_beach/theme/images/user_avatar.png.pagespeed.ce.tlEGucAxYo.png" alt=""/></div>
    <div class="autor">
      <b class="postauthor">leecat</b>
      <span style="margin-left: 100px;"><b>Mensagens:</b> 116</span><span style="margin-left: 100px;"><b>Rio de Janeiro</b></span>
    </div>
  </div>

  
    <div class="profile-icons">
      <a href="https://www.falandodeviagem.com.br/memberlist.php?mode=viewprofile&amp;u=10783&amp;sid=5aa91c30d4923ae7dc55879a7590b259"></a>
    </div>
  

  <div class="postbody">Mas onde que consta essa informação? eu mandei um e-mail pra minha gerente pedindo o Black, ela disse que normalmente alguns clientes recebem 100% de desconto na anuidade, mas não sei se cobram somente uma anuidade pra quem tem o Infinite e o Black...<br/><br/>Todo caso, o 0800 do Infinite é o mesmo do Black, não? pq no Prime é um unico 0800 tanto pro Infinite qnt o Platinum. o Atendimento é o mesmo. Acredito que desta maneira seja mais fácil negociar...</div>

  <!-- EXIBIR ASSINATURA APENAS DE ADMINISTRADORES --><br clear="all"/><hr/><!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) - Start //--><!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) - End //-->
      
      </td>
    </tr>
  </table>


  <table class="tablebg" width="100%" cellspacing="0">
    <tr>
      <td>

  
  <div class="data-titulo" style="float: left;">&nbsp;Título: Re: Benefícios MasterCard Black  <a name="p25141"></a></div>
  <div class="data-titulo" style="float: right;"><a href="https://www.falandodeviagem.com.br/viewtopic.php?p=25141&amp;sid=5aa91c30d4923ae7dc55879a7590b259#p25141"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/pt_br/icon_post_target_unread.gif" width="12" height="9" alt="Mensagem não lida" title="Mensagem não lida"/></a>Enviado: Ter Fev 07, 2012 1:25 am&nbsp;</div>

  <div class="detalhes">
    <div class="avatar"><img src="download/xfile.php,qavatar=55_1517658012.jpg.pagespeed.ic.soOm_u-H8w.jpg" width="90" height="90" alt="Avatar do usuário"/></div>
    <div class="autor">
      <b class="postauthor" style="color: #AA0000">GabrielDias</b>
      <span style="margin-left: 100px;"><b>Mensagens:</b> 35264</span>
    </div>
  </div>

  
    <div class="profile-icons">
      <a href="https://www.falandodeviagem.com.br/memberlist.php?mode=viewprofile&amp;u=55&amp;sid=5aa91c30d4923ae7dc55879a7590b259"></a>
    </div>
  

  <div class="postbody">Eu já vi a propaganda oficial, que minha gerente me mostrou. E, além disto, nunca recebi a anuidade do Visa Infinite.<br/><br/>O 0800 é diferente. As equipes são diferentes, até do Concierge. Pode ser a mesma empresa, mas uma equipe cuida do Visa e outra do MasterCard.</div>

  <!-- EXIBIR ASSINATURA APENAS DE ADMINISTRADORES -->
      <div class="postbody"><br/>_________________<br/><!-- m --><a class="postlink" href="https://fdv.im/GabrielDias" onclick="window.open(this.href);return false;">https://fdv.im/GabrielDias</a><!-- m --></div>
    <br clear="all"/><hr/><!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) - Start //--><!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) - End //-->
      
      </td>
    </tr>
  </table>


  <table class="tablebg" width="100%" cellspacing="0">
    <tr>
      <td>

  
  <div class="data-titulo" style="float: left;">&nbsp;Título: Re: Benefícios MasterCard Black  <a name="p25151"></a></div>
  <div class="data-titulo" style="float: right;"><a href="https://www.falandodeviagem.com.br/viewtopic.php?p=25151&amp;sid=5aa91c30d4923ae7dc55879a7590b259#p25151"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/pt_br/icon_post_target_unread.gif" width="12" height="9" alt="Mensagem não lida" title="Mensagem não lida"/></a>Enviado: Ter Fev 07, 2012 2:12 am&nbsp;</div>

  <div class="detalhes">
    <div class="avatar"><img src="styles/new_beach/theme/images/user_avatar.png.pagespeed.ce.tlEGucAxYo.png" alt=""/></div>
    <div class="autor">
      <b class="postauthor">leecat</b>
      <span style="margin-left: 100px;"><b>Mensagens:</b> 116</span><span style="margin-left: 100px;"><b>Rio de Janeiro</b></span>
    </div>
  </div>

  
    <div class="profile-icons">
      <a href="https://www.falandodeviagem.com.br/memberlist.php?mode=viewprofile&amp;u=10783&amp;sid=5aa91c30d4923ae7dc55879a7590b259"></a>
    </div>
  

  <div class="postbody">Bom, no meu caso é o mesmo número tanto pro Mastercard Platinum qnt do Visa Infinite, só o do Black que deve ser diferente então...<br/><br/>vamos ver primeiro se eu consigo o Black...</div>

  <!-- EXIBIR ASSINATURA APENAS DE ADMINISTRADORES --><br clear="all"/><hr/><!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) - Start //--><!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) - End //-->
      
      </td>
    </tr>
  </table>


  <table class="tablebg" width="100%" cellspacing="0">
    <tr>
      <td>

  
  <div class="data-titulo" style="float: left;">&nbsp;Título: Re: Benefícios MasterCard Black  <a name="p26817"></a></div>
  <div class="data-titulo" style="float: right;"><a href="https://www.falandodeviagem.com.br/viewtopic.php?p=26817&amp;sid=5aa91c30d4923ae7dc55879a7590b259#p26817"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/pt_br/icon_post_target_unread.gif" width="12" height="9" alt="Mensagem não lida" title="Mensagem não lida"/></a>Enviado: Seg Fev 20, 2012 7:45 pm&nbsp;</div>

  <div class="detalhes">
    <div class="avatar"><img src="download/xfile.php,qavatar=9415_1395465667.jpg.pagespeed.ic.2GCeTLsn_O.jpg" width="90" height="85" alt="Avatar do usuário"/></div>
    <div class="autor">
      <b class="postauthor" style="color: #FF9900">ZedoPalito</b>
      <span style="margin-left: 100px;"><b>Mensagens:</b> 2102</span>
    </div>
  </div>

  
    <div class="profile-icons">
      <a href="https://www.falandodeviagem.com.br/memberlist.php?mode=viewprofile&amp;u=9415&amp;sid=5aa91c30d4923ae7dc55879a7590b259"></a>
    </div>
  

  <div class="postbody">Alguem saberia me informar se o plastico do MC Black mudou seu visual? Os sites dos bancos que ja ofereciam o cartão anteriormente a CEF, mostram de uma forma diferente o visual que a CEF e o site do MC black mostra. Sera que mudou?<br/><br/><img src="http://www.caixa.gov.br/voce/resources/dst_p_cartaoblack.jpg" alt="Imagem" class="img_postbody"/><br/><br/><img src="http://www.bradescoprime.com.br/static_files/Prime/saiba_tudo_sobre_cartoes_cartao_de_credito/img_black.jpg" alt="Imagem" class="img_postbody"/><br/><br/>O Santander tb mudou a cara do cartão, mostra como o no site do <a href="http://www.mastercard.com/affluent/br/black/" onclick="window.open(this.href);return false;" class="postlink">http://www.mastercard.com/affluent/br/black/</a><br/><br/><img src="http://www.santander.com.br/document/wps/det_santander_unlimited_black.jpg" alt="Imagem" class="img_postbody"/><br/><br/>BB não mudou tb:<br/><br/><img src="http://www.bb.com.br/portalbb/img.ImgWriter?codigo=16749&amp;origem=cci" alt="Imagem" class="img_postbody"/></div>

  <!-- EXIBIR ASSINATURA APENAS DE ADMINISTRADORES --><br clear="all"/><hr/><!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) - Start //--><!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) - End //-->
      
      </td>
    </tr>
  </table>


  <table class="tablebg" width="100%" cellspacing="0">
    <tr>
      <td>

  
  <div class="data-titulo" style="float: left;">&nbsp;Título: Re: Benefícios MasterCard Black  <a name="p26822"></a></div>
  <div class="data-titulo" style="float: right;"><a href="https://www.falandodeviagem.com.br/viewtopic.php?p=26822&amp;sid=5aa91c30d4923ae7dc55879a7590b259#p26822"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/pt_br/icon_post_target_unread.gif" width="12" height="9" alt="Mensagem não lida" title="Mensagem não lida"/></a>Enviado: Seg Fev 20, 2012 7:54 pm&nbsp;</div>

  <div class="detalhes">
    <div class="avatar"><img src="download/xfile.php,qavatar=55_1517658012.jpg.pagespeed.ic.soOm_u-H8w.jpg" width="90" height="90" alt="Avatar do usuário"/></div>
    <div class="autor">
      <b class="postauthor" style="color: #AA0000">GabrielDias</b>
      <span style="margin-left: 100px;"><b>Mensagens:</b> 35264</span>
    </div>
  </div>

  
    <div class="profile-icons">
      <a href="https://www.falandodeviagem.com.br/memberlist.php?mode=viewprofile&amp;u=55&amp;sid=5aa91c30d4923ae7dc55879a7590b259"></a>
    </div>
  

  <div class="postbody">Eu acho que cada banco define o seu layout.</div>

  <!-- EXIBIR ASSINATURA APENAS DE ADMINISTRADORES -->
      <div class="postbody"><br/>_________________<br/><!-- m --><a class="postlink" href="https://fdv.im/GabrielDias" onclick="window.open(this.href);return false;">https://fdv.im/GabrielDias</a><!-- m --></div>
    <br clear="all"/><hr/><!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) - Start //--><!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) - End //-->
      
      </td>
    </tr>
  </table>


  <table class="tablebg" width="100%" cellspacing="0">
    <tr>
      <td>

  
  <div class="data-titulo" style="float: left;">&nbsp;Título: Re: Benefícios MasterCard Black  <a name="p26826"></a></div>
  <div class="data-titulo" style="float: right;"><a href="https://www.falandodeviagem.com.br/viewtopic.php?p=26826&amp;sid=5aa91c30d4923ae7dc55879a7590b259#p26826"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/pt_br/icon_post_target_unread.gif" width="12" height="9" alt="Mensagem não lida" title="Mensagem não lida"/></a>Enviado: Seg Fev 20, 2012 7:57 pm&nbsp;</div>

  <div class="detalhes">
    <div class="avatar"><img src="styles/new_beach/theme/images/user_avatar.png.pagespeed.ce.tlEGucAxYo.png" alt=""/></div>
    <div class="autor">
      <b class="postauthor" style="color: #FF9900">viajante</b>
      <span style="margin-left: 100px;"><b>Mensagens:</b> 1059</span>
    </div>
  </div>

  
    <div class="profile-icons">
      <a href="https://www.falandodeviagem.com.br/memberlist.php?mode=viewprofile&amp;u=247&amp;sid=5aa91c30d4923ae7dc55879a7590b259"></a>
    </div>
  

  <div class="postbody">Esse logo do mastercard prateado ficou muito melhor.</div>

  <!-- EXIBIR ASSINATURA APENAS DE ADMINISTRADORES --><br clear="all"/><!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) - Start //--><!-- MOD : MSSTI Full Sponsored Forum (v1.0.1) - End //-->
      
      </td>
    </tr>
  </table>


</div>

<div class="tbspace">
  <table width="100%" cellspacing="0">
  <tr>
    <td align="left" height="35" width="100" valign="middle" nowrap="nowrap">
    <div class="buttons">
    
    <div class="reply-icon"><a href="http://app.falandodeviagem.com.br/fdv_profile?ref_url=http://www.falandodeviagem.com.br/posting.php?mode=reply|f=212|t=2049&amp;sid=5aa91c30d4923ae7dc55879a7590b259&c_url=1" title="Responder">Responder</a></div>
    
        </div>
    </td>
        
      <td class="gensmall" width="100%" align="right" nowrap="nowrap"><b><a style="background-color: #44ADF1;color: white;padding: 4px;border: 1px solid #C4FF96" href="#" onclick="jumpto(); return false;" title="Clique para ir à página...">Ir para página</a> <span class="onpagestyle"><strong>1</strong></span><span class="page-sep">, </span><a class="paginstyle" href="https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;start=10">2</a><span class="page-sep">, </span><a class="paginstyle" href="https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;start=20">3</a><span class="page-sep">, </span><a class="paginstyle" href="https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;start=30">4</a><span class="page-sep">, </span><a class="paginstyle" href="https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;start=40">5</a><span class="page-dots"> ... </span><a class="paginstyle" href="https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;start=270">28</a> &nbsp;<a style="background-color: #44ADF1;color: white;padding: 4px;border: 1px solid #C4FF96" href="https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;start=10">&raquo;</a></b></td>
    
  </tr>
  </table>
</div>

<div id="pagefooter"></div>

<br clear="all"/>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="42" height="16"><img src="https://www.falandodeviagem.com.br/styles/new_beach/theme/images/tl.gif" width="42" height="16" alt=""/></td>
    <td height="16" style="background:url(styles/new_beach/theme/images/tm.gif.pagespeed.ce._OG-6Gce5P.gif)">&nbsp;</td>
    <td width="42" height="16"><img src="styles/new_beach/theme/images/tr.gif.pagespeed.ce.ZLPNCtp1GE.gif" width="42" height="16" alt=""/></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="18" style="background:url(styles/new_beach/theme/images/ml.gif)"></td>
    <td>
	<table class="tablebg" width="100%" cellspacing="0">
	<tr>
		<th nowrap="nowrap">&nbsp;Tópicos Relacionados&nbsp;</th>
		<th nowrap="nowrap">&nbsp;&nbsp;</th>
		<th nowrap="nowrap">&nbsp;Respostas&nbsp;</th>
	</tr>
	
		<tr valign="middle">
			<td class="row1">				
				<a href="https://www.falandodeviagem.com.br/viewtopic.php?f=290&amp;t=16463&amp;sid=5aa91c30d4923ae7dc55879a7590b259" class="topictitle">Novos benefícios do cartão de crédito Itaucard MasterCard Black Personnalité e Private</a>
				
					<p class="gensmall"> [ Ir para página: <a class="paginas" href="https://www.falandodeviagem.com.br/viewtopic.php?f=290&amp;t=16463&amp;sid=5aa91c30d4923ae7dc55879a7590b259">1</a><span class="page-sep">, </span><a class="paginas" href="https://www.falandodeviagem.com.br/viewtopic.php?f=290&amp;t=16463&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;start=10">2</a><span class="page-sep">, </span><a class="paginas" href="https://www.falandodeviagem.com.br/viewtopic.php?f=290&amp;t=16463&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;start=20">3</a> ] </p>
				
				<p class="gensmall">em <a href="https://www.falandodeviagem.com.br/viewforum.php?f=290&amp;sid=5aa91c30d4923ae7dc55879a7590b259">Itaú</a></p>
				<p class="gensmall">Última mensagem Qui Dez 20, 2018 3:39 pm <a href="https://www.falandodeviagem.com.br/memberlist.php?mode=viewprofile&amp;u=30531&amp;sid=5aa91c30d4923ae7dc55879a7590b259">ftelles</a> <a href="https://www.falandodeviagem.com.br/viewtopic.php?f=290&amp;t=16463&amp;p=413048&amp;sid=5aa91c30d4923ae7dc55879a7590b259#p413048"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/pt_br/icon_topic_latest.gif" width="18" height="9" alt="Ver última mensagem" title="Ver última mensagem"/></a></p>
			</td>
			<td class="row1" width="50" align="center"><p class="topicdetails">&nbsp;</p></td>
			<td class="row2" width="50" align="center"><p class="topicdetails">23</p></td>
		</tr>
	
		<tr valign="middle">
			<td class="row1">				
				<a href="https://www.falandodeviagem.com.br/viewtopic.php?f=296&amp;t=16090&amp;sid=5aa91c30d4923ae7dc55879a7590b259" class="topictitle">Cartão de crédito Credicard MasterCard Black</a>
				
					<p class="gensmall"> [ Ir para página: <a class="paginas" href="https://www.falandodeviagem.com.br/viewtopic.php?f=296&amp;t=16090&amp;sid=5aa91c30d4923ae7dc55879a7590b259">1</a><span class="page-dots"> ... </span><a class="paginas" href="https://www.falandodeviagem.com.br/viewtopic.php?f=296&amp;t=16090&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;start=800">81</a><span class="page-sep">, </span><a class="paginas" href="https://www.falandodeviagem.com.br/viewtopic.php?f=296&amp;t=16090&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;start=810">82</a><span class="page-sep">, </span><a class="paginas" href="https://www.falandodeviagem.com.br/viewtopic.php?f=296&amp;t=16090&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;start=820">83</a> ] </p>
				
				<p class="gensmall">em <a href="https://www.falandodeviagem.com.br/viewforum.php?f=296&amp;sid=5aa91c30d4923ae7dc55879a7590b259">Credicard</a></p>
				<p class="gensmall">Última mensagem Sex Jan 11, 2019 9:41 am <a href="https://www.falandodeviagem.com.br/memberlist.php?mode=viewprofile&amp;u=26855&amp;sid=5aa91c30d4923ae7dc55879a7590b259">mhenning</a> <a href="https://www.falandodeviagem.com.br/viewtopic.php?f=296&amp;t=16090&amp;p=414167&amp;sid=5aa91c30d4923ae7dc55879a7590b259#p414167"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/pt_br/icon_topic_latest.gif" width="18" height="9" alt="Ver última mensagem" title="Ver última mensagem"/></a></p>
			</td>
			<td class="row1" width="50" align="center"><p class="topicdetails">&nbsp;</p></td>
			<td class="row2" width="50" align="center"><p class="topicdetails">820</p></td>
		</tr>
	
		<tr valign="middle">
			<td class="row1">				
				<a href="https://www.falandodeviagem.com.br/viewtopic.php?f=430&amp;t=15652&amp;sid=5aa91c30d4923ae7dc55879a7590b259" class="topictitle">Cartão de crédito Banrisul MasterCard Black</a>
				
				<p class="gensmall">em <a href="https://www.falandodeviagem.com.br/viewforum.php?f=430&amp;sid=5aa91c30d4923ae7dc55879a7590b259">Banrisul</a></p>
				<p class="gensmall">Última mensagem Qua Set 12, 2018 10:02 am <a href="https://www.falandodeviagem.com.br/memberlist.php?mode=viewprofile&amp;u=16822&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #FF0000;" class="username-coloured">Ajudante</a> <a href="https://www.falandodeviagem.com.br/viewtopic.php?f=430&amp;t=15652&amp;p=405996&amp;sid=5aa91c30d4923ae7dc55879a7590b259#p405996"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/pt_br/icon_topic_latest.gif" width="18" height="9" alt="Ver última mensagem" title="Ver última mensagem"/></a></p>
			</td>
			<td class="row1" width="50" align="center"><p class="topicdetails">&nbsp;</p></td>
			<td class="row2" width="50" align="center"><p class="topicdetails">7</p></td>
		</tr>
	
		<tr valign="middle">
			<td class="row1">				
				<a href="https://www.falandodeviagem.com.br/viewtopic.php?f=4&amp;t=15500&amp;sid=5aa91c30d4923ae7dc55879a7590b259" class="topictitle">Cartão de crédito Daycoval MasterCard Black</a>
				
					<p class="gensmall"> [ Ir para página: <a class="paginas" href="https://www.falandodeviagem.com.br/viewtopic.php?f=4&amp;t=15500&amp;sid=5aa91c30d4923ae7dc55879a7590b259">1</a><span class="page-dots"> ... </span><a class="paginas" href="https://www.falandodeviagem.com.br/viewtopic.php?f=4&amp;t=15500&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;start=60">7</a><span class="page-sep">, </span><a class="paginas" href="https://www.falandodeviagem.com.br/viewtopic.php?f=4&amp;t=15500&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;start=70">8</a><span class="page-sep">, </span><a class="paginas" href="https://www.falandodeviagem.com.br/viewtopic.php?f=4&amp;t=15500&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;start=80">9</a> ] </p>
				
				<p class="gensmall">em <a href="https://www.falandodeviagem.com.br/viewforum.php?f=4&amp;sid=5aa91c30d4923ae7dc55879a7590b259">Cartões de crédito e serviços financeiros</a></p>
				<p class="gensmall">Última mensagem Dom Dez 16, 2018 10:10 pm <a href="https://www.falandodeviagem.com.br/memberlist.php?mode=viewprofile&amp;u=13143&amp;sid=5aa91c30d4923ae7dc55879a7590b259">tbrondani</a> <a href="https://www.falandodeviagem.com.br/viewtopic.php?f=4&amp;t=15500&amp;p=412797&amp;sid=5aa91c30d4923ae7dc55879a7590b259#p412797"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/pt_br/icon_topic_latest.gif" width="18" height="9" alt="Ver última mensagem" title="Ver última mensagem"/></a></p>
			</td>
			<td class="row1" width="50" align="center"><p class="topicdetails">&nbsp;</p></td>
			<td class="row2" width="50" align="center"><p class="topicdetails">85</p></td>
		</tr>
	
		<tr valign="middle">
			<td class="row1">				
				<a href="https://www.falandodeviagem.com.br/viewtopic.php?f=4&amp;t=15699&amp;sid=5aa91c30d4923ae7dc55879a7590b259" class="topictitle">Cartão de crédito Banco Inter MasterCard Black</a>
				
					<p class="gensmall"> [ Ir para página: <a class="paginas" href="https://www.falandodeviagem.com.br/viewtopic.php?f=4&amp;t=15699&amp;sid=5aa91c30d4923ae7dc55879a7590b259">1</a><span class="page-dots"> ... </span><a class="paginas" href="https://www.falandodeviagem.com.br/viewtopic.php?f=4&amp;t=15699&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;start=40">5</a><span class="page-sep">, </span><a class="paginas" href="https://www.falandodeviagem.com.br/viewtopic.php?f=4&amp;t=15699&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;start=50">6</a><span class="page-sep">, </span><a class="paginas" href="https://www.falandodeviagem.com.br/viewtopic.php?f=4&amp;t=15699&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;start=60">7</a> ] </p>
				
				<p class="gensmall">em <a href="https://www.falandodeviagem.com.br/viewforum.php?f=4&amp;sid=5aa91c30d4923ae7dc55879a7590b259">Cartões de crédito e serviços financeiros</a></p>
				<p class="gensmall">Última mensagem Dom Nov 25, 2018 9:24 pm <a href="https://www.falandodeviagem.com.br/memberlist.php?mode=viewprofile&amp;u=21416&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="color: #FF9900;" class="username-coloured">LMBJR</a> <a href="https://www.falandodeviagem.com.br/viewtopic.php?f=4&amp;t=15699&amp;p=411254&amp;sid=5aa91c30d4923ae7dc55879a7590b259#p411254"><img src="https://www.falandodeviagem.com.br/styles/new_beach/imageset/pt_br/icon_topic_latest.gif" width="18" height="9" alt="Ver última mensagem" title="Ver última mensagem"/></a></p>
			</td>
			<td class="row1" width="50" align="center"><p class="topicdetails">&nbsp;</p></td>
			<td class="row2" width="50" align="center"><p class="topicdetails">69</p></td>
		</tr>
	
	</table>
	</td>
    <td width="17" style="background:url(styles/new_beach/theme/images/mr.gif)"></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="42" height="16"><img src="https://www.falandodeviagem.com.br/styles/new_beach/theme/images/bl.gif" width="42" height="16" alt=""/></td>
    <td height="16" style="background:url(styles/new_beach/theme/images/bm.gif.pagespeed.ce.Kz9nwfUnr6.gif)">&nbsp;</td>
    <td width="42" height="16"><img src="styles/new_beach/theme/images/br.gif.pagespeed.ce.Jzgyb6kmWu.gif" width="42" height="16" alt=""/></td>
  </tr>
</table>
	<br clear="all"/><div class="bc" style="clear: both;">
	<table class="bcbg" width="100%" cellspacing="1" cellpadding="0" style="margin-top: 5px;">
	<tr>
		<td class="row1">
			<p class="breadcrumbs"><a style="background-color:#3374d0; color:#FFF; padding:2px; float:left " href="https://www.falandodeviagem.com.br/index.php?sid=5aa91c30d4923ae7dc55879a7590b259">Página Principal
            </a><div style="float:left;width:12px;height:18px;background-image:url(images/bread1.png.pagespeed.ce.rOcnHsFU-Z.png)"></div>
              <div id="customsep" style="float:left;width:12px;height:18px;background-image:url(images/bread2.png.pagespeed.ce.rOcnHsFU-Z.png)"></div><a href="https://www.falandodeviagem.com.br/viewforum.php?f=4&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="padding:2px; color:#FFF; background-color:#6c6c6c; float:left">Cartões de crédito e serviços financeiros</a>  <div id="customsep" style="float:left;width:12px;height:18px;background-image:url(images/bread2.png.pagespeed.ce.rOcnHsFU-Z.png)"></div><a href="https://www.falandodeviagem.com.br/viewforum.php?f=212&amp;sid=5aa91c30d4923ae7dc55879a7590b259" style="padding:2px; color:#FFF; background-color:#6c6c6c; float:left">MasterCard</a><div id="customsep" style="float:left;width:8px;height:18px;background-image:url(images/bread3.png.pagespeed.ce.Aao52Rmq5h.png)"></div></p>
			<p class="datetime"><span style="display: none;">Todos os horários são GMT - 3 horas [ <abbr title="Horário de verão">DST</abbr> ]</p>
		</td>
	</tr>
	</table>
</div>

<br clear="all"/>
<div class="tbspace">
<table width="100%" cellspacing="0">
<tr>
  <td width="40%" valign="top" nowrap="nowrap" align="left"></td>
  <td align="right" valign="top" nowrap="nowrap"></td>
</tr>
</table>
</div>
<br clear="all"/>
<div class="tbspace">
<table width="100%" cellspacing="0">
<tr>
  <td>
	<form method="post" name="jumpbox" action="https://www.falandodeviagem.com.br/viewforum.php?sid=5aa91c30d4923ae7dc55879a7590b259" onsubmit="if(document.jumpbox.f.value == -1){return false;}">

	<table cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td nowrap="nowrap"><span class="gensmall">Ir para:</span>&nbsp;<select name="f" onchange="if(this.options[this.selectedIndex].value != -1){ document.forms['jumpbox'].submit() }">

		
			<option value="-1">Selecione um fórum</option>
		<option value="-1">------------------</option>
			<option value="3">Companhias aéreas</option>
		
			<option value="705">&nbsp; &nbsp;Viajando com crianças</option>
		
			<option value="143">&nbsp; &nbsp;Aeroportos</option>
		
			<option value="23">&nbsp; &nbsp;Programas de Fidelidade</option>
		
			<option value="147">&nbsp; &nbsp;&nbsp; &nbsp;AAdvantage</option>
		
			<option value="149">&nbsp; &nbsp;&nbsp; &nbsp;Smiles</option>
		
			<option value="150">&nbsp; &nbsp;&nbsp; &nbsp;TAM Fidelidade</option>
		
			<option value="185">&nbsp; &nbsp;&nbsp; &nbsp;SkyMiles</option>
		
			<option value="208">&nbsp; &nbsp;&nbsp; &nbsp;Victoria</option>
		
			<option value="246">&nbsp; &nbsp;&nbsp; &nbsp;Dividend Miles</option>
		
			<option value="307">&nbsp; &nbsp;&nbsp; &nbsp;MileagePlus</option>
		
			<option value="313">&nbsp; &nbsp;&nbsp; &nbsp;TudoAzul</option>
		
			<option value="314">&nbsp; &nbsp;&nbsp; &nbsp;LANPASS</option>
		
			<option value="315">&nbsp; &nbsp;&nbsp; &nbsp;Iberia Plus</option>
		
			<option value="560">&nbsp; &nbsp;&nbsp; &nbsp;ConnectMiles</option>
		
			<option value="561">&nbsp; &nbsp;&nbsp; &nbsp;AMIGO</option>
		
			<option value="316">&nbsp; &nbsp;&nbsp; &nbsp;Flying Blue</option>
		
			<option value="317">&nbsp; &nbsp;&nbsp; &nbsp;Executive Club</option>
		
			<option value="413">&nbsp; &nbsp;&nbsp; &nbsp;Miles&amp;Bonus</option>
		
			<option value="609">&nbsp; &nbsp;&nbsp; &nbsp;LATAM Fidelidade</option>
		
			<option value="622">&nbsp; &nbsp;&nbsp; &nbsp;Km de Vantagens</option>
		
			<option value="151">&nbsp; &nbsp;Avaliações</option>
		
			<option value="236">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da American Airlines</option>
		
			<option value="257">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Alitalia</option>
		
			<option value="254">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Copa Airlines</option>
		
			<option value="626">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Emirates Airline</option>
		
			<option value="426">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Delta Air Lines</option>
		
			<option value="238">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da GOL</option>
		
			<option value="256">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da British Airways</option>
		
			<option value="237">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da TAM</option>
		
			<option value="252">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Iberia</option>
		
			<option value="255">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Qatar Airways</option>
		
			<option value="428">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Air France</option>
		
			<option value="377">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações de Azul</option>
		
			<option value="427">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da United Airlines</option>
		
			<option value="431">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Avianca</option>
		
			<option value="627">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Etihad Airways</option>
		
			<option value="660">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Royal Air Maroc</option>
		
			<option value="669">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da TAP</option>
		
			<option value="670">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Lufthansa</option>
		
			<option value="671">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Singapore Airlines</option>
		
			<option value="201">&nbsp; &nbsp;&nbsp; &nbsp;Testes de Check-in</option>
		
			<option value="131">&nbsp; &nbsp;Salas VIP</option>
		
			<option value="280">&nbsp; &nbsp;&nbsp; &nbsp;GOL Premium Lounge</option>
		
			<option value="234">&nbsp; &nbsp;&nbsp; &nbsp;Bradesco / American Express</option>
		
			<option value="233">&nbsp; &nbsp;&nbsp; &nbsp;American Airlines</option>
		
			<option value="652">&nbsp; &nbsp;&nbsp; &nbsp;Priority Pass</option>
		
			<option value="281">&nbsp; &nbsp;&nbsp; &nbsp;LATAM Airlines</option>
		
			<option value="648">&nbsp; &nbsp;&nbsp; &nbsp;Plaza Premium Lounge</option>
		
			<option value="651">&nbsp; &nbsp;&nbsp; &nbsp;Star Alliance</option>
		
			<option value="283">&nbsp; &nbsp;&nbsp; &nbsp;MasterCard</option>
		
			<option value="380">&nbsp; &nbsp;&nbsp; &nbsp;Delta Air Lines</option>
		
			<option value="704">&nbsp; &nbsp;&nbsp; &nbsp;Air France-KLM</option>
		
			<option value="615">&nbsp; &nbsp;&nbsp; &nbsp;Alitalia</option>
		
			<option value="647">&nbsp; &nbsp;&nbsp; &nbsp;Avianca</option>
		
			<option value="664">&nbsp; &nbsp;&nbsp; &nbsp;British Airways</option>
		
			<option value="650">&nbsp; &nbsp;&nbsp; &nbsp;South African Airways</option>
		
			<option value="649">&nbsp; &nbsp;&nbsp; &nbsp;Bangkok Airways</option>
		
			<option value="455">&nbsp; &nbsp;&nbsp; &nbsp;TAP</option>
		
			<option value="453">&nbsp; &nbsp;&nbsp; &nbsp;United Airlines</option>
		
			<option value="457">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;US Airways Club</option>
		
			<option value="284">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Diners</option>
		
			<option value="282">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;HSBC</option>
		
			<option value="347">&nbsp; &nbsp;Aeronaves</option>
		
			<option value="223">&nbsp; &nbsp;Assentos</option>
		
			<option value="24">&nbsp; &nbsp;Promoções</option>
		
			<option value="146">&nbsp; &nbsp;Reflexões</option>
		
			<option value="228">&nbsp; &nbsp;Franquia de bagagem</option>
		
			<option value="200">&nbsp; &nbsp;Notícias</option>
		
			<option value="338">&nbsp; &nbsp;Internet</option>
		
			<option value="332">&nbsp; &nbsp;Parcelamento</option>
		
			<option value="369">&nbsp; &nbsp;Amenities</option>
		
			<option value="376">&nbsp; &nbsp;Alianças Aéreas</option>
		
			<option value="433">&nbsp; &nbsp;Serviço de bordo</option>
		
			<option value="484">&nbsp; &nbsp;Upgrade</option>
		
			<option value="395">&nbsp; &nbsp;Dúvidas gerais</option>
		
			<option value="119">&nbsp; &nbsp;Companhias aéreas nacionais</option>
		
			<option value="118">&nbsp; &nbsp;Companhias aéreas estrangeiras que operam no Brasil</option>
		
			<option value="120">&nbsp; &nbsp;Companhias aéreas estrangeiras que não operam no Brasil</option>
		
			<option value="5">Aluguel de carros</option>
		
			<option value="554">&nbsp; &nbsp;Reserve aqui o seu carro</option>
		
			<option value="547">&nbsp; &nbsp;Rentcars</option>
		
			<option value="231">&nbsp; &nbsp;Sixt</option>
		
			<option value="226">&nbsp; &nbsp;Hertz</option>
		
			<option value="386">&nbsp; &nbsp;Alamo</option>
		
			<option value="387">&nbsp; &nbsp;Dollar</option>
		
			<option value="408">&nbsp; &nbsp;Localiza</option>
		
			<option value="334">&nbsp; &nbsp;Fox Rent A Car</option>
		
			<option value="193">&nbsp; &nbsp;Carros</option>
		
			<option value="412">&nbsp; &nbsp;&nbsp; &nbsp;Esportivos de luxo</option>
		
			<option value="522">&nbsp; &nbsp;&nbsp; &nbsp;Vídeos de carros alugados</option>
		
			<option value="181">&nbsp; &nbsp;Promoções</option>
		
			<option value="399">&nbsp; &nbsp;Dúvidas gerais</option>
		
			<option value="7">Hotéis</option>
		
			<option value="90">&nbsp; &nbsp;Programas de Fidelidade</option>
		
			<option value="218">&nbsp; &nbsp;&nbsp; &nbsp;Marriott Rewards</option>
		
			<option value="220">&nbsp; &nbsp;&nbsp; &nbsp;Le Club Accorhotels</option>
		
			<option value="310">&nbsp; &nbsp;&nbsp; &nbsp;SPG</option>
		
			<option value="311">&nbsp; &nbsp;&nbsp; &nbsp;Hyatt Gold Passport</option>
		
			<option value="312">&nbsp; &nbsp;&nbsp; &nbsp;Hilton HHonors</option>
		
			<option value="331">&nbsp; &nbsp;&nbsp; &nbsp;IHG Rewards Club</option>
		
			<option value="365">&nbsp; &nbsp;&nbsp; &nbsp;Best Western Rewards</option>
		
			<option value="403">&nbsp; &nbsp;&nbsp; &nbsp;MeliáRewards</option>
		
			<option value="111">&nbsp; &nbsp;Promoções</option>
		
			<option value="122">&nbsp; &nbsp;Pousadas</option>
		
			<option value="349">&nbsp; &nbsp;Club Lounge</option>
		
			<option value="374">&nbsp; &nbsp;Notícias</option>
		
			<option value="422">&nbsp; &nbsp;Dúvidas gerais</option>
		
			<option value="6">Cruzeiros</option>
		
			<option value="134">&nbsp; &nbsp;Roteiros</option>
		
			<option value="608">&nbsp; &nbsp;Disney Dream</option>
		
			<option value="631">&nbsp; &nbsp;Allure of the Seas</option>
		
			<option value="701">&nbsp; &nbsp;Symphony of the Seas</option>
		
			<option value="503">&nbsp; &nbsp;Splendour of the Seas</option>
		
			<option value="574">&nbsp; &nbsp;Anthem of the Seas</option>
		
			<option value="300">&nbsp; &nbsp;Norwegian Epic</option>
		
			<option value="483">&nbsp; &nbsp;MSC Preziosa</option>
		
			<option value="579">&nbsp; &nbsp;MSC Orchestra</option>
		
			<option value="511">&nbsp; &nbsp;Silver Spirit</option>
		
			<option value="470">&nbsp; &nbsp;Rhapsody of the Seas</option>
		
			<option value="325">&nbsp; &nbsp;Promoções</option>
		
			<option value="265">&nbsp; &nbsp;Notícias</option>
		
			<option value="695">&nbsp; &nbsp;Dúvidas gerais</option>
		
			<option value="351">Falando +D</option>
		
			<option value="352">&nbsp; &nbsp;Exclusivas</option>
		
			<option value="370">&nbsp; &nbsp;Econômicas</option>
		
			<option value="653">&nbsp; &nbsp;Família</option>
		
			<option value="4">Cartões de crédito e serviços financeiros</option>
		
			<option value="290">&nbsp; &nbsp;Itaú</option>
		
			<option value="289">&nbsp; &nbsp;Bradesco</option>
		
			<option value="295">&nbsp; &nbsp;Banco do Brasil</option>
		
			<option value="294">&nbsp; &nbsp;Caixa</option>
		
			<option value="291">&nbsp; &nbsp;Santander</option>
		
			<option value="458">&nbsp; &nbsp;Porto Seguro</option>
		
			<option value="209">&nbsp; &nbsp;American Express</option>
		
			<option value="210">&nbsp; &nbsp;Diners</option>
		
			<option value="296">&nbsp; &nbsp;Credicard</option>
		
			<option value="327">&nbsp; &nbsp;Safra</option>
		
			<option value="212">&nbsp; &nbsp;MasterCard</option>
		
			<option value="211">&nbsp; &nbsp;Visa</option>
		
			<option value="607">&nbsp; &nbsp;Digio</option>
		
			<option value="382">&nbsp; &nbsp;PanAmericano</option>
		
			<option value="613">&nbsp; &nbsp;Nubank</option>
		
			<option value="616">&nbsp; &nbsp;Sicoob</option>
		
			<option value="614">&nbsp; &nbsp;Votorantim</option>
		
			<option value="619">&nbsp; &nbsp;Sicredi</option>
		
			<option value="633">&nbsp; &nbsp;Banco Original</option>
		
			<option value="623">&nbsp; &nbsp;ELO</option>
		
			<option value="430">&nbsp; &nbsp;Banrisul</option>
		
			<option value="654">&nbsp; &nbsp;next</option>
		
			<option value="709">&nbsp; &nbsp;Neon</option>
		
			<option value="292">&nbsp; &nbsp;Citibank</option>
		
			<option value="293">&nbsp; &nbsp;HSBC</option>
		
			<option value="258">&nbsp; &nbsp;Anuidade</option>
		
			<option value="250">&nbsp; &nbsp;Concierge</option>
		
			<option value="251">&nbsp; &nbsp;Seguros</option>
		
			<option value="242">&nbsp; &nbsp;Milhas aéreas com Pague Contas</option>
		
			<option value="243">&nbsp; &nbsp;Serviços financeiros internacionais</option>
		
			<option value="263">&nbsp; &nbsp;Programas de Fidelidade</option>
		
			<option value="392">&nbsp; &nbsp;Dúvidas gerais</option>
		
			<option value="26">&nbsp; &nbsp;Promoções</option>
		
			<option value="477">&nbsp; &nbsp;Cotação do dólar mês a mês</option>
		
			<option value="285">&nbsp; &nbsp;Reclamações</option>
		
			<option value="13">Destinos - América do Sul</option>
		
			<option value="30">&nbsp; &nbsp;Argentina</option>
		
			<option value="194">&nbsp; &nbsp;&nbsp; &nbsp;Buenos Aires</option>
		
			<option value="195">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em Buenos Aires</option>
		
			<option value="197">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Buenos Aires</option>
		
			<option value="196">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Buenos Aires</option>
		
			<option value="277">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Buenos Aires</option>
		
			<option value="357">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fotos de Buenos Aires</option>
		
			<option value="155">&nbsp; &nbsp;&nbsp; &nbsp;Bariloche</option>
		
			<option value="529">&nbsp; &nbsp;&nbsp; &nbsp;Rosário</option>
		
			<option value="589">&nbsp; &nbsp;&nbsp; &nbsp;Mendoza</option>
		
			<option value="606">&nbsp; &nbsp;&nbsp; &nbsp;Ushuaia</option>
		
			<option value="702">&nbsp; &nbsp;&nbsp; &nbsp;Salta</option>
		
			<option value="31">&nbsp; &nbsp;Chile</option>
		
			<option value="545">&nbsp; &nbsp;&nbsp; &nbsp;Santiago do Chile</option>
		
			<option value="601">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Santiago do Chile</option>
		
			<option value="602">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Santiago do Chile</option>
		
			<option value="605">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Santiago do Chile</option>
		
			<option value="632">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde fazer compras em Santiago do Chile</option>
		
			<option value="583">&nbsp; &nbsp;&nbsp; &nbsp;Patagônia Chilena</option>
		
			<option value="595">&nbsp; &nbsp;&nbsp; &nbsp;Valle Nevado</option>
		
			<option value="675">&nbsp; &nbsp;&nbsp; &nbsp;Atacama</option>
		
			<option value="546">&nbsp; &nbsp;&nbsp; &nbsp;Valparaíso</option>
		
			<option value="600">&nbsp; &nbsp;&nbsp; &nbsp;Corralco</option>
		
			<option value="688">&nbsp; &nbsp;&nbsp; &nbsp;Viña del Mar</option>
		
			<option value="599">&nbsp; &nbsp;&nbsp; &nbsp;Huilo Huilo</option>
		
			<option value="689">&nbsp; &nbsp;&nbsp; &nbsp;Pucón</option>
		
			<option value="38">&nbsp; &nbsp;Peru</option>
		
			<option value="598">&nbsp; &nbsp;&nbsp; &nbsp;Lima</option>
		
			<option value="512">&nbsp; &nbsp;&nbsp; &nbsp;Machu Picchu</option>
		
			<option value="39">&nbsp; &nbsp;Uruguai</option>
		
			<option value="681">&nbsp; &nbsp;&nbsp; &nbsp;Montevidéu</option>
		
			<option value="680">&nbsp; &nbsp;&nbsp; &nbsp;Punta del Este</option>
		
			<option value="682">&nbsp; &nbsp;&nbsp; &nbsp;Carmelo</option>
		
			<option value="683">&nbsp; &nbsp;&nbsp; &nbsp;Colônia do Sacramento</option>
		
			<option value="33">&nbsp; &nbsp;Colômbia</option>
		
			<option value="677">&nbsp; &nbsp;&nbsp; &nbsp;Bogotá</option>
		
			<option value="679">&nbsp; &nbsp;&nbsp; &nbsp;Cartagena</option>
		
			<option value="34">&nbsp; &nbsp;Equador</option>
		
			<option value="36">&nbsp; &nbsp;Paraguai</option>
		
			<option value="32">&nbsp; &nbsp;Bolívia</option>
		
			<option value="37">&nbsp; &nbsp;Patagônia Argentina, Chilena &amp; Terra do Fogo</option>
		
			<option value="40">&nbsp; &nbsp;Venezuela</option>
		
			<option value="166">&nbsp; &nbsp;&nbsp; &nbsp;Los Roques</option>
		
			<option value="35">&nbsp; &nbsp;Guianas &amp; Suriname</option>
		
			<option value="9">Destinos - América do Norte</option>
		
			<option value="41">&nbsp; &nbsp;Estados Unidos</option>
		
			<option value="337">&nbsp; &nbsp;&nbsp; &nbsp;Flórida</option>
		
			<option value="116">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Orlando</option>
		
			<option value="703">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Orlando com crianças</option>
		
			<option value="154">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Orlando</option>
		
			<option value="274">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Shoppings de Orlando</option>
		
			<option value="247">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Outlets de Orlando</option>
		
			<option value="288">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Centros de Compra em Orlando</option>
		
			<option value="239">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Eletrônicos</option>
		
			<option value="275">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras no Magic Kingdom</option>
		
			<option value="276">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Disney Springs</option>
		
			<option value="153">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em Orlando</option>
		
			<option value="192">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Hollywood Studios</option>
		
			<option value="198">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Epcot</option>
		
			<option value="199">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Animal Kingdom</option>
		
			<option value="203">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes em Disney Springs</option>
		
			<option value="191">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Magic Kingdom</option>
		
			<option value="273">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes na Universal Studios</option>
		
			<option value="272">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Islands of Adventure</option>
		
			<option value="260">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Universal CityWalk</option>
		
			<option value="270">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Mall at Millenia</option>
		
			<option value="271">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Florida Mall</option>
		
			<option value="342">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no SeaWorld</option>
		
			<option value="159">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Orlando</option>
		
			<option value="678">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Casas em Orlando</option>
		
			<option value="145">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Orlando</option>
		
			<option value="177">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Ingressos em Orlando</option>
		
			<option value="204">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Parques Aquáticos de Orlando</option>
		
			<option value="322">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Informações gerais sobre Orlando</option>
		
			<option value="161">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Colunas</option>
		
			<option value="248">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Natal em Orlando</option>
		
			<option value="205">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Orlando mês a mês</option>
		
			<option value="187">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Vídeos de Orlando</option>
		
			<option value="162">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Notícias</option>
		
			<option value="144">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Roteiros de Viagem para Orlando</option>
		
			<option value="202">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Relatos de viagens a Orlando</option>
		
			<option value="495">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Dúvidas gerais</option>
		
			<option value="214">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Atrações no Hollywood Studios</option>
		
			<option value="217">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Atrações no Animal Kingdom</option>
		
			<option value="225">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Atrações no Magic Kingdom</option>
		
			<option value="269">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Atrações no LEGOLAND</option>
		
			<option value="504">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Atrações no SeaWorld</option>
		
			<option value="126">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Miami</option>
		
			<option value="188">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em Miami</option>
		
			<option value="171">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Miami</option>
		
			<option value="156">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Miami</option>
		
			<option value="279">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Miami</option>
		
			<option value="354">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fotos de Miami</option>
		
			<option value="170">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fort Lauderdale</option>
		
			<option value="139">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Key West</option>
		
			<option value="180">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Clearwater</option>
		
			<option value="500">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Palm Beach</option>
		
			<option value="286">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Winter Park</option>
		
			<option value="189">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Saint Petersburg</option>
		
			<option value="339">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Celebration</option>
		
			<option value="344">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Bal Harbour</option>
		
			<option value="262">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Daytona Beach</option>
		
			<option value="593">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Tampa</option>
		
			<option value="594">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Kissimmee</option>
		
			<option value="446">&nbsp; &nbsp;&nbsp; &nbsp;New York</option>
		
			<option value="125">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;New York City</option>
		
			<option value="249">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em Nova York</option>
		
			<option value="182">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Nova York</option>
		
			<option value="179">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Nova York</option>
		
			<option value="213">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Nova York</option>
		
			<option value="259">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Roteiros de Viagem para Nova York</option>
		
			<option value="355">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fotos de Nova York</option>
		
			<option value="438">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Buffalo</option>
		
			<option value="441">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Niagara Falls</option>
		
			<option value="304">&nbsp; &nbsp;&nbsp; &nbsp;Califórnia</option>
		
			<option value="137">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Los Angeles</option>
		
			<option value="384">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Los Angeles</option>
		
			<option value="385">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Los Angeles</option>
		
			<option value="359">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fotos de Los Angeles</option>
		
			<option value="133">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;San Diego</option>
		
			<option value="129">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;San Francisco</option>
		
			<option value="396">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em San Francisco</option>
		
			<option value="345">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em San Francisco</option>
		
			<option value="373">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em San Francisco</option>
		
			<option value="463">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em San Francisco</option>
		
			<option value="333">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Oakland</option>
		
			<option value="335">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Napa</option>
		
			<option value="336">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Anaheim</option>
		
			<option value="346">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Santa Barbara</option>
		
			<option value="348">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Indian Wells</option>
		
			<option value="366">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Carmel</option>
		
			<option value="490">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Lake Tahoe</option>
		
			<option value="534">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Big Bear Lake</option>
		
			<option value="541">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Beverly Hills</option>
		
			<option value="559">&nbsp; &nbsp;&nbsp; &nbsp;Nevada</option>
		
			<option value="123">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Las Vegas</option>
		
			<option value="418">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Las Vegas</option>
		
			<option value="388">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde fazer compras em Las Vegas</option>
		
			<option value="298">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Las Vegas</option>
		
			<option value="389">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Las Vegas</option>
		
			<option value="117">&nbsp; &nbsp;&nbsp; &nbsp;Chicago</option>
		
			<option value="328">&nbsp; &nbsp;&nbsp; &nbsp;Atlanta</option>
		
			<option value="323">&nbsp; &nbsp;&nbsp; &nbsp;Boston</option>
		
			<option value="305">&nbsp; &nbsp;&nbsp; &nbsp;Utah</option>
		
			<option value="222">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Park City</option>
		
			<option value="306">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Salt Lake City</option>
		
			<option value="330">&nbsp; &nbsp;&nbsp; &nbsp;Havaí</option>
		
			<option value="364">&nbsp; &nbsp;&nbsp; &nbsp;Charlotte</option>
		
			<option value="434">&nbsp; &nbsp;&nbsp; &nbsp;Pensilvânia</option>
		
			<option value="176">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Filadélfia</option>
		
			<option value="435">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Pittsburgh</option>
		
			<option value="436">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Amish Country</option>
		
			<option value="439">&nbsp; &nbsp;&nbsp; &nbsp;Ohio</option>
		
			<option value="437">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Cleveland</option>
		
			<option value="440">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Cincinnati</option>
		
			<option value="447">&nbsp; &nbsp;&nbsp; &nbsp;Indianápolis</option>
		
			<option value="448">&nbsp; &nbsp;&nbsp; &nbsp;Milwaukee</option>
		
			<option value="459">&nbsp; &nbsp;&nbsp; &nbsp;Alaska</option>
		
			<option value="460">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Skagway</option>
		
			<option value="461">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Juneau</option>
		
			<option value="462">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Ketchikan</option>
		
			<option value="479">&nbsp; &nbsp;&nbsp; &nbsp;Seattle</option>
		
			<option value="488">&nbsp; &nbsp;&nbsp; &nbsp;Texas</option>
		
			<option value="261">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Houston</option>
		
			<option value="489">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;San Antonio</option>
		
			<option value="141">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Dallas</option>
		
			<option value="496">&nbsp; &nbsp;&nbsp; &nbsp;Colorado</option>
		
			<option value="497">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Steamboat Springs</option>
		
			<option value="498">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Vail</option>
		
			<option value="499">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Aspen</option>
		
			<option value="544">&nbsp; &nbsp;&nbsp; &nbsp;Arizona</option>
		
			<option value="542">&nbsp; &nbsp;&nbsp; &nbsp;Tennessee</option>
		
			<option value="543">&nbsp; &nbsp;&nbsp; &nbsp;Mississippi</option>
		
			<option value="569">&nbsp; &nbsp;&nbsp; &nbsp;Carolina do Sul</option>
		
			<option value="577">&nbsp; &nbsp;&nbsp; &nbsp;Região Capital dos EUA</option>
		
			<option value="130">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Washington DC</option>
		
			<option value="215">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Washington DC</option>
		
			<option value="186">&nbsp; &nbsp;&nbsp; &nbsp;Compras nos Estados Unidos</option>
		
			<option value="475">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras de eletrônicos nos Estados Unidos</option>
		
			<option value="472">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras de enxoval de bebê nos Estados Unidos</option>
		
			<option value="476">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras de roupas nos Estados Unidos</option>
		
			<option value="473">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras para casa nos Estados Unidos</option>
		
			<option value="474">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Cupons de desconto nos Estados Unidos</option>
		
			<option value="266">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Promoções</option>
		
			<option value="329">&nbsp; &nbsp;&nbsp; &nbsp;Roteiros de Viagem pelos Estados Unidos</option>
		
			<option value="183">&nbsp; &nbsp;&nbsp; &nbsp;Alimentação nos Estados Unidos</option>
		
			<option value="390">&nbsp; &nbsp;&nbsp; &nbsp;Transporte nos Estados Unidos</option>
		
			<option value="42">&nbsp; &nbsp;Canadá</option>
		
			<option value="450">&nbsp; &nbsp;&nbsp; &nbsp;Vancouver</option>
		
			<option value="451">&nbsp; &nbsp;&nbsp; &nbsp;Toronto</option>
		
			<option value="514">&nbsp; &nbsp;&nbsp; &nbsp;Montreal</option>
		
			<option value="452">&nbsp; &nbsp;&nbsp; &nbsp;Whistler</option>
		
			<option value="665">&nbsp; &nbsp;&nbsp; &nbsp;Ottawa</option>
		
			<option value="668">&nbsp; &nbsp;&nbsp; &nbsp;Cidade de Quebec</option>
		
			<option value="22">Destinos - América Central, Caribe &amp; México</option>
		
			<option value="59">&nbsp; &nbsp;Caribe</option>
		
			<option value="449">&nbsp; &nbsp;&nbsp; &nbsp;Punta Cana</option>
		
			<option value="521">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Punta Cana</option>
		
			<option value="517">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde fazer compras em Punta Cana</option>
		
			<option value="672">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Punta Cana</option>
		
			<option value="164">&nbsp; &nbsp;&nbsp; &nbsp;Aruba</option>
		
			<option value="235">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em Aruba</option>
		
			<option value="230">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Aruba</option>
		
			<option value="227">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Aruba</option>
		
			<option value="268">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Aruba</option>
		
			<option value="178">&nbsp; &nbsp;&nbsp; &nbsp;Curaçao</option>
		
			<option value="232">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Curaçao</option>
		
			<option value="297">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Curaçao</option>
		
			<option value="360">&nbsp; &nbsp;&nbsp; &nbsp;Barbados</option>
		
			<option value="706">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Barbados</option>
		
			<option value="707">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Barbados</option>
		
			<option value="708">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Barbados</option>
		
			<option value="302">&nbsp; &nbsp;&nbsp; &nbsp;Bahamas</option>
		
			<option value="301">&nbsp; &nbsp;&nbsp; &nbsp;St Maarten</option>
		
			<option value="303">&nbsp; &nbsp;&nbsp; &nbsp;Ilhas Virgens Americanas</option>
		
			<option value="700">&nbsp; &nbsp;&nbsp; &nbsp;Ilhas Virgens Britânicas</option>
		
			<option value="528">&nbsp; &nbsp;&nbsp; &nbsp;Antígua</option>
		
			<option value="515">&nbsp; &nbsp;&nbsp; &nbsp;Turks e Caicos</option>
		
			<option value="571">&nbsp; &nbsp;&nbsp; &nbsp;Jamaica</option>
		
			<option value="586">&nbsp; &nbsp;&nbsp; &nbsp;Cuba</option>
		
			<option value="537">&nbsp; &nbsp;&nbsp; &nbsp;Tobago</option>
		
			<option value="587">&nbsp; &nbsp;&nbsp; &nbsp;Saint-Martin</option>
		
			<option value="646">&nbsp; &nbsp;&nbsp; &nbsp;Santa Lucia</option>
		
			<option value="673">&nbsp; &nbsp;&nbsp; &nbsp;Ilhas Cayman</option>
		
			<option value="60">&nbsp; &nbsp;México</option>
		
			<option value="245">&nbsp; &nbsp;&nbsp; &nbsp;Cancún</option>
		
			<option value="241">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em Cancún</option>
		
			<option value="206">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Cancún</option>
		
			<option value="207">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Roteiros de viagem para Cancún</option>
		
			<option value="172">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Cancún</option>
		
			<option value="264">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Cancún</option>
		
			<option value="362">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fotos de Cancún</option>
		
			<option value="618">&nbsp; &nbsp;&nbsp; &nbsp;Cidade do México</option>
		
			<option value="244">&nbsp; &nbsp;&nbsp; &nbsp;Cozumel</option>
		
			<option value="240">&nbsp; &nbsp;&nbsp; &nbsp;Playa del Carmen</option>
		
			<option value="400">&nbsp; &nbsp;&nbsp; &nbsp;Isla Mujeres</option>
		
			<option value="527">&nbsp; &nbsp;&nbsp; &nbsp;Riviera Maya</option>
		
			<option value="507">&nbsp; &nbsp;&nbsp; &nbsp;Riviera Nayarit</option>
		
			<option value="556">&nbsp; &nbsp;&nbsp; &nbsp;Puebla</option>
		
			<option value="58">&nbsp; &nbsp;Panamá</option>
		
			<option value="52">&nbsp; &nbsp;Belize</option>
		
			<option value="568">&nbsp; &nbsp;Costa Rica</option>
		
			<option value="56">&nbsp; &nbsp;Honduras</option>
		
			<option value="55">&nbsp; &nbsp;Guatemala</option>
		
			<option value="54">&nbsp; &nbsp;El Salvador</option>
		
			<option value="57">&nbsp; &nbsp;Nicarágua</option>
		
			<option value="8">Destinos - Brasil</option>
		
			<option value="16">&nbsp; &nbsp;Região Sudeste</option>
		
			<option value="91">&nbsp; &nbsp;&nbsp; &nbsp;Rio de Janeiro</option>
		
			<option value="184">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer no Rio de Janeiro</option>
		
			<option value="169">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar no Rio de Janeiro</option>
		
			<option value="326">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover no Rio de Janeiro</option>
		
			<option value="148">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;English</option>
		
			<option value="485">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Região dos Lagos</option>
		
			<option value="138">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Búzios</option>
		
			<option value="535">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Arraial do Cabo</option>
		
			<option value="486">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Região Serrana</option>
		
			<option value="381">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Petrópolis / Itaipava</option>
		
			<option value="482">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Teresópolis</option>
		
			<option value="372">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Visconde de Mauá</option>
		
			<option value="340">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Penedo</option>
		
			<option value="190">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Angra dos Reis</option>
		
			<option value="526">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Paraty</option>
		
			<option value="610">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Niterói</option>
		
			<option value="596">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Rio 2016</option>
		
			<option value="92">&nbsp; &nbsp;&nbsp; &nbsp;São Paulo</option>
		
			<option value="368">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em São Paulo</option>
		
			<option value="367">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em São Paulo</option>
		
			<option value="402">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em São Paulo</option>
		
			<option value="140">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Campos do Jordão</option>
		
			<option value="371">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Ilhabela</option>
		
			<option value="375">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Campinas</option>
		
			<option value="409">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Santos</option>
		
			<option value="398">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;São Roque</option>
		
			<option value="552">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Riviera de São Lourenço</option>
		
			<option value="570">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Maresias</option>
		
			<option value="591">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Socorro</option>
		
			<option value="592">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;São Sebastião</option>
		
			<option value="93">&nbsp; &nbsp;&nbsp; &nbsp;Minas Gerais</option>
		
			<option value="494">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Belo Horizonte</option>
		
			<option value="493">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Monte Verde</option>
		
			<option value="492">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;São Lourenço</option>
		
			<option value="661">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Tiradentes</option>
		
			<option value="684">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Inhotim</option>
		
			<option value="696">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Ouro Preto</option>
		
			<option value="94">&nbsp; &nbsp;&nbsp; &nbsp;Espírito Santo</option>
		
			<option value="17">&nbsp; &nbsp;Região Nordeste</option>
		
			<option value="96">&nbsp; &nbsp;&nbsp; &nbsp;Bahia</option>
		
			<option value="698">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Salvador</option>
		
			<option value="697">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Trancoso</option>
		
			<option value="525">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Morro de São Paulo</option>
		
			<option value="524">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Boipeba</option>
		
			<option value="674">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Ilha de Comandatuba</option>
		
			<option value="699">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Caraíva</option>
		
			<option value="97">&nbsp; &nbsp;&nbsp; &nbsp;Pernambuco</option>
		
			<option value="107">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Porto de Galinhas</option>
		
			<option value="406">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Porto de Galinhas</option>
		
			<option value="407">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Porto de Galinhas</option>
		
			<option value="635">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Porto de Galinhas</option>
		
			<option value="425">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Praia dos Carneiros</option>
		
			<option value="106">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fernando de Noronha</option>
		
			<option value="108">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Recife</option>
		
			<option value="98">&nbsp; &nbsp;&nbsp; &nbsp;Rio Grande do Norte</option>
		
			<option value="99">&nbsp; &nbsp;&nbsp; &nbsp;Ceará</option>
		
			<option value="638">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fortaleza</option>
		
			<option value="645">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Jericoacoara</option>
		
			<option value="424">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Beach Park</option>
		
			<option value="639">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Cumbuco</option>
		
			<option value="100">&nbsp; &nbsp;&nbsp; &nbsp;Maranhão</option>
		
			<option value="101">&nbsp; &nbsp;&nbsp; &nbsp;Alagoas</option>
		
			<option value="102">&nbsp; &nbsp;&nbsp; &nbsp;Paraíba</option>
		
			<option value="104">&nbsp; &nbsp;&nbsp; &nbsp;Piauí</option>
		
			<option value="105">&nbsp; &nbsp;&nbsp; &nbsp;Sergipe</option>
		
			<option value="18">&nbsp; &nbsp;Região Sul</option>
		
			<option value="142">&nbsp; &nbsp;&nbsp; &nbsp;Rio Grande do Sul</option>
		
			<option value="510">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Gramado</option>
		
			<option value="562">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Gramado</option>
		
			<option value="564">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde fazer compras em Gramado</option>
		
			<option value="565">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Gramado</option>
		
			<option value="563">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Gramado</option>
		
			<option value="663">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Canela</option>
		
			<option value="163">&nbsp; &nbsp;&nbsp; &nbsp;Santa Catarina</option>
		
			<option value="411">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Florianópolis</option>
		
			<option value="523">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Praia do Rosa</option>
		
			<option value="662">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Beto Carrero World</option>
		
			<option value="597">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Serra do Rio do Rastro</option>
		
			<option value="530">&nbsp; &nbsp;&nbsp; &nbsp;Foz do Iguaçu</option>
		
			<option value="531">&nbsp; &nbsp;&nbsp; &nbsp;Aparados da Serra</option>
		
			<option value="19">&nbsp; &nbsp;Centro-Oeste</option>
		
			<option value="152">&nbsp; &nbsp;&nbsp; &nbsp;Distrito Federal</option>
		
			<option value="502">&nbsp; &nbsp;&nbsp; &nbsp;Bonito</option>
		
			<option value="536">&nbsp; &nbsp;&nbsp; &nbsp;Caldas Novas</option>
		
			<option value="634">&nbsp; &nbsp;&nbsp; &nbsp;Rio Quente Resorts</option>
		
			<option value="20">&nbsp; &nbsp;Região Norte</option>
		
			<option value="10">Destinos - Europa</option>
		
			<option value="69">&nbsp; &nbsp;França</option>
		
			<option value="464">&nbsp; &nbsp;&nbsp; &nbsp;Paris</option>
		
			<option value="580">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Paris</option>
		
			<option value="468">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Paris</option>
		
			<option value="469">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Paris</option>
		
			<option value="612">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde fazer compras em Paris</option>
		
			<option value="466">&nbsp; &nbsp;&nbsp; &nbsp;Alpes</option>
		
			<option value="575">&nbsp; &nbsp;&nbsp; &nbsp;Riviera Francesa</option>
		
			<option value="628">&nbsp; &nbsp;&nbsp; &nbsp;Champagne</option>
		
			<option value="465">&nbsp; &nbsp;&nbsp; &nbsp;Lyon e Beaujolais</option>
		
			<option value="516">&nbsp; &nbsp;&nbsp; &nbsp;Annecy</option>
		
			<option value="590">&nbsp; &nbsp;&nbsp; &nbsp;Borgonha</option>
		
			<option value="620">&nbsp; &nbsp;&nbsp; &nbsp;Nantes</option>
		
			<option value="621">&nbsp; &nbsp;&nbsp; &nbsp;Chamonix</option>
		
			<option value="629">&nbsp; &nbsp;&nbsp; &nbsp;Rouen</option>
		
			<option value="655">&nbsp; &nbsp;Reino Unido</option>
		
			<option value="78">&nbsp; &nbsp;&nbsp; &nbsp;Londres</option>
		
			<option value="405">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Londres</option>
		
			<option value="432">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde fazer compras em Londres</option>
		
			<option value="404">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Londres</option>
		
			<option value="656">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Londres</option>
		
			<option value="657">&nbsp; &nbsp;&nbsp; &nbsp;Liverpool</option>
		
			<option value="658">&nbsp; &nbsp;&nbsp; &nbsp;Manchester</option>
		
			<option value="76">&nbsp; &nbsp;Portugal</option>
		
			<option value="442">&nbsp; &nbsp;&nbsp; &nbsp;Lisboa</option>
		
			<option value="221">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Lisboa</option>
		
			<option value="324">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Lisboa</option>
		
			<option value="444">&nbsp; &nbsp;&nbsp; &nbsp;Cascais</option>
		
			<option value="401">&nbsp; &nbsp;&nbsp; &nbsp;Porto</option>
		
			<option value="445">&nbsp; &nbsp;&nbsp; &nbsp;Sintra</option>
		
			<option value="467">&nbsp; &nbsp;&nbsp; &nbsp;Évora</option>
		
			<option value="471">&nbsp; &nbsp;&nbsp; &nbsp;Ourém/Fátima</option>
		
			<option value="501">&nbsp; &nbsp;&nbsp; &nbsp;Guimarães</option>
		
			<option value="508">&nbsp; &nbsp;&nbsp; &nbsp;Alcácer do Sal</option>
		
			<option value="582">&nbsp; &nbsp;&nbsp; &nbsp;Douro</option>
		
			<option value="584">&nbsp; &nbsp;&nbsp; &nbsp;Serra da Estrela</option>
		
			<option value="588">&nbsp; &nbsp;&nbsp; &nbsp;Alentejo</option>
		
			<option value="73">&nbsp; &nbsp;Itália</option>
		
			<option value="480">&nbsp; &nbsp;&nbsp; &nbsp;Toscana</option>
		
			<option value="481">&nbsp; &nbsp;&nbsp; &nbsp;Roma</option>
		
			<option value="625">&nbsp; &nbsp;&nbsp; &nbsp;Veneza</option>
		
			<option value="572">&nbsp; &nbsp;&nbsp; &nbsp;Milão</option>
		
			<option value="555">&nbsp; &nbsp;&nbsp; &nbsp;Sicília</option>
		
			<option value="573">&nbsp; &nbsp;&nbsp; &nbsp;Riviera Italiana</option>
		
			<option value="611">&nbsp; &nbsp;&nbsp; &nbsp;Nápoles</option>
		
			<option value="66">&nbsp; &nbsp;Espanha</option>
		
			<option value="566">&nbsp; &nbsp;&nbsp; &nbsp;Barcelona</option>
		
			<option value="567">&nbsp; &nbsp;&nbsp; &nbsp;Madrid</option>
		
			<option value="666">&nbsp; &nbsp;&nbsp; &nbsp;Valência</option>
		
			<option value="667">&nbsp; &nbsp;&nbsp; &nbsp;Sevilha</option>
		
			<option value="62">&nbsp; &nbsp;Alemanha</option>
		
			<option value="478">&nbsp; &nbsp;&nbsp; &nbsp;Berlim</option>
		
			<option value="686">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Berlim</option>
		
			<option value="687">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Berlim</option>
		
			<option value="690">&nbsp; &nbsp;&nbsp; &nbsp;Munique</option>
		
			<option value="691">&nbsp; &nbsp;&nbsp; &nbsp;Frankfurt</option>
		
			<option value="64">&nbsp; &nbsp;Bélgica</option>
		
			<option value="63">&nbsp; &nbsp;Áustria</option>
		
			<option value="71">&nbsp; &nbsp;Holanda</option>
		
			<option value="640">&nbsp; &nbsp;&nbsp; &nbsp;Amsterdã</option>
		
			<option value="711">&nbsp; &nbsp;&nbsp; &nbsp;Roterdã</option>
		
			<option value="581">&nbsp; &nbsp;&nbsp; &nbsp;Haia</option>
		
			<option value="89">&nbsp; &nbsp;Rússia</option>
		
			<option value="67">&nbsp; &nbsp;Dinamarca</option>
		
			<option value="70">&nbsp; &nbsp;Grécia</option>
		
			<option value="72">&nbsp; &nbsp;Islândia</option>
		
			<option value="65">&nbsp; &nbsp;Mediterrâneo</option>
		
			<option value="74">&nbsp; &nbsp;Noruega</option>
		
			<option value="75">&nbsp; &nbsp;Luxemburgo</option>
		
			<option value="77">&nbsp; &nbsp;Irlanda</option>
		
			<option value="219">&nbsp; &nbsp;República Checa</option>
		
			<option value="158">&nbsp; &nbsp;Mônaco</option>
		
			<option value="79">&nbsp; &nbsp;Suécia</option>
		
			<option value="80">&nbsp; &nbsp;Suíça</option>
		
			<option value="513">&nbsp; &nbsp;Hungria</option>
		
			<option value="115">&nbsp; &nbsp;Turquia</option>
		
			<option value="557">&nbsp; &nbsp;Polônia</option>
		
			<option value="558">&nbsp; &nbsp;Croácia</option>
		
			<option value="68">&nbsp; &nbsp;Finlândia</option>
		
			<option value="505">&nbsp; &nbsp;Roteiros de Viagem pela Europa</option>
		
			<option value="685">&nbsp; &nbsp;Transporte na Europa</option>
		
			<option value="11">Destinos - Ásia</option>
		
			<option value="518">&nbsp; &nbsp;Tailândia</option>
		
			<option value="641">&nbsp; &nbsp;&nbsp; &nbsp;Bangkok</option>
		
			<option value="643">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Bangkok</option>
		
			<option value="644">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Bangkok</option>
		
			<option value="642">&nbsp; &nbsp;&nbsp; &nbsp;Chiang Mai</option>
		
			<option value="506">&nbsp; &nbsp;Maldivas</option>
		
			<option value="44">&nbsp; &nbsp;China</option>
		
			<option value="603">&nbsp; &nbsp;&nbsp; &nbsp;Hong Kong</option>
		
			<option value="604">&nbsp; &nbsp;&nbsp; &nbsp;Shanghai</option>
		
			<option value="491">&nbsp; &nbsp;Singapura</option>
		
			<option value="46">&nbsp; &nbsp;Japão</option>
		
			<option value="45">&nbsp; &nbsp;Índia</option>
		
			<option value="532">&nbsp; &nbsp;Sri Lanka</option>
		
			<option value="636">&nbsp; &nbsp;Vietnã</option>
		
			<option value="637">&nbsp; &nbsp;Camboja</option>
		
			<option value="48">&nbsp; &nbsp;Nepal, Tibete &amp; Butão</option>
		
			<option value="47">&nbsp; &nbsp;Mongólia</option>
		
			<option value="61">Destinos - África</option>
		
			<option value="81">&nbsp; &nbsp;África do Sul</option>
		
			<option value="549">&nbsp; &nbsp;&nbsp; &nbsp;Cape Town</option>
		
			<option value="692">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar na Cidade do Cabo</option>
		
			<option value="694">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer na Cidade do Cabo</option>
		
			<option value="693">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover na Cidade do Cabo</option>
		
			<option value="548">&nbsp; &nbsp;&nbsp; &nbsp;Sun City</option>
		
			<option value="550">&nbsp; &nbsp;&nbsp; &nbsp;Johannesburg</option>
		
			<option value="551">&nbsp; &nbsp;&nbsp; &nbsp;Kruger National Park</option>
		
			<option value="553">&nbsp; &nbsp;&nbsp; &nbsp;Hermanus</option>
		
			<option value="520">&nbsp; &nbsp;Seychelles</option>
		
			<option value="85">&nbsp; &nbsp;Egito</option>
		
			<option value="86">&nbsp; &nbsp;Marrocos</option>
		
			<option value="585">&nbsp; &nbsp;Ilhas Maurício</option>
		
			<option value="630">&nbsp; &nbsp;Botswana</option>
		
			<option value="83">&nbsp; &nbsp;Angola</option>
		
			<option value="87">&nbsp; &nbsp;Tunísia</option>
		
			<option value="12">Destinos - Oceania</option>
		
			<option value="50">&nbsp; &nbsp;Austrália</option>
		
			<option value="51">&nbsp; &nbsp;Nova Zelândia</option>
		
			<option value="216">&nbsp; &nbsp;Polinésia Francesa</option>
		
			<option value="576">&nbsp; &nbsp;Fiji</option>
		
			<option value="25">Destinos - Oriente Médio</option>
		
			<option value="168">&nbsp; &nbsp;Dubai</option>
		
			<option value="175">&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em Dubai</option>
		
			<option value="278">&nbsp; &nbsp;&nbsp; &nbsp;Compras em Dubai</option>
		
			<option value="174">&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Dubai</option>
		
			<option value="173">&nbsp; &nbsp;Abu Dhabi</option>
		
			<option value="519">&nbsp; &nbsp;Israel</option>
		
			<option value="84">Intercâmbio</option>
		
			<option value="95">Informações para viagens</option>
		
			<option value="224">&nbsp; &nbsp;Alfândega</option>
		
			<option value="160">&nbsp; &nbsp;Passaporte</option>
		
			<option value="132">&nbsp; &nbsp;Vistos</option>
		
			<option value="267">&nbsp; &nbsp;Vacinas</option>
		
			<option value="343">&nbsp; &nbsp;Câmbio</option>
		
			<option value="287">&nbsp; &nbsp;Acessórios úteis em viagens</option>
		
			<option value="309">&nbsp; &nbsp;Seguro Viagem</option>
		
			<option value="423">&nbsp; &nbsp;Malas de viagem</option>
		
			<option value="308">&nbsp; &nbsp;Sites</option>
		
			<option value="391">&nbsp; &nbsp;Compra coletiva</option>
		
			<option value="443">&nbsp; &nbsp;Agências de viagem</option>
		
			<option value="341">Dicas de viagem</option>
		
			<option value="165">Tecnologia</option>
		
			<option value="394">&nbsp; &nbsp;Apple</option>
		
			<option value="410">&nbsp; &nbsp;Câmeras digitais</option>
		
			<option value="321">&nbsp; &nbsp;Aplicativos para celular</option>
		
			<option value="393">&nbsp; &nbsp;GPS</option>
		
			<option value="676">&nbsp; &nbsp;Drones</option>
		
			<option value="397">Empresas Rodoviárias</option>
		
			<option value="509">Entrevistas</option>
		
			<option value="414">Colunas</option>
		
			<option value="415">&nbsp; &nbsp;Coluna do Gabriel Dias</option>
		
			<option value="416">&nbsp; &nbsp;Coluna do Fabio Macedo</option>
		
			<option value="539">&nbsp; &nbsp;Coluna da Juliana Magalhães</option>
		
			<option value="659">&nbsp; &nbsp;Coluna da Manoela Caldas</option>
		
			<option value="88">Falando de Viagem</option>
		
			<option value="167">&nbsp; &nbsp;Informações sobre o Falando de Viagem</option>
		
			<option value="578">&nbsp; &nbsp;Parceiro Plus</option>
		
			<option value="353">&nbsp; &nbsp;Vamos viajar juntos?</option>
		
			<option value="299">&nbsp; &nbsp;Bate-papo</option>
		
			<option value="27">&nbsp; &nbsp;Denuncie</option>
		
			<option value="-1">Selecione um fórum</option>
		<option value="-1">------------------</option>
			<option value="3">Companhias aéreas</option>
		
			<option value="705">&nbsp; &nbsp;Viajando com crianças</option>
		
			<option value="143">&nbsp; &nbsp;Aeroportos</option>
		
			<option value="23">&nbsp; &nbsp;Programas de Fidelidade</option>
		
			<option value="147">&nbsp; &nbsp;&nbsp; &nbsp;AAdvantage</option>
		
			<option value="149">&nbsp; &nbsp;&nbsp; &nbsp;Smiles</option>
		
			<option value="150">&nbsp; &nbsp;&nbsp; &nbsp;TAM Fidelidade</option>
		
			<option value="185">&nbsp; &nbsp;&nbsp; &nbsp;SkyMiles</option>
		
			<option value="208">&nbsp; &nbsp;&nbsp; &nbsp;Victoria</option>
		
			<option value="246">&nbsp; &nbsp;&nbsp; &nbsp;Dividend Miles</option>
		
			<option value="307">&nbsp; &nbsp;&nbsp; &nbsp;MileagePlus</option>
		
			<option value="313">&nbsp; &nbsp;&nbsp; &nbsp;TudoAzul</option>
		
			<option value="314">&nbsp; &nbsp;&nbsp; &nbsp;LANPASS</option>
		
			<option value="315">&nbsp; &nbsp;&nbsp; &nbsp;Iberia Plus</option>
		
			<option value="560">&nbsp; &nbsp;&nbsp; &nbsp;ConnectMiles</option>
		
			<option value="561">&nbsp; &nbsp;&nbsp; &nbsp;AMIGO</option>
		
			<option value="316">&nbsp; &nbsp;&nbsp; &nbsp;Flying Blue</option>
		
			<option value="317">&nbsp; &nbsp;&nbsp; &nbsp;Executive Club</option>
		
			<option value="413">&nbsp; &nbsp;&nbsp; &nbsp;Miles&amp;Bonus</option>
		
			<option value="609">&nbsp; &nbsp;&nbsp; &nbsp;LATAM Fidelidade</option>
		
			<option value="622">&nbsp; &nbsp;&nbsp; &nbsp;Km de Vantagens</option>
		
			<option value="151">&nbsp; &nbsp;Avaliações</option>
		
			<option value="236">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da American Airlines</option>
		
			<option value="257">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Alitalia</option>
		
			<option value="254">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Copa Airlines</option>
		
			<option value="626">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Emirates Airline</option>
		
			<option value="426">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Delta Air Lines</option>
		
			<option value="238">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da GOL</option>
		
			<option value="256">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da British Airways</option>
		
			<option value="237">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da TAM</option>
		
			<option value="252">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Iberia</option>
		
			<option value="255">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Qatar Airways</option>
		
			<option value="428">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Air France</option>
		
			<option value="377">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações de Azul</option>
		
			<option value="427">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da United Airlines</option>
		
			<option value="431">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Avianca</option>
		
			<option value="627">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Etihad Airways</option>
		
			<option value="660">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Royal Air Maroc</option>
		
			<option value="669">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da TAP</option>
		
			<option value="670">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Lufthansa</option>
		
			<option value="671">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Singapore Airlines</option>
		
			<option value="201">&nbsp; &nbsp;&nbsp; &nbsp;Testes de Check-in</option>
		
			<option value="131">&nbsp; &nbsp;Salas VIP</option>
		
			<option value="280">&nbsp; &nbsp;&nbsp; &nbsp;GOL Premium Lounge</option>
		
			<option value="234">&nbsp; &nbsp;&nbsp; &nbsp;Bradesco / American Express</option>
		
			<option value="233">&nbsp; &nbsp;&nbsp; &nbsp;American Airlines</option>
		
			<option value="652">&nbsp; &nbsp;&nbsp; &nbsp;Priority Pass</option>
		
			<option value="281">&nbsp; &nbsp;&nbsp; &nbsp;LATAM Airlines</option>
		
			<option value="648">&nbsp; &nbsp;&nbsp; &nbsp;Plaza Premium Lounge</option>
		
			<option value="651">&nbsp; &nbsp;&nbsp; &nbsp;Star Alliance</option>
		
			<option value="283">&nbsp; &nbsp;&nbsp; &nbsp;MasterCard</option>
		
			<option value="380">&nbsp; &nbsp;&nbsp; &nbsp;Delta Air Lines</option>
		
			<option value="704">&nbsp; &nbsp;&nbsp; &nbsp;Air France-KLM</option>
		
			<option value="615">&nbsp; &nbsp;&nbsp; &nbsp;Alitalia</option>
		
			<option value="647">&nbsp; &nbsp;&nbsp; &nbsp;Avianca</option>
		
			<option value="664">&nbsp; &nbsp;&nbsp; &nbsp;British Airways</option>
		
			<option value="650">&nbsp; &nbsp;&nbsp; &nbsp;South African Airways</option>
		
			<option value="649">&nbsp; &nbsp;&nbsp; &nbsp;Bangkok Airways</option>
		
			<option value="455">&nbsp; &nbsp;&nbsp; &nbsp;TAP</option>
		
			<option value="453">&nbsp; &nbsp;&nbsp; &nbsp;United Airlines</option>
		
			<option value="457">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;US Airways Club</option>
		
			<option value="284">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Diners</option>
		
			<option value="282">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;HSBC</option>
		
			<option value="347">&nbsp; &nbsp;Aeronaves</option>
		
			<option value="223">&nbsp; &nbsp;Assentos</option>
		
			<option value="24">&nbsp; &nbsp;Promoções</option>
		
			<option value="146">&nbsp; &nbsp;Reflexões</option>
		
			<option value="228">&nbsp; &nbsp;Franquia de bagagem</option>
		
			<option value="200">&nbsp; &nbsp;Notícias</option>
		
			<option value="338">&nbsp; &nbsp;Internet</option>
		
			<option value="332">&nbsp; &nbsp;Parcelamento</option>
		
			<option value="369">&nbsp; &nbsp;Amenities</option>
		
			<option value="376">&nbsp; &nbsp;Alianças Aéreas</option>
		
			<option value="433">&nbsp; &nbsp;Serviço de bordo</option>
		
			<option value="484">&nbsp; &nbsp;Upgrade</option>
		
			<option value="395">&nbsp; &nbsp;Dúvidas gerais</option>
		
			<option value="119">&nbsp; &nbsp;Companhias aéreas nacionais</option>
		
			<option value="118">&nbsp; &nbsp;Companhias aéreas estrangeiras que operam no Brasil</option>
		
			<option value="120">&nbsp; &nbsp;Companhias aéreas estrangeiras que não operam no Brasil</option>
		
			<option value="5">Aluguel de carros</option>
		
			<option value="554">&nbsp; &nbsp;Reserve aqui o seu carro</option>
		
			<option value="547">&nbsp; &nbsp;Rentcars</option>
		
			<option value="231">&nbsp; &nbsp;Sixt</option>
		
			<option value="226">&nbsp; &nbsp;Hertz</option>
		
			<option value="386">&nbsp; &nbsp;Alamo</option>
		
			<option value="387">&nbsp; &nbsp;Dollar</option>
		
			<option value="408">&nbsp; &nbsp;Localiza</option>
		
			<option value="334">&nbsp; &nbsp;Fox Rent A Car</option>
		
			<option value="193">&nbsp; &nbsp;Carros</option>
		
			<option value="412">&nbsp; &nbsp;&nbsp; &nbsp;Esportivos de luxo</option>
		
			<option value="522">&nbsp; &nbsp;&nbsp; &nbsp;Vídeos de carros alugados</option>
		
			<option value="181">&nbsp; &nbsp;Promoções</option>
		
			<option value="399">&nbsp; &nbsp;Dúvidas gerais</option>
		
			<option value="7">Hotéis</option>
		
			<option value="90">&nbsp; &nbsp;Programas de Fidelidade</option>
		
			<option value="218">&nbsp; &nbsp;&nbsp; &nbsp;Marriott Rewards</option>
		
			<option value="220">&nbsp; &nbsp;&nbsp; &nbsp;Le Club Accorhotels</option>
		
			<option value="310">&nbsp; &nbsp;&nbsp; &nbsp;SPG</option>
		
			<option value="311">&nbsp; &nbsp;&nbsp; &nbsp;Hyatt Gold Passport</option>
		
			<option value="312">&nbsp; &nbsp;&nbsp; &nbsp;Hilton HHonors</option>
		
			<option value="331">&nbsp; &nbsp;&nbsp; &nbsp;IHG Rewards Club</option>
		
			<option value="365">&nbsp; &nbsp;&nbsp; &nbsp;Best Western Rewards</option>
		
			<option value="403">&nbsp; &nbsp;&nbsp; &nbsp;MeliáRewards</option>
		
			<option value="111">&nbsp; &nbsp;Promoções</option>
		
			<option value="122">&nbsp; &nbsp;Pousadas</option>
		
			<option value="349">&nbsp; &nbsp;Club Lounge</option>
		
			<option value="374">&nbsp; &nbsp;Notícias</option>
		
			<option value="422">&nbsp; &nbsp;Dúvidas gerais</option>
		
			<option value="6">Cruzeiros</option>
		
			<option value="134">&nbsp; &nbsp;Roteiros</option>
		
			<option value="608">&nbsp; &nbsp;Disney Dream</option>
		
			<option value="631">&nbsp; &nbsp;Allure of the Seas</option>
		
			<option value="701">&nbsp; &nbsp;Symphony of the Seas</option>
		
			<option value="503">&nbsp; &nbsp;Splendour of the Seas</option>
		
			<option value="574">&nbsp; &nbsp;Anthem of the Seas</option>
		
			<option value="300">&nbsp; &nbsp;Norwegian Epic</option>
		
			<option value="483">&nbsp; &nbsp;MSC Preziosa</option>
		
			<option value="579">&nbsp; &nbsp;MSC Orchestra</option>
		
			<option value="511">&nbsp; &nbsp;Silver Spirit</option>
		
			<option value="470">&nbsp; &nbsp;Rhapsody of the Seas</option>
		
			<option value="325">&nbsp; &nbsp;Promoções</option>
		
			<option value="265">&nbsp; &nbsp;Notícias</option>
		
			<option value="695">&nbsp; &nbsp;Dúvidas gerais</option>
		
			<option value="351">Falando +D</option>
		
			<option value="352">&nbsp; &nbsp;Exclusivas</option>
		
			<option value="370">&nbsp; &nbsp;Econômicas</option>
		
			<option value="653">&nbsp; &nbsp;Família</option>
		
			<option value="4">Cartões de crédito e serviços financeiros</option>
		
			<option value="290">&nbsp; &nbsp;Itaú</option>
		
			<option value="289">&nbsp; &nbsp;Bradesco</option>
		
			<option value="295">&nbsp; &nbsp;Banco do Brasil</option>
		
			<option value="294">&nbsp; &nbsp;Caixa</option>
		
			<option value="291">&nbsp; &nbsp;Santander</option>
		
			<option value="458">&nbsp; &nbsp;Porto Seguro</option>
		
			<option value="209">&nbsp; &nbsp;American Express</option>
		
			<option value="210">&nbsp; &nbsp;Diners</option>
		
			<option value="296">&nbsp; &nbsp;Credicard</option>
		
			<option value="327">&nbsp; &nbsp;Safra</option>
		
			<option value="212" selected="selected">&nbsp; &nbsp;MasterCard</option>
		
			<option value="211">&nbsp; &nbsp;Visa</option>
		
			<option value="607">&nbsp; &nbsp;Digio</option>
		
			<option value="382">&nbsp; &nbsp;PanAmericano</option>
		
			<option value="613">&nbsp; &nbsp;Nubank</option>
		
			<option value="616">&nbsp; &nbsp;Sicoob</option>
		
			<option value="614">&nbsp; &nbsp;Votorantim</option>
		
			<option value="619">&nbsp; &nbsp;Sicredi</option>
		
			<option value="633">&nbsp; &nbsp;Banco Original</option>
		
			<option value="623">&nbsp; &nbsp;ELO</option>
		
			<option value="430">&nbsp; &nbsp;Banrisul</option>
		
			<option value="654">&nbsp; &nbsp;next</option>
		
			<option value="709">&nbsp; &nbsp;Neon</option>
		
			<option value="292">&nbsp; &nbsp;Citibank</option>
		
			<option value="293">&nbsp; &nbsp;HSBC</option>
		
			<option value="258">&nbsp; &nbsp;Anuidade</option>
		
			<option value="250">&nbsp; &nbsp;Concierge</option>
		
			<option value="251">&nbsp; &nbsp;Seguros</option>
		
			<option value="242">&nbsp; &nbsp;Milhas aéreas com Pague Contas</option>
		
			<option value="243">&nbsp; &nbsp;Serviços financeiros internacionais</option>
		
			<option value="263">&nbsp; &nbsp;Programas de Fidelidade</option>
		
			<option value="392">&nbsp; &nbsp;Dúvidas gerais</option>
		
			<option value="26">&nbsp; &nbsp;Promoções</option>
		
			<option value="477">&nbsp; &nbsp;Cotação do dólar mês a mês</option>
		
			<option value="285">&nbsp; &nbsp;Reclamações</option>
		
			<option value="13">Destinos - América do Sul</option>
		
			<option value="30">&nbsp; &nbsp;Argentina</option>
		
			<option value="194">&nbsp; &nbsp;&nbsp; &nbsp;Buenos Aires</option>
		
			<option value="195">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em Buenos Aires</option>
		
			<option value="197">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Buenos Aires</option>
		
			<option value="196">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Buenos Aires</option>
		
			<option value="277">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Buenos Aires</option>
		
			<option value="357">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fotos de Buenos Aires</option>
		
			<option value="155">&nbsp; &nbsp;&nbsp; &nbsp;Bariloche</option>
		
			<option value="529">&nbsp; &nbsp;&nbsp; &nbsp;Rosário</option>
		
			<option value="589">&nbsp; &nbsp;&nbsp; &nbsp;Mendoza</option>
		
			<option value="606">&nbsp; &nbsp;&nbsp; &nbsp;Ushuaia</option>
		
			<option value="702">&nbsp; &nbsp;&nbsp; &nbsp;Salta</option>
		
			<option value="31">&nbsp; &nbsp;Chile</option>
		
			<option value="545">&nbsp; &nbsp;&nbsp; &nbsp;Santiago do Chile</option>
		
			<option value="601">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Santiago do Chile</option>
		
			<option value="602">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Santiago do Chile</option>
		
			<option value="605">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Santiago do Chile</option>
		
			<option value="632">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde fazer compras em Santiago do Chile</option>
		
			<option value="583">&nbsp; &nbsp;&nbsp; &nbsp;Patagônia Chilena</option>
		
			<option value="595">&nbsp; &nbsp;&nbsp; &nbsp;Valle Nevado</option>
		
			<option value="675">&nbsp; &nbsp;&nbsp; &nbsp;Atacama</option>
		
			<option value="546">&nbsp; &nbsp;&nbsp; &nbsp;Valparaíso</option>
		
			<option value="600">&nbsp; &nbsp;&nbsp; &nbsp;Corralco</option>
		
			<option value="688">&nbsp; &nbsp;&nbsp; &nbsp;Viña del Mar</option>
		
			<option value="599">&nbsp; &nbsp;&nbsp; &nbsp;Huilo Huilo</option>
		
			<option value="689">&nbsp; &nbsp;&nbsp; &nbsp;Pucón</option>
		
			<option value="38">&nbsp; &nbsp;Peru</option>
		
			<option value="598">&nbsp; &nbsp;&nbsp; &nbsp;Lima</option>
		
			<option value="512">&nbsp; &nbsp;&nbsp; &nbsp;Machu Picchu</option>
		
			<option value="39">&nbsp; &nbsp;Uruguai</option>
		
			<option value="681">&nbsp; &nbsp;&nbsp; &nbsp;Montevidéu</option>
		
			<option value="680">&nbsp; &nbsp;&nbsp; &nbsp;Punta del Este</option>
		
			<option value="682">&nbsp; &nbsp;&nbsp; &nbsp;Carmelo</option>
		
			<option value="683">&nbsp; &nbsp;&nbsp; &nbsp;Colônia do Sacramento</option>
		
			<option value="33">&nbsp; &nbsp;Colômbia</option>
		
			<option value="677">&nbsp; &nbsp;&nbsp; &nbsp;Bogotá</option>
		
			<option value="679">&nbsp; &nbsp;&nbsp; &nbsp;Cartagena</option>
		
			<option value="34">&nbsp; &nbsp;Equador</option>
		
			<option value="36">&nbsp; &nbsp;Paraguai</option>
		
			<option value="32">&nbsp; &nbsp;Bolívia</option>
		
			<option value="37">&nbsp; &nbsp;Patagônia Argentina, Chilena &amp; Terra do Fogo</option>
		
			<option value="40">&nbsp; &nbsp;Venezuela</option>
		
			<option value="166">&nbsp; &nbsp;&nbsp; &nbsp;Los Roques</option>
		
			<option value="35">&nbsp; &nbsp;Guianas &amp; Suriname</option>
		
			<option value="9">Destinos - América do Norte</option>
		
			<option value="41">&nbsp; &nbsp;Estados Unidos</option>
		
			<option value="337">&nbsp; &nbsp;&nbsp; &nbsp;Flórida</option>
		
			<option value="116">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Orlando</option>
		
			<option value="703">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Orlando com crianças</option>
		
			<option value="154">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Orlando</option>
		
			<option value="274">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Shoppings de Orlando</option>
		
			<option value="247">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Outlets de Orlando</option>
		
			<option value="288">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Centros de Compra em Orlando</option>
		
			<option value="239">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Eletrônicos</option>
		
			<option value="275">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras no Magic Kingdom</option>
		
			<option value="276">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Disney Springs</option>
		
			<option value="153">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em Orlando</option>
		
			<option value="192">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Hollywood Studios</option>
		
			<option value="198">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Epcot</option>
		
			<option value="199">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Animal Kingdom</option>
		
			<option value="203">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes em Disney Springs</option>
		
			<option value="191">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Magic Kingdom</option>
		
			<option value="273">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes na Universal Studios</option>
		
			<option value="272">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Islands of Adventure</option>
		
			<option value="260">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Universal CityWalk</option>
		
			<option value="270">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Mall at Millenia</option>
		
			<option value="271">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Florida Mall</option>
		
			<option value="342">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no SeaWorld</option>
		
			<option value="159">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Orlando</option>
		
			<option value="678">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Casas em Orlando</option>
		
			<option value="145">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Orlando</option>
		
			<option value="177">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Ingressos em Orlando</option>
		
			<option value="204">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Parques Aquáticos de Orlando</option>
		
			<option value="322">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Informações gerais sobre Orlando</option>
		
			<option value="161">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Colunas</option>
		
			<option value="248">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Natal em Orlando</option>
		
			<option value="205">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Orlando mês a mês</option>
		
			<option value="187">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Vídeos de Orlando</option>
		
			<option value="162">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Notícias</option>
		
			<option value="144">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Roteiros de Viagem para Orlando</option>
		
			<option value="202">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Relatos de viagens a Orlando</option>
		
			<option value="495">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Dúvidas gerais</option>
		
			<option value="214">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Atrações no Hollywood Studios</option>
		
			<option value="217">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Atrações no Animal Kingdom</option>
		
			<option value="225">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Atrações no Magic Kingdom</option>
		
			<option value="269">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Atrações no LEGOLAND</option>
		
			<option value="504">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Atrações no SeaWorld</option>
		
			<option value="126">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Miami</option>
		
			<option value="188">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em Miami</option>
		
			<option value="171">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Miami</option>
		
			<option value="156">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Miami</option>
		
			<option value="279">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Miami</option>
		
			<option value="354">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fotos de Miami</option>
		
			<option value="170">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fort Lauderdale</option>
		
			<option value="139">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Key West</option>
		
			<option value="180">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Clearwater</option>
		
			<option value="500">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Palm Beach</option>
		
			<option value="286">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Winter Park</option>
		
			<option value="189">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Saint Petersburg</option>
		
			<option value="339">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Celebration</option>
		
			<option value="344">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Bal Harbour</option>
		
			<option value="262">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Daytona Beach</option>
		
			<option value="593">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Tampa</option>
		
			<option value="594">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Kissimmee</option>
		
			<option value="446">&nbsp; &nbsp;&nbsp; &nbsp;New York</option>
		
			<option value="125">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;New York City</option>
		
			<option value="249">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em Nova York</option>
		
			<option value="182">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Nova York</option>
		
			<option value="179">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Nova York</option>
		
			<option value="213">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Nova York</option>
		
			<option value="259">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Roteiros de Viagem para Nova York</option>
		
			<option value="355">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fotos de Nova York</option>
		
			<option value="438">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Buffalo</option>
		
			<option value="441">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Niagara Falls</option>
		
			<option value="304">&nbsp; &nbsp;&nbsp; &nbsp;Califórnia</option>
		
			<option value="137">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Los Angeles</option>
		
			<option value="384">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Los Angeles</option>
		
			<option value="385">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Los Angeles</option>
		
			<option value="359">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fotos de Los Angeles</option>
		
			<option value="133">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;San Diego</option>
		
			<option value="129">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;San Francisco</option>
		
			<option value="396">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em San Francisco</option>
		
			<option value="345">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em San Francisco</option>
		
			<option value="373">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em San Francisco</option>
		
			<option value="463">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em San Francisco</option>
		
			<option value="333">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Oakland</option>
		
			<option value="335">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Napa</option>
		
			<option value="336">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Anaheim</option>
		
			<option value="346">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Santa Barbara</option>
		
			<option value="348">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Indian Wells</option>
		
			<option value="366">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Carmel</option>
		
			<option value="490">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Lake Tahoe</option>
		
			<option value="534">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Big Bear Lake</option>
		
			<option value="541">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Beverly Hills</option>
		
			<option value="559">&nbsp; &nbsp;&nbsp; &nbsp;Nevada</option>
		
			<option value="123">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Las Vegas</option>
		
			<option value="418">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Las Vegas</option>
		
			<option value="388">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde fazer compras em Las Vegas</option>
		
			<option value="298">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Las Vegas</option>
		
			<option value="389">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Las Vegas</option>
		
			<option value="117">&nbsp; &nbsp;&nbsp; &nbsp;Chicago</option>
		
			<option value="328">&nbsp; &nbsp;&nbsp; &nbsp;Atlanta</option>
		
			<option value="323">&nbsp; &nbsp;&nbsp; &nbsp;Boston</option>
		
			<option value="305">&nbsp; &nbsp;&nbsp; &nbsp;Utah</option>
		
			<option value="222">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Park City</option>
		
			<option value="306">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Salt Lake City</option>
		
			<option value="330">&nbsp; &nbsp;&nbsp; &nbsp;Havaí</option>
		
			<option value="364">&nbsp; &nbsp;&nbsp; &nbsp;Charlotte</option>
		
			<option value="434">&nbsp; &nbsp;&nbsp; &nbsp;Pensilvânia</option>
		
			<option value="176">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Filadélfia</option>
		
			<option value="435">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Pittsburgh</option>
		
			<option value="436">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Amish Country</option>
		
			<option value="439">&nbsp; &nbsp;&nbsp; &nbsp;Ohio</option>
		
			<option value="437">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Cleveland</option>
		
			<option value="440">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Cincinnati</option>
		
			<option value="447">&nbsp; &nbsp;&nbsp; &nbsp;Indianápolis</option>
		
			<option value="448">&nbsp; &nbsp;&nbsp; &nbsp;Milwaukee</option>
		
			<option value="459">&nbsp; &nbsp;&nbsp; &nbsp;Alaska</option>
		
			<option value="460">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Skagway</option>
		
			<option value="461">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Juneau</option>
		
			<option value="462">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Ketchikan</option>
		
			<option value="479">&nbsp; &nbsp;&nbsp; &nbsp;Seattle</option>
		
			<option value="488">&nbsp; &nbsp;&nbsp; &nbsp;Texas</option>
		
			<option value="261">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Houston</option>
		
			<option value="489">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;San Antonio</option>
		
			<option value="141">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Dallas</option>
		
			<option value="496">&nbsp; &nbsp;&nbsp; &nbsp;Colorado</option>
		
			<option value="497">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Steamboat Springs</option>
		
			<option value="498">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Vail</option>
		
			<option value="499">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Aspen</option>
		
			<option value="544">&nbsp; &nbsp;&nbsp; &nbsp;Arizona</option>
		
			<option value="542">&nbsp; &nbsp;&nbsp; &nbsp;Tennessee</option>
		
			<option value="543">&nbsp; &nbsp;&nbsp; &nbsp;Mississippi</option>
		
			<option value="569">&nbsp; &nbsp;&nbsp; &nbsp;Carolina do Sul</option>
		
			<option value="577">&nbsp; &nbsp;&nbsp; &nbsp;Região Capital dos EUA</option>
		
			<option value="130">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Washington DC</option>
		
			<option value="215">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Washington DC</option>
		
			<option value="186">&nbsp; &nbsp;&nbsp; &nbsp;Compras nos Estados Unidos</option>
		
			<option value="475">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras de eletrônicos nos Estados Unidos</option>
		
			<option value="472">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras de enxoval de bebê nos Estados Unidos</option>
		
			<option value="476">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras de roupas nos Estados Unidos</option>
		
			<option value="473">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras para casa nos Estados Unidos</option>
		
			<option value="474">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Cupons de desconto nos Estados Unidos</option>
		
			<option value="266">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Promoções</option>
		
			<option value="329">&nbsp; &nbsp;&nbsp; &nbsp;Roteiros de Viagem pelos Estados Unidos</option>
		
			<option value="183">&nbsp; &nbsp;&nbsp; &nbsp;Alimentação nos Estados Unidos</option>
		
			<option value="390">&nbsp; &nbsp;&nbsp; &nbsp;Transporte nos Estados Unidos</option>
		
			<option value="42">&nbsp; &nbsp;Canadá</option>
		
			<option value="450">&nbsp; &nbsp;&nbsp; &nbsp;Vancouver</option>
		
			<option value="451">&nbsp; &nbsp;&nbsp; &nbsp;Toronto</option>
		
			<option value="514">&nbsp; &nbsp;&nbsp; &nbsp;Montreal</option>
		
			<option value="452">&nbsp; &nbsp;&nbsp; &nbsp;Whistler</option>
		
			<option value="665">&nbsp; &nbsp;&nbsp; &nbsp;Ottawa</option>
		
			<option value="668">&nbsp; &nbsp;&nbsp; &nbsp;Cidade de Quebec</option>
		
			<option value="22">Destinos - América Central, Caribe &amp; México</option>
		
			<option value="59">&nbsp; &nbsp;Caribe</option>
		
			<option value="449">&nbsp; &nbsp;&nbsp; &nbsp;Punta Cana</option>
		
			<option value="521">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Punta Cana</option>
		
			<option value="517">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde fazer compras em Punta Cana</option>
		
			<option value="672">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Punta Cana</option>
		
			<option value="164">&nbsp; &nbsp;&nbsp; &nbsp;Aruba</option>
		
			<option value="235">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em Aruba</option>
		
			<option value="230">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Aruba</option>
		
			<option value="227">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Aruba</option>
		
			<option value="268">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Aruba</option>
		
			<option value="178">&nbsp; &nbsp;&nbsp; &nbsp;Curaçao</option>
		
			<option value="232">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Curaçao</option>
		
			<option value="297">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Curaçao</option>
		
			<option value="360">&nbsp; &nbsp;&nbsp; &nbsp;Barbados</option>
		
			<option value="706">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Barbados</option>
		
			<option value="707">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Barbados</option>
		
			<option value="708">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Barbados</option>
		
			<option value="302">&nbsp; &nbsp;&nbsp; &nbsp;Bahamas</option>
		
			<option value="301">&nbsp; &nbsp;&nbsp; &nbsp;St Maarten</option>
		
			<option value="303">&nbsp; &nbsp;&nbsp; &nbsp;Ilhas Virgens Americanas</option>
		
			<option value="700">&nbsp; &nbsp;&nbsp; &nbsp;Ilhas Virgens Britânicas</option>
		
			<option value="528">&nbsp; &nbsp;&nbsp; &nbsp;Antígua</option>
		
			<option value="515">&nbsp; &nbsp;&nbsp; &nbsp;Turks e Caicos</option>
		
			<option value="571">&nbsp; &nbsp;&nbsp; &nbsp;Jamaica</option>
		
			<option value="586">&nbsp; &nbsp;&nbsp; &nbsp;Cuba</option>
		
			<option value="537">&nbsp; &nbsp;&nbsp; &nbsp;Tobago</option>
		
			<option value="587">&nbsp; &nbsp;&nbsp; &nbsp;Saint-Martin</option>
		
			<option value="646">&nbsp; &nbsp;&nbsp; &nbsp;Santa Lucia</option>
		
			<option value="673">&nbsp; &nbsp;&nbsp; &nbsp;Ilhas Cayman</option>
		
			<option value="60">&nbsp; &nbsp;México</option>
		
			<option value="245">&nbsp; &nbsp;&nbsp; &nbsp;Cancún</option>
		
			<option value="241">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em Cancún</option>
		
			<option value="206">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Cancún</option>
		
			<option value="207">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Roteiros de viagem para Cancún</option>
		
			<option value="172">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Cancún</option>
		
			<option value="264">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Cancún</option>
		
			<option value="362">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fotos de Cancún</option>
		
			<option value="618">&nbsp; &nbsp;&nbsp; &nbsp;Cidade do México</option>
		
			<option value="244">&nbsp; &nbsp;&nbsp; &nbsp;Cozumel</option>
		
			<option value="240">&nbsp; &nbsp;&nbsp; &nbsp;Playa del Carmen</option>
		
			<option value="400">&nbsp; &nbsp;&nbsp; &nbsp;Isla Mujeres</option>
		
			<option value="527">&nbsp; &nbsp;&nbsp; &nbsp;Riviera Maya</option>
		
			<option value="507">&nbsp; &nbsp;&nbsp; &nbsp;Riviera Nayarit</option>
		
			<option value="556">&nbsp; &nbsp;&nbsp; &nbsp;Puebla</option>
		
			<option value="58">&nbsp; &nbsp;Panamá</option>
		
			<option value="52">&nbsp; &nbsp;Belize</option>
		
			<option value="568">&nbsp; &nbsp;Costa Rica</option>
		
			<option value="56">&nbsp; &nbsp;Honduras</option>
		
			<option value="55">&nbsp; &nbsp;Guatemala</option>
		
			<option value="54">&nbsp; &nbsp;El Salvador</option>
		
			<option value="57">&nbsp; &nbsp;Nicarágua</option>
		
			<option value="8">Destinos - Brasil</option>
		
			<option value="16">&nbsp; &nbsp;Região Sudeste</option>
		
			<option value="91">&nbsp; &nbsp;&nbsp; &nbsp;Rio de Janeiro</option>
		
			<option value="184">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer no Rio de Janeiro</option>
		
			<option value="169">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar no Rio de Janeiro</option>
		
			<option value="326">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover no Rio de Janeiro</option>
		
			<option value="148">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;English</option>
		
			<option value="485">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Região dos Lagos</option>
		
			<option value="138">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Búzios</option>
		
			<option value="535">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Arraial do Cabo</option>
		
			<option value="486">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Região Serrana</option>
		
			<option value="381">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Petrópolis / Itaipava</option>
		
			<option value="482">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Teresópolis</option>
		
			<option value="372">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Visconde de Mauá</option>
		
			<option value="340">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Penedo</option>
		
			<option value="190">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Angra dos Reis</option>
		
			<option value="526">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Paraty</option>
		
			<option value="610">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Niterói</option>
		
			<option value="596">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Rio 2016</option>
		
			<option value="92">&nbsp; &nbsp;&nbsp; &nbsp;São Paulo</option>
		
			<option value="368">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em São Paulo</option>
		
			<option value="367">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em São Paulo</option>
		
			<option value="402">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em São Paulo</option>
		
			<option value="140">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Campos do Jordão</option>
		
			<option value="371">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Ilhabela</option>
		
			<option value="375">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Campinas</option>
		
			<option value="409">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Santos</option>
		
			<option value="398">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;São Roque</option>
		
			<option value="552">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Riviera de São Lourenço</option>
		
			<option value="570">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Maresias</option>
		
			<option value="591">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Socorro</option>
		
			<option value="592">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;São Sebastião</option>
		
			<option value="93">&nbsp; &nbsp;&nbsp; &nbsp;Minas Gerais</option>
		
			<option value="494">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Belo Horizonte</option>
		
			<option value="493">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Monte Verde</option>
		
			<option value="492">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;São Lourenço</option>
		
			<option value="661">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Tiradentes</option>
		
			<option value="684">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Inhotim</option>
		
			<option value="696">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Ouro Preto</option>
		
			<option value="94">&nbsp; &nbsp;&nbsp; &nbsp;Espírito Santo</option>
		
			<option value="17">&nbsp; &nbsp;Região Nordeste</option>
		
			<option value="96">&nbsp; &nbsp;&nbsp; &nbsp;Bahia</option>
		
			<option value="698">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Salvador</option>
		
			<option value="697">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Trancoso</option>
		
			<option value="525">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Morro de São Paulo</option>
		
			<option value="524">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Boipeba</option>
		
			<option value="674">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Ilha de Comandatuba</option>
		
			<option value="699">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Caraíva</option>
		
			<option value="97">&nbsp; &nbsp;&nbsp; &nbsp;Pernambuco</option>
		
			<option value="107">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Porto de Galinhas</option>
		
			<option value="406">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Porto de Galinhas</option>
		
			<option value="407">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Porto de Galinhas</option>
		
			<option value="635">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Porto de Galinhas</option>
		
			<option value="425">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Praia dos Carneiros</option>
		
			<option value="106">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fernando de Noronha</option>
		
			<option value="108">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Recife</option>
		
			<option value="98">&nbsp; &nbsp;&nbsp; &nbsp;Rio Grande do Norte</option>
		
			<option value="99">&nbsp; &nbsp;&nbsp; &nbsp;Ceará</option>
		
			<option value="638">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fortaleza</option>
		
			<option value="645">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Jericoacoara</option>
		
			<option value="424">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Beach Park</option>
		
			<option value="639">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Cumbuco</option>
		
			<option value="100">&nbsp; &nbsp;&nbsp; &nbsp;Maranhão</option>
		
			<option value="101">&nbsp; &nbsp;&nbsp; &nbsp;Alagoas</option>
		
			<option value="102">&nbsp; &nbsp;&nbsp; &nbsp;Paraíba</option>
		
			<option value="104">&nbsp; &nbsp;&nbsp; &nbsp;Piauí</option>
		
			<option value="105">&nbsp; &nbsp;&nbsp; &nbsp;Sergipe</option>
		
			<option value="18">&nbsp; &nbsp;Região Sul</option>
		
			<option value="142">&nbsp; &nbsp;&nbsp; &nbsp;Rio Grande do Sul</option>
		
			<option value="510">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Gramado</option>
		
			<option value="562">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Gramado</option>
		
			<option value="564">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde fazer compras em Gramado</option>
		
			<option value="565">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Gramado</option>
		
			<option value="563">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Gramado</option>
		
			<option value="663">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Canela</option>
		
			<option value="163">&nbsp; &nbsp;&nbsp; &nbsp;Santa Catarina</option>
		
			<option value="411">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Florianópolis</option>
		
			<option value="523">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Praia do Rosa</option>
		
			<option value="662">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Beto Carrero World</option>
		
			<option value="597">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Serra do Rio do Rastro</option>
		
			<option value="530">&nbsp; &nbsp;&nbsp; &nbsp;Foz do Iguaçu</option>
		
			<option value="531">&nbsp; &nbsp;&nbsp; &nbsp;Aparados da Serra</option>
		
			<option value="19">&nbsp; &nbsp;Centro-Oeste</option>
		
			<option value="152">&nbsp; &nbsp;&nbsp; &nbsp;Distrito Federal</option>
		
			<option value="502">&nbsp; &nbsp;&nbsp; &nbsp;Bonito</option>
		
			<option value="536">&nbsp; &nbsp;&nbsp; &nbsp;Caldas Novas</option>
		
			<option value="634">&nbsp; &nbsp;&nbsp; &nbsp;Rio Quente Resorts</option>
		
			<option value="20">&nbsp; &nbsp;Região Norte</option>
		
			<option value="10">Destinos - Europa</option>
		
			<option value="69">&nbsp; &nbsp;França</option>
		
			<option value="464">&nbsp; &nbsp;&nbsp; &nbsp;Paris</option>
		
			<option value="580">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Paris</option>
		
			<option value="468">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Paris</option>
		
			<option value="469">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Paris</option>
		
			<option value="612">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde fazer compras em Paris</option>
		
			<option value="466">&nbsp; &nbsp;&nbsp; &nbsp;Alpes</option>
		
			<option value="575">&nbsp; &nbsp;&nbsp; &nbsp;Riviera Francesa</option>
		
			<option value="628">&nbsp; &nbsp;&nbsp; &nbsp;Champagne</option>
		
			<option value="465">&nbsp; &nbsp;&nbsp; &nbsp;Lyon e Beaujolais</option>
		
			<option value="516">&nbsp; &nbsp;&nbsp; &nbsp;Annecy</option>
		
			<option value="590">&nbsp; &nbsp;&nbsp; &nbsp;Borgonha</option>
		
			<option value="620">&nbsp; &nbsp;&nbsp; &nbsp;Nantes</option>
		
			<option value="621">&nbsp; &nbsp;&nbsp; &nbsp;Chamonix</option>
		
			<option value="629">&nbsp; &nbsp;&nbsp; &nbsp;Rouen</option>
		
			<option value="655">&nbsp; &nbsp;Reino Unido</option>
		
			<option value="78">&nbsp; &nbsp;&nbsp; &nbsp;Londres</option>
		
			<option value="405">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Londres</option>
		
			<option value="432">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde fazer compras em Londres</option>
		
			<option value="404">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Londres</option>
		
			<option value="656">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Londres</option>
		
			<option value="657">&nbsp; &nbsp;&nbsp; &nbsp;Liverpool</option>
		
			<option value="658">&nbsp; &nbsp;&nbsp; &nbsp;Manchester</option>
		
			<option value="76">&nbsp; &nbsp;Portugal</option>
		
			<option value="442">&nbsp; &nbsp;&nbsp; &nbsp;Lisboa</option>
		
			<option value="221">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Lisboa</option>
		
			<option value="324">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Lisboa</option>
		
			<option value="444">&nbsp; &nbsp;&nbsp; &nbsp;Cascais</option>
		
			<option value="401">&nbsp; &nbsp;&nbsp; &nbsp;Porto</option>
		
			<option value="445">&nbsp; &nbsp;&nbsp; &nbsp;Sintra</option>
		
			<option value="467">&nbsp; &nbsp;&nbsp; &nbsp;Évora</option>
		
			<option value="471">&nbsp; &nbsp;&nbsp; &nbsp;Ourém/Fátima</option>
		
			<option value="501">&nbsp; &nbsp;&nbsp; &nbsp;Guimarães</option>
		
			<option value="508">&nbsp; &nbsp;&nbsp; &nbsp;Alcácer do Sal</option>
		
			<option value="582">&nbsp; &nbsp;&nbsp; &nbsp;Douro</option>
		
			<option value="584">&nbsp; &nbsp;&nbsp; &nbsp;Serra da Estrela</option>
		
			<option value="588">&nbsp; &nbsp;&nbsp; &nbsp;Alentejo</option>
		
			<option value="73">&nbsp; &nbsp;Itália</option>
		
			<option value="480">&nbsp; &nbsp;&nbsp; &nbsp;Toscana</option>
		
			<option value="481">&nbsp; &nbsp;&nbsp; &nbsp;Roma</option>
		
			<option value="625">&nbsp; &nbsp;&nbsp; &nbsp;Veneza</option>
		
			<option value="572">&nbsp; &nbsp;&nbsp; &nbsp;Milão</option>
		
			<option value="555">&nbsp; &nbsp;&nbsp; &nbsp;Sicília</option>
		
			<option value="573">&nbsp; &nbsp;&nbsp; &nbsp;Riviera Italiana</option>
		
			<option value="611">&nbsp; &nbsp;&nbsp; &nbsp;Nápoles</option>
		
			<option value="66">&nbsp; &nbsp;Espanha</option>
		
			<option value="566">&nbsp; &nbsp;&nbsp; &nbsp;Barcelona</option>
		
			<option value="567">&nbsp; &nbsp;&nbsp; &nbsp;Madrid</option>
		
			<option value="666">&nbsp; &nbsp;&nbsp; &nbsp;Valência</option>
		
			<option value="667">&nbsp; &nbsp;&nbsp; &nbsp;Sevilha</option>
		
			<option value="62">&nbsp; &nbsp;Alemanha</option>
		
			<option value="478">&nbsp; &nbsp;&nbsp; &nbsp;Berlim</option>
		
			<option value="686">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Berlim</option>
		
			<option value="687">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Berlim</option>
		
			<option value="690">&nbsp; &nbsp;&nbsp; &nbsp;Munique</option>
		
			<option value="691">&nbsp; &nbsp;&nbsp; &nbsp;Frankfurt</option>
		
			<option value="64">&nbsp; &nbsp;Bélgica</option>
		
			<option value="63">&nbsp; &nbsp;Áustria</option>
		
			<option value="71">&nbsp; &nbsp;Holanda</option>
		
			<option value="640">&nbsp; &nbsp;&nbsp; &nbsp;Amsterdã</option>
		
			<option value="711">&nbsp; &nbsp;&nbsp; &nbsp;Roterdã</option>
		
			<option value="581">&nbsp; &nbsp;&nbsp; &nbsp;Haia</option>
		
			<option value="89">&nbsp; &nbsp;Rússia</option>
		
			<option value="67">&nbsp; &nbsp;Dinamarca</option>
		
			<option value="70">&nbsp; &nbsp;Grécia</option>
		
			<option value="72">&nbsp; &nbsp;Islândia</option>
		
			<option value="65">&nbsp; &nbsp;Mediterrâneo</option>
		
			<option value="74">&nbsp; &nbsp;Noruega</option>
		
			<option value="75">&nbsp; &nbsp;Luxemburgo</option>
		
			<option value="77">&nbsp; &nbsp;Irlanda</option>
		
			<option value="219">&nbsp; &nbsp;República Checa</option>
		
			<option value="158">&nbsp; &nbsp;Mônaco</option>
		
			<option value="79">&nbsp; &nbsp;Suécia</option>
		
			<option value="80">&nbsp; &nbsp;Suíça</option>
		
			<option value="513">&nbsp; &nbsp;Hungria</option>
		
			<option value="115">&nbsp; &nbsp;Turquia</option>
		
			<option value="557">&nbsp; &nbsp;Polônia</option>
		
			<option value="558">&nbsp; &nbsp;Croácia</option>
		
			<option value="68">&nbsp; &nbsp;Finlândia</option>
		
			<option value="505">&nbsp; &nbsp;Roteiros de Viagem pela Europa</option>
		
			<option value="685">&nbsp; &nbsp;Transporte na Europa</option>
		
			<option value="11">Destinos - Ásia</option>
		
			<option value="518">&nbsp; &nbsp;Tailândia</option>
		
			<option value="641">&nbsp; &nbsp;&nbsp; &nbsp;Bangkok</option>
		
			<option value="643">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Bangkok</option>
		
			<option value="644">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Bangkok</option>
		
			<option value="642">&nbsp; &nbsp;&nbsp; &nbsp;Chiang Mai</option>
		
			<option value="506">&nbsp; &nbsp;Maldivas</option>
		
			<option value="44">&nbsp; &nbsp;China</option>
		
			<option value="603">&nbsp; &nbsp;&nbsp; &nbsp;Hong Kong</option>
		
			<option value="604">&nbsp; &nbsp;&nbsp; &nbsp;Shanghai</option>
		
			<option value="491">&nbsp; &nbsp;Singapura</option>
		
			<option value="46">&nbsp; &nbsp;Japão</option>
		
			<option value="45">&nbsp; &nbsp;Índia</option>
		
			<option value="532">&nbsp; &nbsp;Sri Lanka</option>
		
			<option value="636">&nbsp; &nbsp;Vietnã</option>
		
			<option value="637">&nbsp; &nbsp;Camboja</option>
		
			<option value="48">&nbsp; &nbsp;Nepal, Tibete &amp; Butão</option>
		
			<option value="47">&nbsp; &nbsp;Mongólia</option>
		
			<option value="61">Destinos - África</option>
		
			<option value="81">&nbsp; &nbsp;África do Sul</option>
		
			<option value="549">&nbsp; &nbsp;&nbsp; &nbsp;Cape Town</option>
		
			<option value="692">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar na Cidade do Cabo</option>
		
			<option value="694">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer na Cidade do Cabo</option>
		
			<option value="693">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover na Cidade do Cabo</option>
		
			<option value="548">&nbsp; &nbsp;&nbsp; &nbsp;Sun City</option>
		
			<option value="550">&nbsp; &nbsp;&nbsp; &nbsp;Johannesburg</option>
		
			<option value="551">&nbsp; &nbsp;&nbsp; &nbsp;Kruger National Park</option>
		
			<option value="553">&nbsp; &nbsp;&nbsp; &nbsp;Hermanus</option>
		
			<option value="520">&nbsp; &nbsp;Seychelles</option>
		
			<option value="85">&nbsp; &nbsp;Egito</option>
		
			<option value="86">&nbsp; &nbsp;Marrocos</option>
		
			<option value="585">&nbsp; &nbsp;Ilhas Maurício</option>
		
			<option value="630">&nbsp; &nbsp;Botswana</option>
		
			<option value="83">&nbsp; &nbsp;Angola</option>
		
			<option value="87">&nbsp; &nbsp;Tunísia</option>
		
			<option value="12">Destinos - Oceania</option>
		
			<option value="50">&nbsp; &nbsp;Austrália</option>
		
			<option value="51">&nbsp; &nbsp;Nova Zelândia</option>
		
			<option value="216">&nbsp; &nbsp;Polinésia Francesa</option>
		
			<option value="576">&nbsp; &nbsp;Fiji</option>
		
			<option value="25">Destinos - Oriente Médio</option>
		
			<option value="168">&nbsp; &nbsp;Dubai</option>
		
			<option value="175">&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em Dubai</option>
		
			<option value="278">&nbsp; &nbsp;&nbsp; &nbsp;Compras em Dubai</option>
		
			<option value="174">&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Dubai</option>
		
			<option value="173">&nbsp; &nbsp;Abu Dhabi</option>
		
			<option value="519">&nbsp; &nbsp;Israel</option>
		
			<option value="84">Intercâmbio</option>
		
			<option value="95">Informações para viagens</option>
		
			<option value="224">&nbsp; &nbsp;Alfândega</option>
		
			<option value="160">&nbsp; &nbsp;Passaporte</option>
		
			<option value="132">&nbsp; &nbsp;Vistos</option>
		
			<option value="267">&nbsp; &nbsp;Vacinas</option>
		
			<option value="343">&nbsp; &nbsp;Câmbio</option>
		
			<option value="287">&nbsp; &nbsp;Acessórios úteis em viagens</option>
		
			<option value="309">&nbsp; &nbsp;Seguro Viagem</option>
		
			<option value="423">&nbsp; &nbsp;Malas de viagem</option>
		
			<option value="308">&nbsp; &nbsp;Sites</option>
		
			<option value="391">&nbsp; &nbsp;Compra coletiva</option>
		
			<option value="443">&nbsp; &nbsp;Agências de viagem</option>
		
			<option value="341">Dicas de viagem</option>
		
			<option value="165">Tecnologia</option>
		
			<option value="394">&nbsp; &nbsp;Apple</option>
		
			<option value="410">&nbsp; &nbsp;Câmeras digitais</option>
		
			<option value="321">&nbsp; &nbsp;Aplicativos para celular</option>
		
			<option value="393">&nbsp; &nbsp;GPS</option>
		
			<option value="676">&nbsp; &nbsp;Drones</option>
		
			<option value="397">Empresas Rodoviárias</option>
		
			<option value="509">Entrevistas</option>
		
			<option value="414">Colunas</option>
		
			<option value="415">&nbsp; &nbsp;Coluna do Gabriel Dias</option>
		
			<option value="416">&nbsp; &nbsp;Coluna do Fabio Macedo</option>
		
			<option value="539">&nbsp; &nbsp;Coluna da Juliana Magalhães</option>
		
			<option value="659">&nbsp; &nbsp;Coluna da Manoela Caldas</option>
		
			<option value="88">Falando de Viagem</option>
		
			<option value="167">&nbsp; &nbsp;Informações sobre o Falando de Viagem</option>
		
			<option value="578">&nbsp; &nbsp;Parceiro Plus</option>
		
			<option value="353">&nbsp; &nbsp;Vamos viajar juntos?</option>
		
			<option value="299">&nbsp; &nbsp;Bate-papo</option>
		
			<option value="27">&nbsp; &nbsp;Denuncie</option>
		
			<option value="-1">Selecione um fórum</option>
		<option value="-1">------------------</option>
			<option value="3">Companhias aéreas</option>
		
			<option value="705">&nbsp; &nbsp;Viajando com crianças</option>
		
			<option value="143">&nbsp; &nbsp;Aeroportos</option>
		
			<option value="23">&nbsp; &nbsp;Programas de Fidelidade</option>
		
			<option value="147">&nbsp; &nbsp;&nbsp; &nbsp;AAdvantage</option>
		
			<option value="149">&nbsp; &nbsp;&nbsp; &nbsp;Smiles</option>
		
			<option value="150">&nbsp; &nbsp;&nbsp; &nbsp;TAM Fidelidade</option>
		
			<option value="185">&nbsp; &nbsp;&nbsp; &nbsp;SkyMiles</option>
		
			<option value="208">&nbsp; &nbsp;&nbsp; &nbsp;Victoria</option>
		
			<option value="246">&nbsp; &nbsp;&nbsp; &nbsp;Dividend Miles</option>
		
			<option value="307">&nbsp; &nbsp;&nbsp; &nbsp;MileagePlus</option>
		
			<option value="313">&nbsp; &nbsp;&nbsp; &nbsp;TudoAzul</option>
		
			<option value="314">&nbsp; &nbsp;&nbsp; &nbsp;LANPASS</option>
		
			<option value="315">&nbsp; &nbsp;&nbsp; &nbsp;Iberia Plus</option>
		
			<option value="560">&nbsp; &nbsp;&nbsp; &nbsp;ConnectMiles</option>
		
			<option value="561">&nbsp; &nbsp;&nbsp; &nbsp;AMIGO</option>
		
			<option value="316">&nbsp; &nbsp;&nbsp; &nbsp;Flying Blue</option>
		
			<option value="317">&nbsp; &nbsp;&nbsp; &nbsp;Executive Club</option>
		
			<option value="413">&nbsp; &nbsp;&nbsp; &nbsp;Miles&amp;Bonus</option>
		
			<option value="609">&nbsp; &nbsp;&nbsp; &nbsp;LATAM Fidelidade</option>
		
			<option value="622">&nbsp; &nbsp;&nbsp; &nbsp;Km de Vantagens</option>
		
			<option value="151">&nbsp; &nbsp;Avaliações</option>
		
			<option value="236">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da American Airlines</option>
		
			<option value="257">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Alitalia</option>
		
			<option value="254">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Copa Airlines</option>
		
			<option value="626">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Emirates Airline</option>
		
			<option value="426">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Delta Air Lines</option>
		
			<option value="238">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da GOL</option>
		
			<option value="256">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da British Airways</option>
		
			<option value="237">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da TAM</option>
		
			<option value="252">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Iberia</option>
		
			<option value="255">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Qatar Airways</option>
		
			<option value="428">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Air France</option>
		
			<option value="377">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações de Azul</option>
		
			<option value="427">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da United Airlines</option>
		
			<option value="431">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Avianca</option>
		
			<option value="627">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Etihad Airways</option>
		
			<option value="660">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Royal Air Maroc</option>
		
			<option value="669">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da TAP</option>
		
			<option value="670">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Lufthansa</option>
		
			<option value="671">&nbsp; &nbsp;&nbsp; &nbsp;Avaliações da Singapore Airlines</option>
		
			<option value="201">&nbsp; &nbsp;&nbsp; &nbsp;Testes de Check-in</option>
		
			<option value="131">&nbsp; &nbsp;Salas VIP</option>
		
			<option value="280">&nbsp; &nbsp;&nbsp; &nbsp;GOL Premium Lounge</option>
		
			<option value="234">&nbsp; &nbsp;&nbsp; &nbsp;Bradesco / American Express</option>
		
			<option value="233">&nbsp; &nbsp;&nbsp; &nbsp;American Airlines</option>
		
			<option value="652">&nbsp; &nbsp;&nbsp; &nbsp;Priority Pass</option>
		
			<option value="281">&nbsp; &nbsp;&nbsp; &nbsp;LATAM Airlines</option>
		
			<option value="648">&nbsp; &nbsp;&nbsp; &nbsp;Plaza Premium Lounge</option>
		
			<option value="651">&nbsp; &nbsp;&nbsp; &nbsp;Star Alliance</option>
		
			<option value="283">&nbsp; &nbsp;&nbsp; &nbsp;MasterCard</option>
		
			<option value="380">&nbsp; &nbsp;&nbsp; &nbsp;Delta Air Lines</option>
		
			<option value="704">&nbsp; &nbsp;&nbsp; &nbsp;Air France-KLM</option>
		
			<option value="615">&nbsp; &nbsp;&nbsp; &nbsp;Alitalia</option>
		
			<option value="647">&nbsp; &nbsp;&nbsp; &nbsp;Avianca</option>
		
			<option value="664">&nbsp; &nbsp;&nbsp; &nbsp;British Airways</option>
		
			<option value="650">&nbsp; &nbsp;&nbsp; &nbsp;South African Airways</option>
		
			<option value="649">&nbsp; &nbsp;&nbsp; &nbsp;Bangkok Airways</option>
		
			<option value="455">&nbsp; &nbsp;&nbsp; &nbsp;TAP</option>
		
			<option value="453">&nbsp; &nbsp;&nbsp; &nbsp;United Airlines</option>
		
			<option value="457">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;US Airways Club</option>
		
			<option value="284">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Diners</option>
		
			<option value="282">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;HSBC</option>
		
			<option value="347">&nbsp; &nbsp;Aeronaves</option>
		
			<option value="223">&nbsp; &nbsp;Assentos</option>
		
			<option value="24">&nbsp; &nbsp;Promoções</option>
		
			<option value="146">&nbsp; &nbsp;Reflexões</option>
		
			<option value="228">&nbsp; &nbsp;Franquia de bagagem</option>
		
			<option value="200">&nbsp; &nbsp;Notícias</option>
		
			<option value="338">&nbsp; &nbsp;Internet</option>
		
			<option value="332">&nbsp; &nbsp;Parcelamento</option>
		
			<option value="369">&nbsp; &nbsp;Amenities</option>
		
			<option value="376">&nbsp; &nbsp;Alianças Aéreas</option>
		
			<option value="433">&nbsp; &nbsp;Serviço de bordo</option>
		
			<option value="484">&nbsp; &nbsp;Upgrade</option>
		
			<option value="395">&nbsp; &nbsp;Dúvidas gerais</option>
		
			<option value="119">&nbsp; &nbsp;Companhias aéreas nacionais</option>
		
			<option value="118">&nbsp; &nbsp;Companhias aéreas estrangeiras que operam no Brasil</option>
		
			<option value="120">&nbsp; &nbsp;Companhias aéreas estrangeiras que não operam no Brasil</option>
		
			<option value="5">Aluguel de carros</option>
		
			<option value="554">&nbsp; &nbsp;Reserve aqui o seu carro</option>
		
			<option value="547">&nbsp; &nbsp;Rentcars</option>
		
			<option value="231">&nbsp; &nbsp;Sixt</option>
		
			<option value="226">&nbsp; &nbsp;Hertz</option>
		
			<option value="386">&nbsp; &nbsp;Alamo</option>
		
			<option value="387">&nbsp; &nbsp;Dollar</option>
		
			<option value="408">&nbsp; &nbsp;Localiza</option>
		
			<option value="334">&nbsp; &nbsp;Fox Rent A Car</option>
		
			<option value="193">&nbsp; &nbsp;Carros</option>
		
			<option value="412">&nbsp; &nbsp;&nbsp; &nbsp;Esportivos de luxo</option>
		
			<option value="522">&nbsp; &nbsp;&nbsp; &nbsp;Vídeos de carros alugados</option>
		
			<option value="181">&nbsp; &nbsp;Promoções</option>
		
			<option value="399">&nbsp; &nbsp;Dúvidas gerais</option>
		
			<option value="7">Hotéis</option>
		
			<option value="90">&nbsp; &nbsp;Programas de Fidelidade</option>
		
			<option value="218">&nbsp; &nbsp;&nbsp; &nbsp;Marriott Rewards</option>
		
			<option value="220">&nbsp; &nbsp;&nbsp; &nbsp;Le Club Accorhotels</option>
		
			<option value="310">&nbsp; &nbsp;&nbsp; &nbsp;SPG</option>
		
			<option value="311">&nbsp; &nbsp;&nbsp; &nbsp;Hyatt Gold Passport</option>
		
			<option value="312">&nbsp; &nbsp;&nbsp; &nbsp;Hilton HHonors</option>
		
			<option value="331">&nbsp; &nbsp;&nbsp; &nbsp;IHG Rewards Club</option>
		
			<option value="365">&nbsp; &nbsp;&nbsp; &nbsp;Best Western Rewards</option>
		
			<option value="403">&nbsp; &nbsp;&nbsp; &nbsp;MeliáRewards</option>
		
			<option value="111">&nbsp; &nbsp;Promoções</option>
		
			<option value="122">&nbsp; &nbsp;Pousadas</option>
		
			<option value="349">&nbsp; &nbsp;Club Lounge</option>
		
			<option value="374">&nbsp; &nbsp;Notícias</option>
		
			<option value="422">&nbsp; &nbsp;Dúvidas gerais</option>
		
			<option value="6">Cruzeiros</option>
		
			<option value="134">&nbsp; &nbsp;Roteiros</option>
		
			<option value="608">&nbsp; &nbsp;Disney Dream</option>
		
			<option value="631">&nbsp; &nbsp;Allure of the Seas</option>
		
			<option value="701">&nbsp; &nbsp;Symphony of the Seas</option>
		
			<option value="503">&nbsp; &nbsp;Splendour of the Seas</option>
		
			<option value="574">&nbsp; &nbsp;Anthem of the Seas</option>
		
			<option value="300">&nbsp; &nbsp;Norwegian Epic</option>
		
			<option value="483">&nbsp; &nbsp;MSC Preziosa</option>
		
			<option value="579">&nbsp; &nbsp;MSC Orchestra</option>
		
			<option value="511">&nbsp; &nbsp;Silver Spirit</option>
		
			<option value="470">&nbsp; &nbsp;Rhapsody of the Seas</option>
		
			<option value="325">&nbsp; &nbsp;Promoções</option>
		
			<option value="265">&nbsp; &nbsp;Notícias</option>
		
			<option value="695">&nbsp; &nbsp;Dúvidas gerais</option>
		
			<option value="351">Falando +D</option>
		
			<option value="352">&nbsp; &nbsp;Exclusivas</option>
		
			<option value="370">&nbsp; &nbsp;Econômicas</option>
		
			<option value="653">&nbsp; &nbsp;Família</option>
		
			<option value="4">Cartões de crédito e serviços financeiros</option>
		
			<option value="290">&nbsp; &nbsp;Itaú</option>
		
			<option value="289">&nbsp; &nbsp;Bradesco</option>
		
			<option value="295">&nbsp; &nbsp;Banco do Brasil</option>
		
			<option value="294">&nbsp; &nbsp;Caixa</option>
		
			<option value="291">&nbsp; &nbsp;Santander</option>
		
			<option value="458">&nbsp; &nbsp;Porto Seguro</option>
		
			<option value="209">&nbsp; &nbsp;American Express</option>
		
			<option value="210">&nbsp; &nbsp;Diners</option>
		
			<option value="296">&nbsp; &nbsp;Credicard</option>
		
			<option value="327">&nbsp; &nbsp;Safra</option>
		
			<option value="212" selected="selected">&nbsp; &nbsp;MasterCard</option>
		
			<option value="211">&nbsp; &nbsp;Visa</option>
		
			<option value="607">&nbsp; &nbsp;Digio</option>
		
			<option value="382">&nbsp; &nbsp;PanAmericano</option>
		
			<option value="613">&nbsp; &nbsp;Nubank</option>
		
			<option value="616">&nbsp; &nbsp;Sicoob</option>
		
			<option value="614">&nbsp; &nbsp;Votorantim</option>
		
			<option value="619">&nbsp; &nbsp;Sicredi</option>
		
			<option value="633">&nbsp; &nbsp;Banco Original</option>
		
			<option value="623">&nbsp; &nbsp;ELO</option>
		
			<option value="430">&nbsp; &nbsp;Banrisul</option>
		
			<option value="654">&nbsp; &nbsp;next</option>
		
			<option value="709">&nbsp; &nbsp;Neon</option>
		
			<option value="292">&nbsp; &nbsp;Citibank</option>
		
			<option value="293">&nbsp; &nbsp;HSBC</option>
		
			<option value="258">&nbsp; &nbsp;Anuidade</option>
		
			<option value="250">&nbsp; &nbsp;Concierge</option>
		
			<option value="251">&nbsp; &nbsp;Seguros</option>
		
			<option value="242">&nbsp; &nbsp;Milhas aéreas com Pague Contas</option>
		
			<option value="243">&nbsp; &nbsp;Serviços financeiros internacionais</option>
		
			<option value="263">&nbsp; &nbsp;Programas de Fidelidade</option>
		
			<option value="392">&nbsp; &nbsp;Dúvidas gerais</option>
		
			<option value="26">&nbsp; &nbsp;Promoções</option>
		
			<option value="477">&nbsp; &nbsp;Cotação do dólar mês a mês</option>
		
			<option value="285">&nbsp; &nbsp;Reclamações</option>
		
			<option value="13">Destinos - América do Sul</option>
		
			<option value="30">&nbsp; &nbsp;Argentina</option>
		
			<option value="194">&nbsp; &nbsp;&nbsp; &nbsp;Buenos Aires</option>
		
			<option value="195">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em Buenos Aires</option>
		
			<option value="197">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Buenos Aires</option>
		
			<option value="196">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Buenos Aires</option>
		
			<option value="277">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Buenos Aires</option>
		
			<option value="357">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fotos de Buenos Aires</option>
		
			<option value="155">&nbsp; &nbsp;&nbsp; &nbsp;Bariloche</option>
		
			<option value="529">&nbsp; &nbsp;&nbsp; &nbsp;Rosário</option>
		
			<option value="589">&nbsp; &nbsp;&nbsp; &nbsp;Mendoza</option>
		
			<option value="606">&nbsp; &nbsp;&nbsp; &nbsp;Ushuaia</option>
		
			<option value="702">&nbsp; &nbsp;&nbsp; &nbsp;Salta</option>
		
			<option value="31">&nbsp; &nbsp;Chile</option>
		
			<option value="545">&nbsp; &nbsp;&nbsp; &nbsp;Santiago do Chile</option>
		
			<option value="601">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Santiago do Chile</option>
		
			<option value="602">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Santiago do Chile</option>
		
			<option value="605">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Santiago do Chile</option>
		
			<option value="632">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde fazer compras em Santiago do Chile</option>
		
			<option value="583">&nbsp; &nbsp;&nbsp; &nbsp;Patagônia Chilena</option>
		
			<option value="595">&nbsp; &nbsp;&nbsp; &nbsp;Valle Nevado</option>
		
			<option value="675">&nbsp; &nbsp;&nbsp; &nbsp;Atacama</option>
		
			<option value="546">&nbsp; &nbsp;&nbsp; &nbsp;Valparaíso</option>
		
			<option value="600">&nbsp; &nbsp;&nbsp; &nbsp;Corralco</option>
		
			<option value="688">&nbsp; &nbsp;&nbsp; &nbsp;Viña del Mar</option>
		
			<option value="599">&nbsp; &nbsp;&nbsp; &nbsp;Huilo Huilo</option>
		
			<option value="689">&nbsp; &nbsp;&nbsp; &nbsp;Pucón</option>
		
			<option value="38">&nbsp; &nbsp;Peru</option>
		
			<option value="598">&nbsp; &nbsp;&nbsp; &nbsp;Lima</option>
		
			<option value="512">&nbsp; &nbsp;&nbsp; &nbsp;Machu Picchu</option>
		
			<option value="39">&nbsp; &nbsp;Uruguai</option>
		
			<option value="681">&nbsp; &nbsp;&nbsp; &nbsp;Montevidéu</option>
		
			<option value="680">&nbsp; &nbsp;&nbsp; &nbsp;Punta del Este</option>
		
			<option value="682">&nbsp; &nbsp;&nbsp; &nbsp;Carmelo</option>
		
			<option value="683">&nbsp; &nbsp;&nbsp; &nbsp;Colônia do Sacramento</option>
		
			<option value="33">&nbsp; &nbsp;Colômbia</option>
		
			<option value="677">&nbsp; &nbsp;&nbsp; &nbsp;Bogotá</option>
		
			<option value="679">&nbsp; &nbsp;&nbsp; &nbsp;Cartagena</option>
		
			<option value="34">&nbsp; &nbsp;Equador</option>
		
			<option value="36">&nbsp; &nbsp;Paraguai</option>
		
			<option value="32">&nbsp; &nbsp;Bolívia</option>
		
			<option value="37">&nbsp; &nbsp;Patagônia Argentina, Chilena &amp; Terra do Fogo</option>
		
			<option value="40">&nbsp; &nbsp;Venezuela</option>
		
			<option value="166">&nbsp; &nbsp;&nbsp; &nbsp;Los Roques</option>
		
			<option value="35">&nbsp; &nbsp;Guianas &amp; Suriname</option>
		
			<option value="9">Destinos - América do Norte</option>
		
			<option value="41">&nbsp; &nbsp;Estados Unidos</option>
		
			<option value="337">&nbsp; &nbsp;&nbsp; &nbsp;Flórida</option>
		
			<option value="116">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Orlando</option>
		
			<option value="703">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Orlando com crianças</option>
		
			<option value="154">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Orlando</option>
		
			<option value="274">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Shoppings de Orlando</option>
		
			<option value="247">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Outlets de Orlando</option>
		
			<option value="288">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Centros de Compra em Orlando</option>
		
			<option value="239">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Eletrônicos</option>
		
			<option value="275">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras no Magic Kingdom</option>
		
			<option value="276">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Disney Springs</option>
		
			<option value="153">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em Orlando</option>
		
			<option value="192">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Hollywood Studios</option>
		
			<option value="198">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Epcot</option>
		
			<option value="199">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Animal Kingdom</option>
		
			<option value="203">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes em Disney Springs</option>
		
			<option value="191">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Magic Kingdom</option>
		
			<option value="273">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes na Universal Studios</option>
		
			<option value="272">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Islands of Adventure</option>
		
			<option value="260">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Universal CityWalk</option>
		
			<option value="270">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Mall at Millenia</option>
		
			<option value="271">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no Florida Mall</option>
		
			<option value="342">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Restaurantes no SeaWorld</option>
		
			<option value="159">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Orlando</option>
		
			<option value="678">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Casas em Orlando</option>
		
			<option value="145">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Orlando</option>
		
			<option value="177">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Ingressos em Orlando</option>
		
			<option value="204">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Parques Aquáticos de Orlando</option>
		
			<option value="322">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Informações gerais sobre Orlando</option>
		
			<option value="161">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Colunas</option>
		
			<option value="248">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Natal em Orlando</option>
		
			<option value="205">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Orlando mês a mês</option>
		
			<option value="187">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Vídeos de Orlando</option>
		
			<option value="162">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Notícias</option>
		
			<option value="144">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Roteiros de Viagem para Orlando</option>
		
			<option value="202">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Relatos de viagens a Orlando</option>
		
			<option value="495">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Dúvidas gerais</option>
		
			<option value="214">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Atrações no Hollywood Studios</option>
		
			<option value="217">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Atrações no Animal Kingdom</option>
		
			<option value="225">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Atrações no Magic Kingdom</option>
		
			<option value="269">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Atrações no LEGOLAND</option>
		
			<option value="504">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Atrações no SeaWorld</option>
		
			<option value="126">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Miami</option>
		
			<option value="188">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em Miami</option>
		
			<option value="171">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Miami</option>
		
			<option value="156">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Miami</option>
		
			<option value="279">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Miami</option>
		
			<option value="354">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fotos de Miami</option>
		
			<option value="170">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fort Lauderdale</option>
		
			<option value="139">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Key West</option>
		
			<option value="180">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Clearwater</option>
		
			<option value="500">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Palm Beach</option>
		
			<option value="286">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Winter Park</option>
		
			<option value="189">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Saint Petersburg</option>
		
			<option value="339">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Celebration</option>
		
			<option value="344">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Bal Harbour</option>
		
			<option value="262">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Daytona Beach</option>
		
			<option value="593">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Tampa</option>
		
			<option value="594">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Kissimmee</option>
		
			<option value="446">&nbsp; &nbsp;&nbsp; &nbsp;New York</option>
		
			<option value="125">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;New York City</option>
		
			<option value="249">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em Nova York</option>
		
			<option value="182">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Nova York</option>
		
			<option value="179">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Nova York</option>
		
			<option value="213">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Nova York</option>
		
			<option value="259">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Roteiros de Viagem para Nova York</option>
		
			<option value="355">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fotos de Nova York</option>
		
			<option value="438">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Buffalo</option>
		
			<option value="441">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Niagara Falls</option>
		
			<option value="304">&nbsp; &nbsp;&nbsp; &nbsp;Califórnia</option>
		
			<option value="137">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Los Angeles</option>
		
			<option value="384">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Los Angeles</option>
		
			<option value="385">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Los Angeles</option>
		
			<option value="359">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fotos de Los Angeles</option>
		
			<option value="133">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;San Diego</option>
		
			<option value="129">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;San Francisco</option>
		
			<option value="396">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em San Francisco</option>
		
			<option value="345">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em San Francisco</option>
		
			<option value="373">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em San Francisco</option>
		
			<option value="463">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em San Francisco</option>
		
			<option value="333">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Oakland</option>
		
			<option value="335">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Napa</option>
		
			<option value="336">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Anaheim</option>
		
			<option value="346">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Santa Barbara</option>
		
			<option value="348">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Indian Wells</option>
		
			<option value="366">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Carmel</option>
		
			<option value="490">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Lake Tahoe</option>
		
			<option value="534">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Big Bear Lake</option>
		
			<option value="541">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Beverly Hills</option>
		
			<option value="559">&nbsp; &nbsp;&nbsp; &nbsp;Nevada</option>
		
			<option value="123">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Las Vegas</option>
		
			<option value="418">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Las Vegas</option>
		
			<option value="388">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde fazer compras em Las Vegas</option>
		
			<option value="298">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Las Vegas</option>
		
			<option value="389">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Las Vegas</option>
		
			<option value="117">&nbsp; &nbsp;&nbsp; &nbsp;Chicago</option>
		
			<option value="328">&nbsp; &nbsp;&nbsp; &nbsp;Atlanta</option>
		
			<option value="323">&nbsp; &nbsp;&nbsp; &nbsp;Boston</option>
		
			<option value="305">&nbsp; &nbsp;&nbsp; &nbsp;Utah</option>
		
			<option value="222">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Park City</option>
		
			<option value="306">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Salt Lake City</option>
		
			<option value="330">&nbsp; &nbsp;&nbsp; &nbsp;Havaí</option>
		
			<option value="364">&nbsp; &nbsp;&nbsp; &nbsp;Charlotte</option>
		
			<option value="434">&nbsp; &nbsp;&nbsp; &nbsp;Pensilvânia</option>
		
			<option value="176">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Filadélfia</option>
		
			<option value="435">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Pittsburgh</option>
		
			<option value="436">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Amish Country</option>
		
			<option value="439">&nbsp; &nbsp;&nbsp; &nbsp;Ohio</option>
		
			<option value="437">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Cleveland</option>
		
			<option value="440">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Cincinnati</option>
		
			<option value="447">&nbsp; &nbsp;&nbsp; &nbsp;Indianápolis</option>
		
			<option value="448">&nbsp; &nbsp;&nbsp; &nbsp;Milwaukee</option>
		
			<option value="459">&nbsp; &nbsp;&nbsp; &nbsp;Alaska</option>
		
			<option value="460">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Skagway</option>
		
			<option value="461">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Juneau</option>
		
			<option value="462">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Ketchikan</option>
		
			<option value="479">&nbsp; &nbsp;&nbsp; &nbsp;Seattle</option>
		
			<option value="488">&nbsp; &nbsp;&nbsp; &nbsp;Texas</option>
		
			<option value="261">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Houston</option>
		
			<option value="489">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;San Antonio</option>
		
			<option value="141">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Dallas</option>
		
			<option value="496">&nbsp; &nbsp;&nbsp; &nbsp;Colorado</option>
		
			<option value="497">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Steamboat Springs</option>
		
			<option value="498">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Vail</option>
		
			<option value="499">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Aspen</option>
		
			<option value="544">&nbsp; &nbsp;&nbsp; &nbsp;Arizona</option>
		
			<option value="542">&nbsp; &nbsp;&nbsp; &nbsp;Tennessee</option>
		
			<option value="543">&nbsp; &nbsp;&nbsp; &nbsp;Mississippi</option>
		
			<option value="569">&nbsp; &nbsp;&nbsp; &nbsp;Carolina do Sul</option>
		
			<option value="577">&nbsp; &nbsp;&nbsp; &nbsp;Região Capital dos EUA</option>
		
			<option value="130">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Washington DC</option>
		
			<option value="215">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Washington DC</option>
		
			<option value="186">&nbsp; &nbsp;&nbsp; &nbsp;Compras nos Estados Unidos</option>
		
			<option value="475">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras de eletrônicos nos Estados Unidos</option>
		
			<option value="472">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras de enxoval de bebê nos Estados Unidos</option>
		
			<option value="476">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras de roupas nos Estados Unidos</option>
		
			<option value="473">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras para casa nos Estados Unidos</option>
		
			<option value="474">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Cupons de desconto nos Estados Unidos</option>
		
			<option value="266">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Promoções</option>
		
			<option value="329">&nbsp; &nbsp;&nbsp; &nbsp;Roteiros de Viagem pelos Estados Unidos</option>
		
			<option value="183">&nbsp; &nbsp;&nbsp; &nbsp;Alimentação nos Estados Unidos</option>
		
			<option value="390">&nbsp; &nbsp;&nbsp; &nbsp;Transporte nos Estados Unidos</option>
		
			<option value="42">&nbsp; &nbsp;Canadá</option>
		
			<option value="450">&nbsp; &nbsp;&nbsp; &nbsp;Vancouver</option>
		
			<option value="451">&nbsp; &nbsp;&nbsp; &nbsp;Toronto</option>
		
			<option value="514">&nbsp; &nbsp;&nbsp; &nbsp;Montreal</option>
		
			<option value="452">&nbsp; &nbsp;&nbsp; &nbsp;Whistler</option>
		
			<option value="665">&nbsp; &nbsp;&nbsp; &nbsp;Ottawa</option>
		
			<option value="668">&nbsp; &nbsp;&nbsp; &nbsp;Cidade de Quebec</option>
		
			<option value="22">Destinos - América Central, Caribe &amp; México</option>
		
			<option value="59">&nbsp; &nbsp;Caribe</option>
		
			<option value="449">&nbsp; &nbsp;&nbsp; &nbsp;Punta Cana</option>
		
			<option value="521">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Punta Cana</option>
		
			<option value="517">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde fazer compras em Punta Cana</option>
		
			<option value="672">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Punta Cana</option>
		
			<option value="164">&nbsp; &nbsp;&nbsp; &nbsp;Aruba</option>
		
			<option value="235">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em Aruba</option>
		
			<option value="230">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Aruba</option>
		
			<option value="227">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Aruba</option>
		
			<option value="268">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Aruba</option>
		
			<option value="178">&nbsp; &nbsp;&nbsp; &nbsp;Curaçao</option>
		
			<option value="232">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Curaçao</option>
		
			<option value="297">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Curaçao</option>
		
			<option value="360">&nbsp; &nbsp;&nbsp; &nbsp;Barbados</option>
		
			<option value="706">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Barbados</option>
		
			<option value="707">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Barbados</option>
		
			<option value="708">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Barbados</option>
		
			<option value="302">&nbsp; &nbsp;&nbsp; &nbsp;Bahamas</option>
		
			<option value="301">&nbsp; &nbsp;&nbsp; &nbsp;St Maarten</option>
		
			<option value="303">&nbsp; &nbsp;&nbsp; &nbsp;Ilhas Virgens Americanas</option>
		
			<option value="700">&nbsp; &nbsp;&nbsp; &nbsp;Ilhas Virgens Britânicas</option>
		
			<option value="528">&nbsp; &nbsp;&nbsp; &nbsp;Antígua</option>
		
			<option value="515">&nbsp; &nbsp;&nbsp; &nbsp;Turks e Caicos</option>
		
			<option value="571">&nbsp; &nbsp;&nbsp; &nbsp;Jamaica</option>
		
			<option value="586">&nbsp; &nbsp;&nbsp; &nbsp;Cuba</option>
		
			<option value="537">&nbsp; &nbsp;&nbsp; &nbsp;Tobago</option>
		
			<option value="587">&nbsp; &nbsp;&nbsp; &nbsp;Saint-Martin</option>
		
			<option value="646">&nbsp; &nbsp;&nbsp; &nbsp;Santa Lucia</option>
		
			<option value="673">&nbsp; &nbsp;&nbsp; &nbsp;Ilhas Cayman</option>
		
			<option value="60">&nbsp; &nbsp;México</option>
		
			<option value="245">&nbsp; &nbsp;&nbsp; &nbsp;Cancún</option>
		
			<option value="241">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em Cancún</option>
		
			<option value="206">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Cancún</option>
		
			<option value="207">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Roteiros de viagem para Cancún</option>
		
			<option value="172">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Cancún</option>
		
			<option value="264">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em Cancún</option>
		
			<option value="362">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fotos de Cancún</option>
		
			<option value="618">&nbsp; &nbsp;&nbsp; &nbsp;Cidade do México</option>
		
			<option value="244">&nbsp; &nbsp;&nbsp; &nbsp;Cozumel</option>
		
			<option value="240">&nbsp; &nbsp;&nbsp; &nbsp;Playa del Carmen</option>
		
			<option value="400">&nbsp; &nbsp;&nbsp; &nbsp;Isla Mujeres</option>
		
			<option value="527">&nbsp; &nbsp;&nbsp; &nbsp;Riviera Maya</option>
		
			<option value="507">&nbsp; &nbsp;&nbsp; &nbsp;Riviera Nayarit</option>
		
			<option value="556">&nbsp; &nbsp;&nbsp; &nbsp;Puebla</option>
		
			<option value="58">&nbsp; &nbsp;Panamá</option>
		
			<option value="52">&nbsp; &nbsp;Belize</option>
		
			<option value="568">&nbsp; &nbsp;Costa Rica</option>
		
			<option value="56">&nbsp; &nbsp;Honduras</option>
		
			<option value="55">&nbsp; &nbsp;Guatemala</option>
		
			<option value="54">&nbsp; &nbsp;El Salvador</option>
		
			<option value="57">&nbsp; &nbsp;Nicarágua</option>
		
			<option value="8">Destinos - Brasil</option>
		
			<option value="16">&nbsp; &nbsp;Região Sudeste</option>
		
			<option value="91">&nbsp; &nbsp;&nbsp; &nbsp;Rio de Janeiro</option>
		
			<option value="184">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer no Rio de Janeiro</option>
		
			<option value="169">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar no Rio de Janeiro</option>
		
			<option value="326">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover no Rio de Janeiro</option>
		
			<option value="148">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;English</option>
		
			<option value="485">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Região dos Lagos</option>
		
			<option value="138">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Búzios</option>
		
			<option value="535">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Arraial do Cabo</option>
		
			<option value="486">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Região Serrana</option>
		
			<option value="381">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Petrópolis / Itaipava</option>
		
			<option value="482">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Teresópolis</option>
		
			<option value="372">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Visconde de Mauá</option>
		
			<option value="340">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Penedo</option>
		
			<option value="190">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Angra dos Reis</option>
		
			<option value="526">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Paraty</option>
		
			<option value="610">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Niterói</option>
		
			<option value="596">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Rio 2016</option>
		
			<option value="92">&nbsp; &nbsp;&nbsp; &nbsp;São Paulo</option>
		
			<option value="368">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em São Paulo</option>
		
			<option value="367">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em São Paulo</option>
		
			<option value="402">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Transporte em São Paulo</option>
		
			<option value="140">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Campos do Jordão</option>
		
			<option value="371">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Ilhabela</option>
		
			<option value="375">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Campinas</option>
		
			<option value="409">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Santos</option>
		
			<option value="398">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;São Roque</option>
		
			<option value="552">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Riviera de São Lourenço</option>
		
			<option value="570">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Maresias</option>
		
			<option value="591">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Socorro</option>
		
			<option value="592">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;São Sebastião</option>
		
			<option value="93">&nbsp; &nbsp;&nbsp; &nbsp;Minas Gerais</option>
		
			<option value="494">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Belo Horizonte</option>
		
			<option value="493">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Monte Verde</option>
		
			<option value="492">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;São Lourenço</option>
		
			<option value="661">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Tiradentes</option>
		
			<option value="684">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Inhotim</option>
		
			<option value="696">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Ouro Preto</option>
		
			<option value="94">&nbsp; &nbsp;&nbsp; &nbsp;Espírito Santo</option>
		
			<option value="17">&nbsp; &nbsp;Região Nordeste</option>
		
			<option value="96">&nbsp; &nbsp;&nbsp; &nbsp;Bahia</option>
		
			<option value="698">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Salvador</option>
		
			<option value="697">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Trancoso</option>
		
			<option value="525">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Morro de São Paulo</option>
		
			<option value="524">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Boipeba</option>
		
			<option value="674">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Ilha de Comandatuba</option>
		
			<option value="699">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Caraíva</option>
		
			<option value="97">&nbsp; &nbsp;&nbsp; &nbsp;Pernambuco</option>
		
			<option value="107">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Porto de Galinhas</option>
		
			<option value="406">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Porto de Galinhas</option>
		
			<option value="407">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Porto de Galinhas</option>
		
			<option value="635">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Porto de Galinhas</option>
		
			<option value="425">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Praia dos Carneiros</option>
		
			<option value="106">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fernando de Noronha</option>
		
			<option value="108">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Recife</option>
		
			<option value="98">&nbsp; &nbsp;&nbsp; &nbsp;Rio Grande do Norte</option>
		
			<option value="99">&nbsp; &nbsp;&nbsp; &nbsp;Ceará</option>
		
			<option value="638">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Fortaleza</option>
		
			<option value="645">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Jericoacoara</option>
		
			<option value="424">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Beach Park</option>
		
			<option value="639">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Cumbuco</option>
		
			<option value="100">&nbsp; &nbsp;&nbsp; &nbsp;Maranhão</option>
		
			<option value="101">&nbsp; &nbsp;&nbsp; &nbsp;Alagoas</option>
		
			<option value="102">&nbsp; &nbsp;&nbsp; &nbsp;Paraíba</option>
		
			<option value="104">&nbsp; &nbsp;&nbsp; &nbsp;Piauí</option>
		
			<option value="105">&nbsp; &nbsp;&nbsp; &nbsp;Sergipe</option>
		
			<option value="18">&nbsp; &nbsp;Região Sul</option>
		
			<option value="142">&nbsp; &nbsp;&nbsp; &nbsp;Rio Grande do Sul</option>
		
			<option value="510">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Gramado</option>
		
			<option value="562">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Gramado</option>
		
			<option value="564">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde fazer compras em Gramado</option>
		
			<option value="565">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Gramado</option>
		
			<option value="563">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Gramado</option>
		
			<option value="663">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Canela</option>
		
			<option value="163">&nbsp; &nbsp;&nbsp; &nbsp;Santa Catarina</option>
		
			<option value="411">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Florianópolis</option>
		
			<option value="523">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Praia do Rosa</option>
		
			<option value="662">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Beto Carrero World</option>
		
			<option value="597">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Serra do Rio do Rastro</option>
		
			<option value="530">&nbsp; &nbsp;&nbsp; &nbsp;Foz do Iguaçu</option>
		
			<option value="531">&nbsp; &nbsp;&nbsp; &nbsp;Aparados da Serra</option>
		
			<option value="19">&nbsp; &nbsp;Centro-Oeste</option>
		
			<option value="152">&nbsp; &nbsp;&nbsp; &nbsp;Distrito Federal</option>
		
			<option value="502">&nbsp; &nbsp;&nbsp; &nbsp;Bonito</option>
		
			<option value="536">&nbsp; &nbsp;&nbsp; &nbsp;Caldas Novas</option>
		
			<option value="634">&nbsp; &nbsp;&nbsp; &nbsp;Rio Quente Resorts</option>
		
			<option value="20">&nbsp; &nbsp;Região Norte</option>
		
			<option value="10">Destinos - Europa</option>
		
			<option value="69">&nbsp; &nbsp;França</option>
		
			<option value="464">&nbsp; &nbsp;&nbsp; &nbsp;Paris</option>
		
			<option value="580">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Paris</option>
		
			<option value="468">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Paris</option>
		
			<option value="469">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Paris</option>
		
			<option value="612">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde fazer compras em Paris</option>
		
			<option value="466">&nbsp; &nbsp;&nbsp; &nbsp;Alpes</option>
		
			<option value="575">&nbsp; &nbsp;&nbsp; &nbsp;Riviera Francesa</option>
		
			<option value="628">&nbsp; &nbsp;&nbsp; &nbsp;Champagne</option>
		
			<option value="465">&nbsp; &nbsp;&nbsp; &nbsp;Lyon e Beaujolais</option>
		
			<option value="516">&nbsp; &nbsp;&nbsp; &nbsp;Annecy</option>
		
			<option value="590">&nbsp; &nbsp;&nbsp; &nbsp;Borgonha</option>
		
			<option value="620">&nbsp; &nbsp;&nbsp; &nbsp;Nantes</option>
		
			<option value="621">&nbsp; &nbsp;&nbsp; &nbsp;Chamonix</option>
		
			<option value="629">&nbsp; &nbsp;&nbsp; &nbsp;Rouen</option>
		
			<option value="655">&nbsp; &nbsp;Reino Unido</option>
		
			<option value="78">&nbsp; &nbsp;&nbsp; &nbsp;Londres</option>
		
			<option value="405">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Londres</option>
		
			<option value="432">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde fazer compras em Londres</option>
		
			<option value="404">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Londres</option>
		
			<option value="656">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover em Londres</option>
		
			<option value="657">&nbsp; &nbsp;&nbsp; &nbsp;Liverpool</option>
		
			<option value="658">&nbsp; &nbsp;&nbsp; &nbsp;Manchester</option>
		
			<option value="76">&nbsp; &nbsp;Portugal</option>
		
			<option value="442">&nbsp; &nbsp;&nbsp; &nbsp;Lisboa</option>
		
			<option value="221">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Compras em Lisboa</option>
		
			<option value="324">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Lisboa</option>
		
			<option value="444">&nbsp; &nbsp;&nbsp; &nbsp;Cascais</option>
		
			<option value="401">&nbsp; &nbsp;&nbsp; &nbsp;Porto</option>
		
			<option value="445">&nbsp; &nbsp;&nbsp; &nbsp;Sintra</option>
		
			<option value="467">&nbsp; &nbsp;&nbsp; &nbsp;Évora</option>
		
			<option value="471">&nbsp; &nbsp;&nbsp; &nbsp;Ourém/Fátima</option>
		
			<option value="501">&nbsp; &nbsp;&nbsp; &nbsp;Guimarães</option>
		
			<option value="508">&nbsp; &nbsp;&nbsp; &nbsp;Alcácer do Sal</option>
		
			<option value="582">&nbsp; &nbsp;&nbsp; &nbsp;Douro</option>
		
			<option value="584">&nbsp; &nbsp;&nbsp; &nbsp;Serra da Estrela</option>
		
			<option value="588">&nbsp; &nbsp;&nbsp; &nbsp;Alentejo</option>
		
			<option value="73">&nbsp; &nbsp;Itália</option>
		
			<option value="480">&nbsp; &nbsp;&nbsp; &nbsp;Toscana</option>
		
			<option value="481">&nbsp; &nbsp;&nbsp; &nbsp;Roma</option>
		
			<option value="625">&nbsp; &nbsp;&nbsp; &nbsp;Veneza</option>
		
			<option value="572">&nbsp; &nbsp;&nbsp; &nbsp;Milão</option>
		
			<option value="555">&nbsp; &nbsp;&nbsp; &nbsp;Sicília</option>
		
			<option value="573">&nbsp; &nbsp;&nbsp; &nbsp;Riviera Italiana</option>
		
			<option value="611">&nbsp; &nbsp;&nbsp; &nbsp;Nápoles</option>
		
			<option value="66">&nbsp; &nbsp;Espanha</option>
		
			<option value="566">&nbsp; &nbsp;&nbsp; &nbsp;Barcelona</option>
		
			<option value="567">&nbsp; &nbsp;&nbsp; &nbsp;Madrid</option>
		
			<option value="666">&nbsp; &nbsp;&nbsp; &nbsp;Valência</option>
		
			<option value="667">&nbsp; &nbsp;&nbsp; &nbsp;Sevilha</option>
		
			<option value="62">&nbsp; &nbsp;Alemanha</option>
		
			<option value="478">&nbsp; &nbsp;&nbsp; &nbsp;Berlim</option>
		
			<option value="686">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Berlim</option>
		
			<option value="687">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Berlim</option>
		
			<option value="690">&nbsp; &nbsp;&nbsp; &nbsp;Munique</option>
		
			<option value="691">&nbsp; &nbsp;&nbsp; &nbsp;Frankfurt</option>
		
			<option value="64">&nbsp; &nbsp;Bélgica</option>
		
			<option value="63">&nbsp; &nbsp;Áustria</option>
		
			<option value="71">&nbsp; &nbsp;Holanda</option>
		
			<option value="640">&nbsp; &nbsp;&nbsp; &nbsp;Amsterdã</option>
		
			<option value="711">&nbsp; &nbsp;&nbsp; &nbsp;Roterdã</option>
		
			<option value="581">&nbsp; &nbsp;&nbsp; &nbsp;Haia</option>
		
			<option value="89">&nbsp; &nbsp;Rússia</option>
		
			<option value="67">&nbsp; &nbsp;Dinamarca</option>
		
			<option value="70">&nbsp; &nbsp;Grécia</option>
		
			<option value="72">&nbsp; &nbsp;Islândia</option>
		
			<option value="65">&nbsp; &nbsp;Mediterrâneo</option>
		
			<option value="74">&nbsp; &nbsp;Noruega</option>
		
			<option value="75">&nbsp; &nbsp;Luxemburgo</option>
		
			<option value="77">&nbsp; &nbsp;Irlanda</option>
		
			<option value="219">&nbsp; &nbsp;República Checa</option>
		
			<option value="158">&nbsp; &nbsp;Mônaco</option>
		
			<option value="79">&nbsp; &nbsp;Suécia</option>
		
			<option value="80">&nbsp; &nbsp;Suíça</option>
		
			<option value="513">&nbsp; &nbsp;Hungria</option>
		
			<option value="115">&nbsp; &nbsp;Turquia</option>
		
			<option value="557">&nbsp; &nbsp;Polônia</option>
		
			<option value="558">&nbsp; &nbsp;Croácia</option>
		
			<option value="68">&nbsp; &nbsp;Finlândia</option>
		
			<option value="505">&nbsp; &nbsp;Roteiros de Viagem pela Europa</option>
		
			<option value="685">&nbsp; &nbsp;Transporte na Europa</option>
		
			<option value="11">Destinos - Ásia</option>
		
			<option value="518">&nbsp; &nbsp;Tailândia</option>
		
			<option value="641">&nbsp; &nbsp;&nbsp; &nbsp;Bangkok</option>
		
			<option value="643">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar em Bangkok</option>
		
			<option value="644">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer em Bangkok</option>
		
			<option value="642">&nbsp; &nbsp;&nbsp; &nbsp;Chiang Mai</option>
		
			<option value="506">&nbsp; &nbsp;Maldivas</option>
		
			<option value="44">&nbsp; &nbsp;China</option>
		
			<option value="603">&nbsp; &nbsp;&nbsp; &nbsp;Hong Kong</option>
		
			<option value="604">&nbsp; &nbsp;&nbsp; &nbsp;Shanghai</option>
		
			<option value="491">&nbsp; &nbsp;Singapura</option>
		
			<option value="46">&nbsp; &nbsp;Japão</option>
		
			<option value="45">&nbsp; &nbsp;Índia</option>
		
			<option value="532">&nbsp; &nbsp;Sri Lanka</option>
		
			<option value="636">&nbsp; &nbsp;Vietnã</option>
		
			<option value="637">&nbsp; &nbsp;Camboja</option>
		
			<option value="48">&nbsp; &nbsp;Nepal, Tibete &amp; Butão</option>
		
			<option value="47">&nbsp; &nbsp;Mongólia</option>
		
			<option value="61">Destinos - África</option>
		
			<option value="81">&nbsp; &nbsp;África do Sul</option>
		
			<option value="549">&nbsp; &nbsp;&nbsp; &nbsp;Cape Town</option>
		
			<option value="692">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde se hospedar na Cidade do Cabo</option>
		
			<option value="694">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Onde comer na Cidade do Cabo</option>
		
			<option value="693">&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Como se locomover na Cidade do Cabo</option>
		
			<option value="548">&nbsp; &nbsp;&nbsp; &nbsp;Sun City</option>
		
			<option value="550">&nbsp; &nbsp;&nbsp; &nbsp;Johannesburg</option>
		
			<option value="551">&nbsp; &nbsp;&nbsp; &nbsp;Kruger National Park</option>
		
			<option value="553">&nbsp; &nbsp;&nbsp; &nbsp;Hermanus</option>
		
			<option value="520">&nbsp; &nbsp;Seychelles</option>
		
			<option value="85">&nbsp; &nbsp;Egito</option>
		
			<option value="86">&nbsp; &nbsp;Marrocos</option>
		
			<option value="585">&nbsp; &nbsp;Ilhas Maurício</option>
		
			<option value="630">&nbsp; &nbsp;Botswana</option>
		
			<option value="83">&nbsp; &nbsp;Angola</option>
		
			<option value="87">&nbsp; &nbsp;Tunísia</option>
		
			<option value="12">Destinos - Oceania</option>
		
			<option value="50">&nbsp; &nbsp;Austrália</option>
		
			<option value="51">&nbsp; &nbsp;Nova Zelândia</option>
		
			<option value="216">&nbsp; &nbsp;Polinésia Francesa</option>
		
			<option value="576">&nbsp; &nbsp;Fiji</option>
		
			<option value="25">Destinos - Oriente Médio</option>
		
			<option value="168">&nbsp; &nbsp;Dubai</option>
		
			<option value="175">&nbsp; &nbsp;&nbsp; &nbsp;Alimentação em Dubai</option>
		
			<option value="278">&nbsp; &nbsp;&nbsp; &nbsp;Compras em Dubai</option>
		
			<option value="174">&nbsp; &nbsp;&nbsp; &nbsp;Hotéis em Dubai</option>
		
			<option value="173">&nbsp; &nbsp;Abu Dhabi</option>
		
			<option value="519">&nbsp; &nbsp;Israel</option>
		
			<option value="84">Intercâmbio</option>
		
			<option value="95">Informações para viagens</option>
		
			<option value="224">&nbsp; &nbsp;Alfândega</option>
		
			<option value="160">&nbsp; &nbsp;Passaporte</option>
		
			<option value="132">&nbsp; &nbsp;Vistos</option>
		
			<option value="267">&nbsp; &nbsp;Vacinas</option>
		
			<option value="343">&nbsp; &nbsp;Câmbio</option>
		
			<option value="287">&nbsp; &nbsp;Acessórios úteis em viagens</option>
		
			<option value="309">&nbsp; &nbsp;Seguro Viagem</option>
		
			<option value="423">&nbsp; &nbsp;Malas de viagem</option>
		
			<option value="308">&nbsp; &nbsp;Sites</option>
		
			<option value="391">&nbsp; &nbsp;Compra coletiva</option>
		
			<option value="443">&nbsp; &nbsp;Agências de viagem</option>
		
			<option value="341">Dicas de viagem</option>
		
			<option value="165">Tecnologia</option>
		
			<option value="394">&nbsp; &nbsp;Apple</option>
		
			<option value="410">&nbsp; &nbsp;Câmeras digitais</option>
		
			<option value="321">&nbsp; &nbsp;Aplicativos para celular</option>
		
			<option value="393">&nbsp; &nbsp;GPS</option>
		
			<option value="676">&nbsp; &nbsp;Drones</option>
		
			<option value="397">Empresas Rodoviárias</option>
		
			<option value="509">Entrevistas</option>
		
			<option value="414">Colunas</option>
		
			<option value="415">&nbsp; &nbsp;Coluna do Gabriel Dias</option>
		
			<option value="416">&nbsp; &nbsp;Coluna do Fabio Macedo</option>
		
			<option value="539">&nbsp; &nbsp;Coluna da Juliana Magalhães</option>
		
			<option value="659">&nbsp; &nbsp;Coluna da Manoela Caldas</option>
		
			<option value="88">Falando de Viagem</option>
		
			<option value="167">&nbsp; &nbsp;Informações sobre o Falando de Viagem</option>
		
			<option value="578">&nbsp; &nbsp;Parceiro Plus</option>
		
			<option value="353">&nbsp; &nbsp;Vamos viajar juntos?</option>
		
			<option value="299">&nbsp; &nbsp;Bate-papo</option>
		
			<option value="27">&nbsp; &nbsp;Denuncie</option>
		

		</select>&nbsp;<input class="btnlite" type="submit" value="Ir"/></td>
	</tr>
	</table>

	</form>
</td>
</tr>
</table>
</div>

</div> <!-- pagecontent -->
            </article>
        </section>
    </div>
</div>

</td>
    <td width="1"></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="42" height="16"></td>
    <td height="16">&nbsp;</td>
    <td width="42" height="16"></td>
  </tr>
</table>

</div>


  <br clear="all"/>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="42" height="16"><img src="https://www.falandodeviagem.com.br/styles/new_beach/theme/images/tl.gif" width="42" height="16" alt=""/></td>
    <td height="16" style="background:url(styles/new_beach/theme/images/tm.gif.pagespeed.ce._OG-6Gce5P.gif)">&nbsp;</td>
    <td width="42" height="16"><img src="styles/new_beach/theme/images/tr.gif.pagespeed.ce.ZLPNCtp1GE.gif" width="42" height="16" alt=""/></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="18" style="background:url(styles/new_beach/theme/images/ml.gif)"></td>
    <td>
  <table class="tablebg" width="100%" cellspacing="0">
  <tr>
    <td class="catb"><h4>Quem está online</h4></td>
  </tr>
  <tr>
    <td class="row1"><p class="gensmall">Usuários navegando neste fórum: Nenhum usuário registrado e 3 visitantes</p></td>
  </tr>
  </table>
</td>
    <td width="17" style="background:url(styles/new_beach/theme/images/mr.gif)"></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="42" height="16"><img src="https://www.falandodeviagem.com.br/styles/new_beach/theme/images/bl.gif" width="42" height="16" alt=""/></td>
    <td height="16" style="background:url(styles/new_beach/theme/images/bm.gif.pagespeed.ce.Kz9nwfUnr6.gif)">&nbsp;</td>
    <td width="42" height="16"><img src="styles/new_beach/theme/images/br.gif.pagespeed.ce.Jzgyb6kmWu.gif" width="42" height="16" alt=""/></td>
  </tr>
</table>
<div class="mobile-style-switch mobile-style-switch-footer" style="padding: 5px; text-align: center;"><a href="https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049&amp;sid=5aa91c30d4923ae7dc55879a7590b259&amp;mobile=mobile">VERSÃO MOBILE</a></div><center>
<!-- banner_rodape -->
<div id='div-gpt-ad-1380752548597-1' style='width:728px; height:90px;'>
<script type='text/javascript'>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1380752548597-1');});</script>
</div>
</center>


<!--script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-17636981-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  jQuery( document ).ready(function() {      
    //jQuery('.wrap').css({"width": "auto"});
  });
</script-->
</td>
</tr>
</table>
<section>
    <footer id='footer'>
          <div class='limit-1190'>
            <div class='left'>
              <nav>
                <ul>
                  <li>
                    <a data-remote="true" href="http://app.falandodeviagem.com.br/account/login">Blog</a>
                  </li>
                  <li><a href="http://app.falandodeviagem.com.br/revista">Revista</a></li>
                  <li><a href="http://app.falandodeviagem.com.br/page/about-us">Sobre nós</a></li>
                  <li><a href="http://app.falandodeviagem.com.br/avaliacoes">Faça uma avaliação</a></li>
                  <li>
                    <a data-remote="true" href="http://app.falandodeviagem.com.br/account/login">Meu perfil</a>
                  </li>
                  <li>
                    <a href='http://app.falandodeviagem.com.br'>Perfil plus</a>
                  </li>
                  <li><a href="http://app.falandodeviagem.com.br/page/owners">Proprietários</a></li>
                  <li><a href="http://app.falandodeviagem.com.br/page/business">Empresas</a></li>
                  <li><a href="http://app.falandodeviagem.com.br/page/press">Imprensa</a></li>
                  <li><a href="http://app.falandodeviagem.com.br/page/terms">Termo e condições</a></li>
                  <li><a href="http://app.falandodeviagem.com.br/page/privacy">Privacidade</a></li>
                </ul>
              </nav>
              <h4>Sobre Nós</h4>
              <p><a class="about-us" href="//app.falandodeviagem.com.br/page/about-us"> O Falando de Viagem® é um dos maiores sites de viagem do Brasil. Lançado em 2010, inovou na maneira como aborda as informações e dicas sobre viagem, co...</a>
              </p>
              <div class='follow-us'>
                <ul>
                  <li>
                    <a class='spr fb external' href='https://www.facebook.com/FalandoDeViagem' target='blank'>facebook</a>
                  </li>
                  <li>
                    <a class='spr goo external' href='https://plus.google.com/u/0/106394975895440310613' target='blank'>google</a>
                  </li>
                  <li>
                    <a class='spr ytb external' href='https://www.youtube.com/falandodeviagem' target='blank'>youtube</a>
                  </li>
                  <li>
                    <a class='spr tt external' href='https://twitter.com/falandodeviagem' target='blank'>twitter</a>
                  </li>
                  <li>
                    <a class='spr stg external' href='http://instagram.com/falandodeviagem' target='blank'>instagram</a>
                  </li>
                </ul>
              </div>
              <p class='copy'>© -   Copyright - Todos os Direitos reservados</p>
            </div>
            <div class='right'>
              <h4>Hotéis Populares</h4>              
              <ul class='hotel-pop-ft'>
                <li><a title="Barcelona Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/barcelona">Barcelona Hotéis</a></li><li><a title="Bariloche Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/san-carlos-de-bariloche">Bariloche Hotéis</a></li><li><a title="Buenos Aires Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/buenos-aires">Buenos Air... Hotéis</a></li><li><a title="Cairo Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/cairo">Cairo Hotéis</a></li><li><a title="Campos do Jordão Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/campos-do-jordao">Campos do ... Hotéis</a></li><li><a title="Cancún Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/cancun">Cancún Hotéis</a></li><li><a title="Cusco Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/cuscos">Cusco Hotéis</a></li><li><a title="Fernando de Noronha Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/fernando-de-noronha">Fernando d... Hotéis</a></li><li><a title="Florianópolis Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/florianopolis">Florianóp... Hotéis</a></li><li><a title="Foz do Iguaçu Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/foz-do-iguacu">Foz do Igu... Hotéis</a></li><li><a title="Gramado Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/gramado">Gramado Hotéis</a></li><li><a title="Las Vegas Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/las-vegas">Las Vegas Hotéis</a></li><li><a title="Lisboa Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/lisbon">Lisboa Hotéis</a></li><li><a title="Londres Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/londres">Londres Hotéis</a></li><li><a title="Los Angeles Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/los-angeles">Los Angele... Hotéis</a></li><li><a title="Malé Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/male">Malé Hotéis</a></li><li><a title="Miami Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/miami">Miami Hotéis</a></li><li><a title="Munique Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/munique">Munique Hotéis</a></li><li><a title="New York City Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/new-york">New York C... Hotéis</a></li><li><a title="Orlando Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/orlando">Orlando Hotéis</a></li><li><a title="Porto de Galinhas Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/porto-de-galinhas">Porto de G... Hotéis</a></li><li><a title="Punta del Este Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/punta-del-este">Punta del ... Hotéis</a></li><li><a title="Rio de Janeiro Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/rio-de-janeiro">Rio de Jan... Hotéis</a></li><li><a title="Salvador Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/salvador">Salvador Hotéis</a></li><li><a title="San Francisco Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/san-francisco">San Franci... Hotéis</a></li><li><a title="Santiago do Chile Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/santiago">Santiago d... Hotéis</a></li><li><a title="São Paulo Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/sao-paulo">São Paulo Hotéis</a></li><li><a title="Veneza Hotéis" href="http://app.falandodeviagem.com.br/destinos_list/hotels/nova-veneza">Veneza Hotéis</a></li>
              </ul>
              <h4>Destinos Populares</h4>
              <ul class='destino-pop-ft'>
                <li><a title="Barcelona" href="http://app.falandodeviagem.com.br/destinos/espanha/catalonia/barcelona">Barcelona</a></li><li><a title="Bariloche" href="http://app.falandodeviagem.com.br/destinos/argentina/rio-negro/san-carlos-de-bariloche">Bariloche</a></li><li><a title="Buenos Aires" href="http://app.falandodeviagem.com.br/destinos/argentina/buenos-aires">Buenos Aires</a></li><li><a title="Cairo" href="http://app.falandodeviagem.com.br/destinos/egito/mu-afaz-at-al-qahirah/cairo">Cairo</a></li><li><a title="Campos do Jordão" href="http://app.falandodeviagem.com.br/destinos/brasil/sao-paulo%09/campos-do-jordao">Campos do Jordão</a></li><li><a title="Cancún" href="http://app.falandodeviagem.com.br/destinos/mexico/qr/cancun">Cancún</a></li><li><a title="Cusco" href="http://app.falandodeviagem.com.br/destinos/peru/cuscos">Cusco</a></li><li><a title="Fernando de Noronha" href="http://app.falandodeviagem.com.br/destinos/brasil/pernambuco/fernando-de-noronha">Fernando de Noronha</a></li><li><a title="Florianópolis" href="http://app.falandodeviagem.com.br/destinos/brasil/santa-catarina/florianopolis">Florianópolis</a></li><li><a title="Foz do Iguaçu" href="http://app.falandodeviagem.com.br/destinos/brasil/parana/foz-do-iguacu">Foz do Iguaçu</a></li><li><a title="Gramado" href="http://app.falandodeviagem.com.br/destinos/brasil/rio-grande-do-sul/gramado">Gramado</a></li><li><a title="Las Vegas" href="http://app.falandodeviagem.com.br/destinos/estados-unidos-da-america/nevada/las-vegas">Las Vegas</a></li><li><a title="Lisboa" href="http://app.falandodeviagem.com.br/destinos/portugal/lisboa/lisbon">Lisboa</a></li><li><a title="Londres" href="http://app.falandodeviagem.com.br/destinos/reino-unido/inglaterra/londres">Londres</a></li><li><a title="Los Angeles" href="http://app.falandodeviagem.com.br/destinos/estados-unidos-da-america/ca/los-angeles">Los Angeles</a></li><li><a title="Malé" href="http://app.falandodeviagem.com.br/destinos/maldivas/maale/male">Malé</a></li><li><a title="Miami" href="http://app.falandodeviagem.com.br/destinos/estados-unidos-da-america/fl/miami">Miami</a></li><li><a title="Munique" href="http://app.falandodeviagem.com.br/destinos/alemanha/bayern/munique">Munique</a></li><li><a title="New York City" href="http://app.falandodeviagem.com.br/destinos/estados-unidos-da-america/ny/new-york">New York City</a></li><li><a title="Orlando" href="http://app.falandodeviagem.com.br/destinos/estados-unidos-da-america/fl/orlando">Orlando</a></li><li><a title="Porto de Galinhas" href="http://app.falandodeviagem.com.br/destinos/brasil/pernambuco/porto-de-galinhas">Porto de Galinhas</a></li><li><a title="Punta del Este" href="http://app.falandodeviagem.com.br/destinos/uruguai/maldonado/punta-del-este">Punta del Este</a></li><li><a title="Rio de Janeiro" href="http://app.falandodeviagem.com.br/destinos/brasil/s_rio-de-janeiro/rio-de-janeiro">Rio de Janeiro</a></li><li><a title="Salvador" href="http://app.falandodeviagem.com.br/destinos/brasil/bahia/salvador">Salvador</a></li><li><a title="San Francisco" href="http://app.falandodeviagem.com.br/destinos/estados-unidos-da-america/ca/san-francisco">San Francisco</a></li><li><a title="Santiago do Chile" href="http://app.falandodeviagem.com.br/destinos/chile/santiago-metropolitan/santiago">Santiago do Chile</a></li><li><a title="São Paulo" href="http://app.falandodeviagem.com.br/destinos/brasil/sao-paulo%09/sao-paulo">São Paulo</a></li><li><a title="Veneza" href="http://app.falandodeviagem.com.br/destinos/brasil/santa-catarina/nova-veneza">Veneza</a></li>
              </ul>

               <div class='footer-logo'>

                 Parceiro: <a href='https://diversao.r7.com/viagens' target="blank" title='R7 Viagens'><img src="/images/r7viagens.png" alt="R7 Viagens" style="width:121px;"></a>

              </div>        
	     
              <div class='footer-logo'>

                 desenvolvido por: <a href='https://www.falandodeviagem.com.br//www.clariontechnologies.co.in' target='blank' title="Clarion Technologies"><img src="/images/clarion-logo.png.pagespeed.ce.qx8iZI7MWB.png" alt="Clarion-Technologies"></a>

              </div>
	      
            </div>
          </div>
          <!-- end limit-1190 -->
        </footer>
    </section>
<script type="text/javascript">FB.Event.subscribe('edge.create',function(response){$("#popcon").css("display","none");$.cookie("liked","true",{expires:3650,path:'/'});});</script>
<img alt="" src="/images/xfdv-logo.jpg.pagespeed.ic.svhVAEdz36.jpg" style="height:0px;width:0px">

<link href="https://fonts.googleapis.com/css?family=Arimo" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- inclusão do menu fixo lateral --><!--div class="plus_btn">
    <a href="#" class="plus_btn_a">
        <span class="fa fa-plus"></span>
    </a>
</div-->

<div class="icons">
        <ul>
            <li>
                <div class="icon_div">
                    <a href=" https://app.falandodeviagem.com.br/">
                        <img src="styles/new_beach/template/imgages/home.png" alt="" title=""/>
                        <p>Home</p>
                    </a>
                </div>
            </li>
            <li>
                <div class="icon_div">
                <a href="https://app.falandodeviagem.com.br/">
                    <img src="styles/new_beach/template/imgages/reserve.png" alt="" title=""/>
                    <p>Reverse</p>
                </a>
                </div>
            </li>
            <li>
                <div class="icon_div">
                <a href="https://app.falandodeviagem.com.br/avaliacoes">
                    <img src="styles/new_beach/template/imgages/Avalie.png" alt="" title=""/>
                    <p>Avalie</p>
                </a>
                </div>
            </li>
            <li>
                <div class="icon_div">
                <a href="https://falandodeviagem.com.br/index.php">
                    <img src="styles/new_beach/template/imgages/forum.png" alt="" title=""/>
                    <p>Forum</p>
                </a>
                </div>
            </li>
            <li>
                <div class="icon_div">
                <a href="http://app.falandodeviagem.com.br/fotos">
                    <img src="styles/new_beach/template/imgages/photos.png" alt="" title=""/>
                    <p>Fotos</p>
                </a>
                </div>
            </li>
            <li>
                <div class="icon_div">
                <a href="https://app.falandodeviagem.com.br/destinos">
                    <img src="styles/new_beach/template/imgages/Destinos.png" alt="" title=""/>
                    <p>Destinos</p>
                </a>
                </div>
            </li>
            <li>
                <div class="icon_div">
                <a href="https://app.falandodeviagem.com.br/revista">
                    <img src="styles/new_beach/template/imgages/Revista.png.pagespeed.ce.kbLM39rbpP.png" alt="" title=""/>
                    <p>Revista</p>
                </a>
                </div>
            </li>
            <li>
                <div class="icon_div">  
                <a href="https://app.falandodeviagem.com.br/partners">
                    <img src="styles/new_beach/template/imgages/Parceiros.png.pagespeed.ce.ALEJOyHsLS.png" alt="" title=""/>
                    <p>Parceiros</p>
                </a>
                </div>
            </li>
            <li>
                <div class="icon_div">
                <a href="https://app.falandodeviagem.com.br/interests">
                    <img src="styles/new_beach/template/imgages/Interesses.png" alt="" title=""/>
                    <p>Interesses </p>
                </a>
                </div>
            </li>
            <li>
                <div class="icon_div">
                <a href="https://app.falandodeviagem.com.br/comunidades">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAATCAMAAACnUt2HAAAANlBMVEUAAAD////////////////////////////////////////////////////////////////////xY8b8AAAAEXRSTlMAAA8fLz9PX29/j5+vv8/f7yfr07sAAABtSURBVBhXrc9BDoAgDATAFVoRscD+/7OKkOhBT7pJm2bSQ4vpIfiK6PE6BlwYyOLuKMdSJRkBlYHH0jIwkdIxkqm16mGkdpRSVUXUqwZmd+JmIRRynXNr0WJD3mKtXhFwLpkVZrPZjzsf3/wfd9wGBqvCr+HuAAAAAElFTkSuQmCC" alt="" title=""/>
                    <p>Comunidade</p>
                </a>
                </div>
            </li>
            <li>
                <div class="icon_div">
                <a href="https://app.falandodeviagem.com.br/newsletter">
                    <img src="styles/new_beach/template/imgages/Newsletter.png" alt="" title=""/>
                    <p>Newsletter</p>
                </a>
                </div>
            </li>   
        </ul>
</div>
<div class="clear"></div>

<style type="text/css">.clear{clear:both}.plus_btn{position:fixed;margin-top:20px;float:right;z-index:999;right:10px;top:100px}.plus_btn span{color:#ff0054;font-weight:300;font-size:16px;height:50px;width:50px;text-align:center;line-height:50px;background:#d7d7cd;transition:all .4s;border-radius:50%}.plus_btn:hover span{color:#fff;background-color:#ff0054}.icons{position:fixed;right:0;transition:all .4s;top:100px}.icon_cm{right:0;top:100px}.icons{width:160px}.icons ul{text-align:right;float:right}.icons ul li{text-align:right;list-style:none;overflow-x:hidden;transition:all .5s}.icons ul li a{list-style:none;background-color:#153448;padding:10px 0;display:inline-block;border-bottom:1px solid #255c82;transition:all .4s;text-align:center}.icons ul li img{padding-left:10px;padding-right:10px}.icons ul li:hover{width:auto}.icons ul li:hover img{padding-right:0;text-decoration:none}.icons ul li:hover a p{display:inline-block;text-decoration:none}.icons ul li:hover .icon_div{width:auto}.icons ul li a:hover{background-color:#ff0054}.icons ul li a img{vertical-align:middle;text-decoration:none}.icons ul li a p{display:none;color:#fff;margin-right:10px;margin-top:0;margin-bottom:0;padding-left:6px;transition:all .5s;vertical-align:middle;line-height:21px;font-family:'Arimo',sans-serif}body{overflow-x:hidden}.icon_div{display:inline-block;text-align:center;width:40px}</style>


<script type="text/javascript">$(".plus_btn_a").click(function(){$(".icons").addClass('icon_cm');$(".plus_btn_a").hide();});$(".icons").click(function(){$(".icons").removeClass('icon_cm');$(".plus_btn_a").show();});</script><!-- inclusão do menu fixo no rodapé --><script src="styles/new_beach/template/social-share-count.js"></script>

<?php 
// require_once('./teste2.php');

echo "Algo";


?>
    <!-- Teste adboxes -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <div id="mostra_adboxes"></div>
    <script>
       var UrlAds = "http://ftestes.falandodeviagem.com.br/styles/new_beach/template/adboxes/adboxes_code.php";
       // var UrlAds = "http://localhost/freelas/fdv/anuncio.php?mostra_ad";
if(document.getElementById('mostra_adboxes')){
    $.get(UrlAds, function(data) {
        document.getElementById('mostra_adboxes').innerHTML = data; 
    });
}
    </script>

    <!-- <script src="http://ftestes.falandodeviagem.com.br/styles/new_beach/template/adboxes/require.js"></script> -->
    <!-- Teste adboxes -->
<div class="bottom_sprit">
    
        <div class="share_fac">
            <ul>
                <li class="share_con">
                    <i class="fa fa-share"></i>
                    <p><span><strong data-sc-fb="https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049"></strong></span>shares</p>
                </li>
                <li class="facebook_btn">
<div id="fb-root"></div>
<script>(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.9";fjs.parentNode.insertBefore(js,fjs);}(document,'script','facebook-jssdk'));</script>

<div class="fb-share-button" data-href="https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049" data-layout="button_count" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.falandodeviagem.com.br/viewtopic.php?f=212&amp;t=2049">Share</a></div>
                </li>
            </ul>
        </div>
        
        <div class="flight_icons">
            <ul>
                
                <li>
                    <div class="sprit_point">
                        <ul>
                            <li>
                                <a href="https://fdv.im/VoosAppFt" rel="nofollow" target="blank">
                                    <span class="fa fa-plane"></span>
                                    <p>COMPRAR<br> PASSAGENS</p>
                                </a>
                            </li>
                            <li>
                                <a href="http://fdv.im/HotelBTST" rel="nofollow" target="blank">
                                    <span class="fa fa-hospital-o"></span>
                                    <p>RESERVAR<br/> HOTEL</p>
                                </a>
                            </li>
                            <li>
                                <a href="http://fdv.im/Carro" rel="nofollow" target="blank">
                                    <span class="fa fa-car"></span>
                                    <p>RESERVAR<br/> CARRO </p>
                                </a>
                            </li>
							<li>
                                <a href="https://fdv.im/PasseiosAppFt" rel="nofollow" target="blank">
                                    <span class="fa fa-ticket fa-rotate-180"></span>
                                    <p>COMPRAR <br/> INGRESSOS</p>
                                </a>
                            </li>
                            <li>
                                <a href="https://fdv.im/CompareSeguros2" rel="nofollow" target="blank">
                                    <span class="fa fa-shield"></span>
                                    <p>FAZER <br/> SEGURO</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li> 
            </ul>
        </div>
        <div class="clear"></div>
    </ul>
</div>

<div class="sr">
    
</div>

<style type="text/css">body{margin:0;padding:0}a{text-decoration:none}.clear{clear:both}.bottom_sprit{background-color:rgba(255,255,255,.7);padding:0 30px}.bottom_sprit ul li{display:inline-block;vertical-align:middle}.bottom_sprit ul li.share_con i{font-size:40px;color:#000;font-weight:bold;vertical-align:middle}.bottom_sprit ul li.share_con p{display:inline-block;font-size:19px;color:#000;padding-left:20px;vertical-align:middle;text-align:center}.bottom_sprit ul li.share_con p span{font-family:Arial\ Black;display:block;font-weight:900;font-size:36px;line-height:24px;min-height:24px}.facebook_btn{margin-left:20px}.facebook_btn i{font-size:25px;color:#fff;display:inline-block;vertical-align:middle;margin-right:5px}.facebook_btn p{display:inline-block;color:#fff;font-size:16px;vertical-align:middle}.sprit_point{padding:10px 20px}.sprit_point ul>li{position:relative;padding-right:20px;padding:0 30px;margin-right:-5px;border:2px solid #fff;background-color:#39b9bc;border-radius:5px;margin-right:10px;transition:all .4s}.sprit_point ul>li:hover{background-color:#153448}.sprit_point ul>li:hover span{color:#ff0054}.sprit_point ul>li:hover p{color:#ff0054}.sprit_point ul>li p{transition:all .4s;vertical-align:middle;display:inline-block;font-size:16px;line-height:18px;text-align:center;color:#fff;font-weight:bold;font-family:'Roboto',sans-serif}.sprit_fix{bottom:0;z-index:999;display:block;position:fixed;width:100%;padding:0}.sprit_fix>ul{padding:0;margin:0}.sprit_point ul>li span{transition:all .4s;vertical-align:middle;font-size:40px;margin-right:10px;color:#fff}.share_fac{float:left;vertical-align:middle;margin-top:10px}.flight_icons{float:right;vertical-align:middle}.flight_icons ul{padding:0;margin:0}.sr{height:63px;background:#d8d7ce}.bottom_sprit_con{text-align:center}@media (min-width:1311px){.sprit_point ul>li p{font-size:13px}.sprit_point ul>li span{font-size:25px}}</style>

<!-- plugin -->


<script type="text/javascript" >
$(window).scroll(function() {
    var win_height = $(window).height() / 3;
    var win_scrol = $(window).scrollTop();
    if (win_height < win_scrol) {
        $(".bottom_sprit").addClass('sprit_fix');
    } else {
        $(".bottom_sprit").removeClass('sprit_fix');
    };
});
</script>

</body>
</html>