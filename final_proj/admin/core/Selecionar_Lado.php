<?php
require_once "../bootstrap.php";

//Altera apenas o principal do item
if (isset($_GET[ 'cont_selecionar_lado']) AND isset($_GET[ 'cont_lado']) AND isset($_GET['cont_id'])) {
	$ID = $_GET['cont_id'];
	$lado = $_GET[ 'cont_lado'];
	
	//Verifica se  $lado tem o valor 'esquerdo' ou 'direito'
	if($lado != 'direito' && $lado != 'esquerdo'){
		$msg = "Selecionado lado incorreto!";
		irPara(URLADM . "?p=listar_conteudo&msg=$msg");
		exit();
	}

	$ladoInverso = ($lado == 'direito')?'esquerdo':'direito' ;

	$tabela = "adboxes_conts";
	$estado = "habilitado";
	$where = "WHERE cont_id='{$ID}' AND cont_estado='{$estado}'";
	$ativado = $conn->query("select count(*) from $tabela {$where}")->fetchColumn();



	if ($ativado) {
		try {
			//Passo 1 - Elimina a concorrencia: zera outros banner que possam estar com a mesma posição
			$sql = "UPDATE adboxes_conts SET cont_lado='' WHERE cont_lado='' or cont_lado=NULL OR cont_lado='{$lado}' OR cont_lado='{ $ladoInverso}';";
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$x = $conn->prepare($sql);
			$x->execute();
			//Fim do //Passo 1 

			//Passo 2 - Setar efetivamente a posição do banner (se a mesma posição já estiver sendo usada, ficara apenas 1 banner como ativo)
			$sql = "UPDATE adboxes_conts SET cont_lado='{$lado}' WHERE cont_id=$ID;";
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$x = $conn->prepare($sql);
			$x->execute();
			//Fim do //Passo 2 

			$msg = "Atualizado!";
			irPara(URLADM . "?p=listar_conteudo&msg=$msg");
			exit();
		} catch (\Throwable $th) {
			throw $th;

		}
	} else {
		$msg = "O banner \"$ID\" não pode ser selecionado por estar desabilitado!";
		irPara(URLADM . "?p=listar_conteudo&msg=$msg");
	}

	exit();
} else {
	irPara();
}
?>