<?php
    $msg = "";
    require_once('../bootstrap.php');
    
    if(isset($_GET['altera_para']) AND !$_GET['altera_para']=='' 
    AND isset($_GET['cont_id']) AND !$_GET['cont_id']=='')
    {
        
        $ID = $_GET['cont_id'];
        $tabela = "adboxes_conts";
        $estado = $_GET['altera_para'];
        $where = "where cont_id='$ID'";
        // $sql = "delete from $tabela $where";
        
        $sql = "UPDATE adboxes_conts SET cont_estado='$estado' $where;";
        // $conn->exec($sql);


        //Verifica se o ID existe
        $haLinhas = $conn->query("select count(*) from $tabela {$where}")->fetchColumn();
        // $haLinhas = $conn->query($sql)->fetchColumn();
        if($haLinhas)
        {
            $checaDir = $conn->query("select count(*) from $tabela {$where} and cont_lado='direito'")->fetchColumn();
            $checaEsq = $conn->query("select count(*) from $tabela {$where} and cont_lado='esquerdo'")->fetchColumn();
            $bannerPrincipal = $checaDir+$checaEsq;
            
            //Verifica se é bannerPrincipal
            if(!$bannerPrincipal)
            {
                if($conn->exec($sql))
                {
                    $msg = "O estado do banner \"$ID\" alterado para $estado";
                    irPara(URLADM."?p=listar_conteudo&msg=$msg");
                    // echo $msg;
                }
                else
                {
                    $msg = "Erro ao desabilitar banner \"$ID\"";
                    irPara(URLADM."?p=listar_conteudo&msg=$msg");
                    // echo $msg;
                }
            }
            else
            {
                $msg = "O banner \"$ID\" não pode ser desabilitado por ser um Banner Principal";
                irPara(URLADM."?p=listar_conteudo&msg=$msg");
                // echo $msg;
            }

        }
        else
        {                            
            $msg = "Registro \"$ID\" não foi encontrado";
            irPara(URLADM."?p=listar_conteudo&msg=$msg");
            // echo $msg;
        }
    }
?>