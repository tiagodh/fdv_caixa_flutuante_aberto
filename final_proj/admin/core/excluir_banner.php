<?php
    $msg = "";
    require_once('../bootstrap.php');
    if(isset($_GET['excluir_banner']) AND isset($_GET['cont_id']) AND !$_GET['cont_id']==''){
        $ID = $_GET['cont_id'];
        $tabela = "adboxes_conts";
        $where = "where cont_id='$ID'";
        $sql = "delete from $tabela $where";
        
        //Verifica se o ID existe
        $haLinhas = $conn->query("select count(*) from $tabela {$where}")->fetchColumn();
        // $haLinhas = $conn->query($sql)->fetchColumn();
        if($haLinhas){
            
            $checaDir = $conn->query("select count(*) from $tabela {$where} and cont_lado='direito'")->fetchColumn();
            $checaEsq = $conn->query("select count(*) from $tabela {$where} and cont_lado='esquerdo'")->fetchColumn();
            $bannerPrincipal = $checaDir+$checaEsq;
            
            //Verifica se é bannerPrincipal
            if(!$bannerPrincipal){
                if($conn->exec($sql)){
                    $msg = "Banner \"$ID\" deletado";
                    irPara(URLADM."?p=listar_conteudo&msg=$msg");
                    // echo $msg;
                    }else{
                    $msg = "erro ao excluir banner \"$ID\"";
                    irPara(URLADM."?p=listar_conteudo&msg=$msg");
                    // echo $msg;
                }
            }else{
                $msg = "O banner \"$ID\" não pode ser excluido por ser um Banner Principal";
                irPara(URLADM."?p=listar_conteudo&msg=$msg");
                // echo $msg;
            }

        }else{
            $msg = "Registro \"$ID\" não foi encontrado";
            irPara(URLADM."?p=listar_conteudo&msg=$msg");
            // echo $msg;
        }
    }else{
        $msg = "";
        irPara(URLADM."?p=listar_conteudo&msg=$msg");
        // echo $msg;
    }
?>