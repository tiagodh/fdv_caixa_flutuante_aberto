<?php
    require_once("../bootstrap.php");

    //Altera apenas o estado do item
    if(isset($_POST['ad_altera_estado']) AND isset($_POST['ad_ativo']) AND isset($_POST['ad_id']))
    {
        
        $ID = $_POST['ad_id'];
        $estado = $_POST['ad_ativo'];
        try {
            
    $sql = <<<EOF
    UPDATE "adboxes_ads" SET "ad_ativo"="{$estado}" WHERE "ad_id"="{$ID}"
EOF;
    $conn->query($sql);
    irPara();
    exit();
        } catch (\Throwable $th) {
            throw $th;
        }
        exit();
    }else{

    //Altera outros dados do item
    $valida = false;
    if(
        isset($_POST['ad_id']) 
        AND isset($_POST['ad_atualiza_dados']) 
        AND isset($_POST['ad_nome']) 
        AND isset($_POST['ad_titulo']) 
        AND isset($_POST['ad_conteudo'])
    ){$valida=true;}

    if($valida){
    
        $ID         = $_POST['ad_id'];
        $adNome     = $_POST['ad_nome'];
        $adTitulo   = $_POST['ad_titulo'];
        $adCont     = htmlspecialchars($_POST['ad_conteudo']);//htmlspecialchars_decode

        try {
            
    $sql = <<<EOF
    UPDATE "adboxes_ads" SET 
    "ad_nome"="{$adNome}",
    "ad_titulo"="{$adTitulo}",
    "ad_conteudo"="{$adCont}"    
    WHERE "ad_id"="{$ID}"
EOF;
    $conn->query($sql);    
    irPara();
    echo "<pre>".$sql."</pre>";
    exit();
        } catch (\Throwable $th) {
            throw $th;
        }
        exit();
    }

}
irPara();
?>