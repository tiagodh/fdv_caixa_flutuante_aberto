<?php
require_once "../bootstrap.php";

//Altera apenas o principal do item
if (isset($_GET['cont_altera_principal']) AND isset($_GET['cont_id'])) {
	$ID = $_GET['cont_id'];

	$tabela = "adboxes_conts";
	$estado = "habilitado";
	$where = "WHERE cont_id='{$ID}' AND cont_estado='{$estado}'";
	$ativado = $conn->query("select count(*) from $tabela {$where}")->fetchColumn();

	if ($ativado) {
		try {
			//Passo 1 - Remove todos os principais se houver
			$sql = "UPDATE adboxes_conts SET cont_principal='nao' WHERE cont_principal='' or cont_principal=NULL or cont_principal='sim' or cont_principal='nao';";
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$x = $conn->prepare($sql);
			$x->execute();

			//Passo 1 - Seta o principal
			$sql = "UPDATE adboxes_conts SET cont_principal='sim' WHERE cont_id=:con;";
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$x = $conn->prepare($sql);
			$x->bindParam(":con", $ID);
			$x->execute();

			$msg = "Atualizado!";
			irPara(URLADM . "?p=listar_conteudo&msg=$msg");
			exit();
		} catch (\Throwable $th) {
			throw $th;
		}
	} else {
		$msg = "O banner \"$ID\" não pode se tornar o principal por estar desabilitado!";
		irPara(URLADM . "?p=listar_conteudo&msg=$msg");
	}

	exit();
} else {
	irPara();
}
?>