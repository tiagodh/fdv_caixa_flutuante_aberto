<?php
function needLogin(){
	if(!isset($_SESSION)){session_start();}
	if(!isset($_SESSION['loginAdmPainel']) OR $_SESSION['loginAdmPainel'] != "LOGADO" )
	irPara(URLADM."index.php?p=login");
	// redirHeader(URLADM."?p=listar_conteudo#falta-dados");
}

//Função que retorna apenas 1 resultado
function singleResult($col, $table, $where)
{
	global $conn;
	$caixaEsquerda = "SELECT {$col} FROM '{$table}' WHERE {$where} LIMIT 1";
	$statementEsquerda = $conn->prepare($caixaEsquerda);
	$statementEsquerda->execute();
	$resultadoEsquerda = $statementEsquerda->fetchAll(PDO::FETCH_ASSOC);
	$rescaixaEsquerda = $resultadoEsquerda[0]['caixa_estado'];
	return $rescaixaEsquerda;
}

//Função que retorna os resultado da tabela
function showResult($col, $table, $where)
{
	global $conn;
	$caixaEsquerda = "SELECT caixa_estado FROM 'adboxes_boxes' WHERE `caixa_nome`='esquerda'";
	$statementEsquerda = $conn->prepare($caixaEsquerda);
	$statementEsquerda->execute();
	$resultadoEsquerda = $statementEsquerda->fetchAll(PDO::FETCH_ASSOC);
	$rescaixaEsquerda = $resultadoEsquerda[0]['caixa_estado'];
	return $rescaixaEsquerda;
}

function redirHeader($url) {
	header('Location:' . $url);
}

function irPara($url = URLSITE) {
	if (isset($_GET['irPara'])) {
		$url = $_GET['irPara'];
	}
	return redirHeader($url);
}

function errolog($sim = true) // true | false
{
	if ($sim == true) {
		ini_set('display_errors', 1);
		ini_set('log_errors', 1);
		ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
		error_reporting(E_ALL);
	}
}

function getHostFromURL($url = '') {
	$parse = parse_url($url);
	$hostUrl = $url;
	if (isset($parse['host'])) {
		$scheme = $parse['scheme'];
		$host = $parse['host'];
		$divider = "://";
		$hostUrl = $scheme . $divider . $host;
	}
	return $hostUrl;
}

function vd($vd){
	return var_dump($vd);
}

function limitChars($str, $maxChars = 200) {
	if (strlen($str) <= $maxChars) {
		echo $str;
	} else {
		$str = substr($str, 0, $maxChars) . '...';
		echo $str;
	}
}

function toMb($num){
	return $num *(1024*1024);
}

function botaoEditorVisual()
{
	global $estaUrl;
	if (!isset($_GET['editor']) OR $_GET['editor']=="false") { 
    	echo '<a href="'.$estaUrl.'&editor=true" onclick="if(!confirm(\'Ao alterar de editor, suas alterações serão perdidas.\nDeseja continuar?\')) {return false;}" class="botao bg-verde">Usar editor visual</a>';
        }else{
		echo '<a href="'.$estaUrl.'&editor=false" onclick="if(!confirm(\'Ao alterar de editor, suas alterações serão perdidas.\nDeseja continuar?\')) {return false;}" class="botao bg-azul">Usar editor simples</a>';
	}
}

function manyTests(){
echo '<div class="row">    
	<div class="col-12" style="text-align: center;">';

$writeDb = '<div class="msg_alert">Sem permissão de escrita na pasta "/admin/"</div>';
$writeImgUp = '<div class="msg_alert">Sem permissão de escrita na pasta "/'.IMG_FOLDER_UP.'"</div>';

if (!is_writable(ROOTSITE)) {
	echo $writeDb;
}
if (!is_writable(IMG_PATH_FULL)) {
	echo $writeImgUp;
}

echo '	</div>
</div>';
	
}