<?php

//URL Base base do código (Ao final, coloque uma barra '/')
$URLBase = "http://localhost/falandodeviagem/fdv_clone27042019/final_proj/";
$docVersion = "Documentacao_v1.pdf";
$imgExtAllow = ["jpeg","jpg","png"]; //Extensões permitidas para upload, cada extensão entre aspas e separado por vírgula
$imgFolderUp = "imagens_up/"; //Pasta de upload das imagens
$imgMaxSize = 6; //Tamanho máximo das imagens para upload por MB

#-------------- NÃO MEXER a partir deste ponto ----------------
//NÃO MEXER constantes

define('IMG_FOLDER_UP', $imgFolderUp); //NÃO MEXER   | Pasta de upload das imagens
define('IMG_EXT_ALLOW', $imgExtAllow); //NÃO MEXER   | Tamanho máximo das imagens para upload por MB
define('IMG_MAX_SIZE', $imgMaxSize); //NÃO MEXER     | Extensões permitidas para upload
define('URLSITE', $URLBase); //NÃO MEXER
define('ROOTSITE', __DIR__ . "/"); //NÃO MEXER
define('IMG_PATH_FULL', dirname(ROOTSITE)."/".IMG_FOLDER_UP); //NÃO MEXER
define('URLADM', URLSITE . "admin/"); //NÃO MEXER
define('IMG_URL_FULL', URLSITE.IMG_FOLDER_UP); //NÃO MEXER     | Extensões permitidas para upload
require_once ROOTSITE . 'banco.php';
require_once ROOTSITE . 'configs.php';
require_once __DIR__ . '/core/functions.php';

$estaUrl = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

define('ASSETS', URLADM . 'assets/');
define('VIEWS', ROOTSITE . 'views/');
define('CORE', ROOTSITE . 'core/');
define('PAGINAS', VIEWS . 'paginas/');
