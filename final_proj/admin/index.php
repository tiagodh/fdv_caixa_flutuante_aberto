<?php session_start();
require_once('bootstrap.php');

    $p = (isset($_GET['p']))?$_GET['p'] : "inicio";
    $pagina = PAGINAS.$p.".php";
    if(!file_exists($pagina)){
        $pagina = PAGINAS."404.php";
    }

    require_once(VIEWS.'header.php');
    require_once($pagina);
    require_once(VIEWS.'footer.php');
    errolog();// true | false
?>