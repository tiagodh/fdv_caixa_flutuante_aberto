<?php needLogin(); ?>
<div class="row">
  <div class="col-12">
    <h2>Listar Banners</h2>
  </div>
</div>

<div class="row">

  <style>
    table {
      border-collapse: collapse;
      border-spacing: 0;
      width: 100%;
      border: 1px solid #ddd;
    }

    th,
    td {
      text-align: left;
      padding: 8px;
    }

    tr:hover {
      background: #a3b9ea91 !important;
    }

    thead tr {
      background: #a3dada !important;
    }

    thead tr:hover {
      background: transparent !important;
    }

    tr:nth-child(even) {
      background-color: #f2f2f2
    }

    @media screen and (max-width: 600px) {
      table {
        border: 0;
      }

      table caption {
        font-size: 1.3em;
      }

      table thead {
        border: none;
        clip: rect(0 0 0 0);
        height: 1px;
        margin: -1px;
        overflow: hidden;
        padding: 0;
        position: absolute;
        width: 1px;
      }

      table tr {
        border-bottom: 3px solid #ddd;
        display: block;
        margin-bottom: .625em;
      }

      table td {
        border-bottom: 1px solid #ddd;
        display: block;
        font-size: .8em;
        text-align: right;
        height: auto;
      }

      table td::before {
        /*
    * aria-label has no advantage, it won't be read inside a table
    content: attr(aria-label);
    */
        content: attr(data-label);
        float: left;
        font-weight: bold;
        text-transform: uppercase;
      }

      table td:last-child {
        border-bottom: 0;
      }

      tr:hover {
        background: transparent;
      }
    }
  </style>

</div>
<table>
  <div class="bottom_box">
    <a class="botao bg-verde" href="?p=editar_conteudo&adicionar_item">Adicionar novo Item</a>
  </div>
  <caption>Listar Banners</caption>
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nome</th>
      <th scope="col">Titulo</th>
      <th scope="col">Imagem</th>
      <th scope="col">Conteúdo</th>
      <th scope="col">Estado</th>
      <th scope="col">Posição</th>
      <th scope="col">Editar</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $sql = "select * from adboxes_conts";
    $consulta = $conn->query($sql);
    // $haLinhas = $conn->query("select count(*) from adboxes_ads {$where}")->fetchColumn();
    $haLinhas = $conn->query($sql)->fetchColumn();
    if (!$haLinhas) {
      echo "Não ha registros.";
    } else {
      foreach ($consulta as $keyContent => $valueContent) {
        $ID = $valueContent['cont_id'];
        $ID = ($ID > 0 and $ID < 10) ? "0" . $ID : $ID;
        $nome = $valueContent['cont_nome'];
        $titulo = $valueContent['cont_titulo'];
        $img = $valueContent['cont_img'];
        $contImg = $img;

        //Verificação da imagem
        // $pathImg = ($contImg == "") ? "" : IMG_URL_FULL;
        // $seContem = '/http(s?)\:\/\/|\/\//i'; //Patern (regular) para verificar se há uma url
        // $contImgSrc = (preg_match($seContem, $img)) ? $img : $pathImg . $img; //Verifica se tem http, https ou // na url da imagem
        $contImgSrc = str_replace("{{URL_IMAGENS_UP}}", IMG_URL_FULL, $img); //Solução mais simplificada

        $link = $valueContent['cont_link'];
        $conteudo = htmlspecialchars_decode($valueContent['cont_conteudo']);
        $conteudo = strip_tags($conteudo);
        $conteudo = substr($conteudo, 0, 25) . "...";
        $estado = $valueContent['cont_estado'];
        $principal = $valueContent['cont_principal'];
        $caixaLado = $valueContent['cont_lado'];

        switch ($estado) {
          case 'habilitado':
            $estado = "
                        <span class='cor-verde'>Habilitado</span> |
                        <a class='cor-semcor botao' href='core/altera_estado.php?altera_para=desabilitado&cont_id=$ID' style='font-size: 0.7em;'>Desabilitar</a>
              ";
            break;

          case 'desabilitado':
            $estado = "
                        <span class='cor-vermelho'>Desabilitado</span> |
                        <a class='cor-semcor botao' href='core/altera_estado.php?altera_para=habilitado&cont_id=$ID' style='font-size: 0.7em;'>Habilitar</a>
              ";
            break;

          default:
            $estado = "-";
            break;
        }

        $linkSelecionarLadoDireito = URLADM . "core/Selecionar_Lado.php?cont_selecionar_lado&cont_lado=direito&cont_id=" . $ID;
        $linkSelecionarLadoEsquerdo = URLADM . "core/Selecionar_Lado.php?cont_selecionar_lado&cont_lado=esquerdo&cont_id=" . $ID;

        $ladoSelecaoDireita = ($caixaLado == 'direito') ? 'clicado' : 'a-clicar';
        $ladoSelecaoEsquerda = ($caixaLado == 'esquerdo') ? 'clicado' : 'a-clicar';

        $selDireita = "<a class='btn-posicao {$ladoSelecaoDireita}' style='font-size:0.7em;float:left;width: 80px;' href='{$linkSelecionarLadoDireito}'>Direita</a>";
        $selEsquerda = "<a class='btn-posicao {$ladoSelecaoEsquerda}' style='font-size:0.7em;float:left;width: 80px;' href='{$linkSelecionarLadoEsquerdo}'>Esquerda</a>";

        $selecionaLado = $selEsquerda . $selDireita;

        echo <<<EOF
    <tr>
        <td data-label='ID'>{$ID}</td>
        <td data-label='Nome'>{$nome}</td>
        <td data-label='Titulo'>{$titulo}</td>
        <td data-label='Imagem'><img class='col-8' style='cursor:pointer;' onclick='setImg(this)' src='{$contImgSrc}' alt=''></td>
        <td data-label='Conteúdo'>{$conteudo}</td>
        <td data-label='Estado'>
          <div><strong>
            {$estado}
          </strong></div>
        </td>
        <td data-label='Posição'><div style="display:inline-flex">{$selecionaLado}</div></td>
        <td data-label='Editar' style='height:3em;'>
          <div style="display:inline-flex"><a class='botao cor-azul' style='background:#9dbeef;' href='?p=editar_conteudo&cont_id={$ID}&cont_editar'>Editar</a>  <a class='botao cor-vermelho' style='background:#d68a8a;' href='core/excluir_banner.php?excluir_banner&cont_id={$ID}'  onclick=" if (!confirm('Deseja mesmo deletar o registro {$ID}?')) {
      return false;
    }
    ">Excluir</a></div>
          </td>
    </tr>
EOF;
      }
    }
    ?>
  </tbody>
</table>
<?php
require_once(VIEWS . 'modal.php');
?>