<?php needLogin(); ?>
<?php
    function adDetalhes(){
        global $conn;
        $sqlAd = "SELECT * FROM 'adboxes_ads' where ad_id='1'";
        $consultaAd = $conn->query($sqlAd);
        if($consultaAd){                            
            foreach($consultaAd as $keyAd => $valueAd){
                $conteudo = $valueAd['ad_conteudo'];
                echo "<center><strong>Estado do Ad:</strong></center>";
                echo "<strong>Ativo:</strong> ".$valueAd['ad_ativo']."<br>";
                echo "<strong>Nome:</strong> ".$valueAd['ad_nome']."<br>";
                echo "<strong>Título:</strong> ".$valueAd['ad_titulo']."<br>";
                echo "<strong>Código:</strong><br>";
                echo "<pre>".$conteudo."<br>";
                echo "</pre>";
                // echo "Fica assim:<br>";
                // echo htmlspecialchars_decode($conteudo)."<br>";
            }
        }
    }

    function contDetalhes(){
        global $conn;
        $sqlAd = "SELECT * FROM 'adboxes_conts' where cont_id='1'";
        $consultaAd = $conn->query($sqlAd);
        if($consultaAd){
            echo "<br>Conteúdos selecionados como 'Ativo' serão mostrados nas páginas.";
            /* *
            /* Mostrar detalhes do conteúdopadrãi ou não?                       
            foreach($consultaAd as $keyAd => $valueAd){
                echo "<br>";
                echo "<strong>Ativo:</strong> ".$valueAd['cont_estado']."<br>";
                echo "<strong>Nome:</strong> ".$valueAd['cont_nome']."<br>";
                echo "<strong>Título:</strong> ".$valueAd['cont_titulo']."<br>";
                echo "<strong>Código:</strong><br>";
                echo "<pre>".htmlentities($valueAd['cont_conteudo'], ENT_QUOTES)."<br>";
                echo "</pre>";
            }
            /**/
        }
    }
?>
<div class="row">
    <div class="col-12">
        <h2>Inicio</h2>
    </div>
</div>

<div class="row">    
    <div class="col-4 painel">
    <?php
        $where = "where caixa_nome='esquerda'";
        $sql = "SELECT * FROM adboxes_boxes $where ORDER BY caixa_id ASC";
        $consulta = $conn->query($sql);
        foreach ($consulta as $key => $value) {
            $checkOff = ($value['caixa_estado']=='off')?'checked':'';
            $checkOn = ($value['caixa_estado']=='on')?'checked':'';
    ?>
        <div class="top_box">
            <strong style="font-size: 0.8em;">Caixa esquerda</strong>
            <form action="<?=URLADM;?>core/Muda_Estado_Box.php">
                <input type="hidden" name="caixa_nome" value="esquerda">
                <label class="btn-vermelho"><input type="radio" name="caixa_estado" value="off" <?=$checkOff;?> required>Off</label>
                <label class="btn-verde"><input type="radio" name="caixa_estado" value="on" <?=$checkOn;?> required>On</label>
                <input type="hidden" name="irPara" value="<?=$estaUrl;?>">
                <button class="botao bg-verde" type="submit" name="altera_cont_box">Atualizar</button>
            </form>
        </div>
        <?php
            if($value['caixa_tipo']=='ads'){
        ?>                
                <div class="desc_box">                
                <?php                
                   adDetalhes();
                ?>
                </div>
        <?php
            }elseif($value['caixa_tipo']=='conteudo'){
        ?>
                <!-- <div class="desc_box">                
                <?php                
                    contDetalhes();
                ?>
                </div> -->
        <?php } ?>
       
        <div class="bottom_box">
        <?php
            if($value['caixa_tipo']=='conteudo'){
        ?>                
                <a class="botao" href="<?=URLADM;?>?p=listar_conteudo">Listar conteúdo</a>
        <?php
            }elseif($value['caixa_tipo']=='ads'){
        ?>                
            <a class="botao" href="<?=URLADM;?>?p=editar_ad">Alterar código Ad</a>
        <?php
                    }
        ?>
        </div>
    <?php } ?>
    </div>
    <!-- Fim do painel esquerda -->
    
    <div class="col-4 painel">
    <?php
        $where = "where caixa_nome='direita'";
        $sql = "SELECT * FROM adboxes_boxes $where ORDER BY caixa_id ASC";
        $consulta = $conn->query($sql);
        foreach ($consulta as $key => $value) {
            $checkOff = ($value['caixa_estado']=='off')?'checked':'';
            $checkOn = ($value['caixa_estado']=='on')?'checked':'';
    ?>
        <div class="top_box">
            <strong style="font-size: 0.8em;">Caixa direita</strong>
            <form action="<?=URLADM;?>core/Muda_Estado_Box.php">
                <input type="hidden" name="caixa_nome" value="direita">
                <label class="btn-vermelho"><input type="radio" name="caixa_estado" value="off" <?=$checkOff;?> required>Off</label>
                <label class="btn-verde"><input type="radio" name="caixa_estado" value="on" <?=$checkOn;?> required>On</label>
                <input type="hidden" name="irPara" value="<?=$estaUrl;?>">
                <button class="botao bg-verde" type="submit" name="altera_cont_box">Atualizar</button>
            </form>
        </div>
        <?php
            if($value['caixa_tipo']=='ads'){
        ?>                
                <div class="desc_box">
                <?php                
                  adDetalhes();
                ?>
                </div>
        <?php
            }elseif($value['caixa_tipo']=='conteudo'){
        ?>
                <!-- <div class="desc_box">
                <?php                
                    contDetalhes();
                ?>
                </div> -->
        <?php } ?>
       
        <div class="bottom_box">
        <?php
            if($value['caixa_tipo']=='conteudo'){
        ?>                
                <a class="botao" href="<?=URLADM;?>?p=listar_conteudo">Listar conteúdo</a>
        <?php
            }elseif($value['caixa_tipo']=='ads'){
        ?>                
            <a class="botao" href="<?=URLADM;?>?p=editar_ad">Alterar código Ad</a>
        <?php
                    }
        ?>
        </div>
    <?php } ?>
    </div>
    <!-- Fim do painel direita -->

</div>