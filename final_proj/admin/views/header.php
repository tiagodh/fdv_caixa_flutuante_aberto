<!DOCTYPE html>
<html lang="pt">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Social share meta tags -->
  <meta property="og:title" content="Float Banner">
  <meta property="og:description" content="Float Banner">
  <meta property="og:type" content="website">
  <meta property="og:image" content="http://tiagofranca.com/img/metaThumbImage.png">
  <meta property="og:site_name" content="Float Banner">
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:title" content="Float Banner">
  <meta name="twitter:description" content="Float Banner">
  <meta name="twitter:image:src" content="http://tiagofranca.com/img/metaThumbImage.png">
  <!-- End social share meta tags -->
  <link rel="shortcut icon" href="<?=ASSETS;?>img/favicon.png">
  <link rel="stylesheet" href="<?=ASSETS;?>css/reset.css">
  <link rel="stylesheet" href="<?=ASSETS;?>css/simple-grid.css">
  <link rel="stylesheet" href="<?=ASSETS;?>css/template.css">
  <title>Float Banner | Gerenciador</title>
  <meta name="description" content="Gerenciador de Banner flutuante">
<?php
    if(isset($_GET['editor'])){
      if($_GET['editor']=='true')
        require_once(VIEWS.'editor_antes_body.php');
    }
?>
</head>
<body>
<div class="container-box">
<?php
  if(isset($_SESSION['loginAdmPainel']) AND $_SESSION['loginAdmPainel'] === "LOGADO" )
    require_once(VIEWS.'menu.php');
  
?>
